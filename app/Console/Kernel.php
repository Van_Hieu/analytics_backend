<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Illuminate\Support\Facades\DB;
use Mail;

// require_once '../../csrest_subscribers.php';

use CS_REST_Subscribers;   

// require __DIR__.'/campaign_monitor/csrest_campaigns.php';
// require __DIR__.'/campaign_monitor/csrest_general.php';
// require __DIR__.'/campaign_monitor/csrest_clients.php';
// require __DIR__.'/campaign_monitor/csrest_templates.php';
// require __DIR__.'/campaign_monitor/csrest_lists.php';
// require __DIR__.'/campaign_monitor/csrest_segments.php';
// require __DIR__.'/campaign_monitor/csrest_subscribers.php';

// require_once 'campaign_monitor/csrest_campaigns.php';
// require_once 'campaign_monitor/csrest_general.php';
// 	require_once 'campaign_monitor/csrest_clients.php';
// 	require_once 'campaign_monitor/csrest_templates.php';
// 	require_once 'campaign_monitor/csrest_lists.php';
// 	require_once 'campaign_monitor/csrest_segments.php';
// 	require_once 'campaign_monitor/csrest_subscribers.php';



class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        $schedule->call(function () {
            
            $file_name = 'abc'.'.txt';
                    $path = base_path() .'/'. "cron_test/" . $file_name;
                    $current = file_get_contents($path);
                    // Ajoute une personne
                    $current .= "Jean Dupond\n";
                    // Écrit le résultat dans le fichier
                    
                     $result = DB::table('mt_users')
                        ->select('*')
                        // ->where('mt_users.mu_username',$email)
                        ->get();
                        
                    $campaigns ;
                    
                     $auth = array('api_key' => '131f52147c77c564d0b86213cf2b3d67f4a40f5b85c3e0ea');
                                        
                                        $list_id = 'b6e7d20ce05993e6a5589d85f9d67bbc';

                                      $wrap = new CS_REST_Subscribers($list_id, $auth);
                                      
                                      $f_t_1m;
                                      $f_t_1k;
                                      $start_date;
                                      $status;
                        
                        foreach( $result as $row){
                            
                            $email = $row->mu_username;
                            
                            $subscriber =  array (
                                    
                                                'EmailAddress' => $email,
                                                'Name' => '',
                                                 'CustomFields' => array(      
                                                                 array(
                                                                     'Key' => 'Status',
                                                                      'Value' => 'Inactive'
                                                                  )
                                                             ),
                                                'ConsentToTrack' => "yes"
                                                );
                            
                           $add =  $wrap->add($subscriber);
                            $current .= "\n". json_encode( $add);      
                            
                            if($row->verify_email == null){
                                          
                                          $f_t_1m = array(
                                                'Key' => 'Free Trial 1M',
                                                'Value' => 'No'
                                              );
                                              
                                              $f_t_1k = array(
                                                'Key' => 'Free Trial 1K',
                                                'Value' => 'Yes'
                                              );
                                          
                                      }else{
                                          
                                          $f_t_1m = array(
                                                'Key' => 'Free Trial 1M',
                                                'Value' => 'Yes'
                                              );
                                              
                                              $f_t_1k = array(
                                                'Key' => 'Free Trial 1K',
                                                'Value' => 'No'
                                              );
                                          
                                      }
                                      
                                      
                            
                            $start_first = date('Y-m-d H:i:s', StrToTime ( $row->mu_created_date ));      

                                        $s_d = new \DateTime($start_first);     
                                      
                                      $start_date = array(
                                            'Key' => 'Start Date',
                                            'Value' => $s_d->format('Y-m-d')
                                            
                                          );
                                          
                            
                            $campaigns = DB::table('master_campaign')
                                    ->select('master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_created_date','master_campaign.mc_order_id','master_campaign.mc_industry','master_campaign.mc_order_hash','master_campaign.mc_opens','master_campaign.mc_bonus','master_campaign.mc_auto_top_up','master_campaign.mc_type', 'master_campaign.mc_set_cap', 'master_campaign.mc_end', 'master_campaign.mc_html_heat_map', 'master_campaign.mc_is_sent_email')    
                                    ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                                    ->where('mt_users.mu_username',$email)
                                    ->orderBy('master_campaign.mc_created_date','asc')
                                    ->get();
                                    
                                    
                                    if(count($campaigns) > 0){
                                        
                                        $status = array(
                                                                     'Key' => 'Status',
                                                                      'Value' => 'Partially Active'
                                                                  );
                                        
                                                foreach( $campaigns as $row2){
                                  
                                                        $mc_id = $row2->mc_id;
                                                        $mc_type = $row2->mc_type;
                                                        
                                                        if($mc_type == 1){    /// Email type
                                                            
                                                                $ids = DB::table('master_campaign')
                                                                            ->select('Campaign.Camp_Code_Id')
                                                                            ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                                                                            ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                                                                            ->where('master_campaign.mc_id', $mc_id)
                                                                            ->get();
                                                                    $total = 0;        
                                                                    foreach ($ids as $row3) {
                                                                        $total = DB::table('Sent_Mail')
                                                                                ->select(DB::raw('COUNT(Cust_Email_uniqid) as Opens'))
                                                                                ->where('Camp_Code_Id',$row3->Camp_Code_Id)->get()[0]->Opens ;
                                                                                
                                                                                
                                                                            if($total >= 5){
                                                                                
                                                                                $status = array(
                                                                                         'Key' => 'Status',
                                                                                          'Value' => 'Active'
                                                                                      );
                                                                                      
                                                                                      break;  
                                                                            }
                                                                    }
                                                                    
                                                                    
                                                            
                                                            
                                                        }else{     // Link Type
                                                            
                                                                $ids = DB::table('master_campaign')
                                                                            ->select('Linkly.li_id')
                                                                            ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
                                                                            ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
                                                                            ->where('master_campaign.mc_id', $mc_id)
                                                                            ->get();
                                                                            
                                                                $total = 0;        
                                                                    foreach ($ids as $row3) {
                                                                        $total = DB::table('linkly_details')
                                                                                ->select(DB::raw('COUNT(ld_uid) as Opens'))
                                                                                ->where('ld_id',$row3->li_id)->get()[0]->Opens ;
                                                                                
                                                                                
                                                                            if($total >= 5){
                                                                                
                                                                                $status = array(
                                                                                         'Key' => 'Status',
                                                                                          'Value' => 'Active'
                                                                                      );
                                                                                      
                                                                                      break;  
                                                                            }
                                                                    }
                                                        }
                                                      
                                                  } 
                                        
                                    }else{
                                        
                                       
                                      
                                      
                                      
                                      
                                      
                                //       $datetime = new DateTime();
		                              //$s_d = $datetime->createFromFormat('Y-m-d H:i:s', $row->mu_created_date);
		                              
		                               
                                          
                                          
                                          $status = array(
                                                                     'Key' => 'Status',
                                                                      'Value' => 'Inactive'
                                                                  );
                                       
                                       
                                      
                                        
                                    }
                                    
                                    $subscriber =  array (
                                    
                                                'EmailAddress' => $email,
                                                'Name' => '',
                                                'CustomFields' => array(      
                                                                 $status,
                                                                  $f_t_1m,
                                                                  $f_t_1k,
                                                                  $start_date
                                                             ),
                                                'ConsentToTrack' => "yes"
                            //                      'Resubscribe' =>false,
						                      //  'RestartSubscriptionBasedAutoResponders' => false

                                            );
                                        
                                        // $result = $wrap->add($subscriber); 
                                        
                                         $result = $wrap->update($email, $subscriber);
                                         
                                         $result->email = $email;    
                                         
                                          $current .= "\n". json_encode( $result);
                            
                                     
                            
                        }
                       
        // $current .= "\n". json_encode( array($result));    
                    // file_put_contents($path, $current);    
            
       
            
        })->hourly();      
        
     
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
