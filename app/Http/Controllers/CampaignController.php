<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use Illuminate\Support\Facades\DB;
// use App\Campaign;

class CampaignController extends Controller
{
    public function addOpensClicksCredit(Request $request){
        $mc_id = $request->input('mc_id');
        $opens = $request->input('opens');

        DB::table("master_campaign_detail")->insert([
            // 'mc_id' => null,
            'mc_id' => $mc_id,
            'balance' => $opens
        ]);
        
        DB::table('master_campaign')
                        ->where("master_campaign.mc_id", $mc_id)
                        ->update(["master_campaign.mc_is_sent_email" => 0]); 

        return Response::json(array(
            'error' => false,
            'response' => 'Add credit successfully',                
            'status_code' => 200
        ));
        
    }

    public function changeSetCapToTrackAll(Request $request){


            $mc_id = $request->input('mc_id');

            DB::table('master_campaign')
                    ->where('mc_id', $mc_id)
                    ->update(['mc_set_cap' => 0, 'mc_opens' => 0]);

            $results = DB::table('master_campaign')
                        ->select('Campaign.Camp_Code_Id')
                        ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                        ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                        ->where('master_campaign.mc_id', $mc_id)
                        ->get();
            $total = 0;        
            foreach ($results as $row2) {
                $total += DB::table('Sent_Mail_Detail')
                        ->select(DB::raw('COUNT(Email) as Opens'))
                        ->where('Camp_Code_Id',$row2->Camp_Code_Id)->get()[0]->Opens ;
            }

            $current_camp = DB::table('master_campaign')
                                ->where('mc_id', $mc_id)
                                ->get();

            foreach ($current_camp as $row1) {

                if($row1->mc_set_cap == 0 && $row1->mc_opens == 0){

                    $row1->total = $total;

                    $over_used = $row1->total - $row1->mc_opens;
    
                    DB::table('master_campaign')
                           ->where("master_campaign.mc_id", $row1->mc_id)
                           ->update(["master_campaign.mc_bonus" => $over_used]); 
                           
                    DB::table('master_campaign_detail')
                        ->where('mc_id', $mc_id)
                        ->update(['balance' => 0]);  

                    echo 1;
               }else{
                    echo 0;
               }
            }

            




    }
    public function getOrders(Request $request){

        $email = $request->input('email');
        
        $response = DB::table('master_campaign')
                ->select('master_campaign.mc_order_id')
                ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                ->where('mt_users.mu_username',$email)->distinct()->orderBy('mc_order_id', 'asc')
                ->get();

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),                
            'status_code' => 200
        ));
    }
    public function createMaterCampaign(Request $request)
    {
        $email = $request->input('email');

        // master campaign
        $name = $request->input('name');

        // $user_id = $request->input('user_id');
        
        $order_id = $request->input('order_id');

        $product_id = $request->input('product_id');

        
        $order_hash = md5(uniqid(rand(), true));


        $unique_id = $request->input('unique_id');

        $opens = $request->input('opens');

        $industry = $request->input('industry');

        $ad = $request->input('ad');

        $bonus = $request->input('bonus');

        $auto_top_up = $request->input('auto_top_up');

        $set_cap = $request->input('set_cap');

        $order_code = $request->input('order_code');
        

        if(!isset($industry)){
            $industry = 0;
        }
        if(!isset($ad)){
            $ad = 0;
        }

        if(!isset($auto_top_up)){
            $auto_top_up = 0;
        }
        // var_dump($request);die();

        // $clicks = $request->input('clicks');

        // $response = DB::table('master_campaign')
        //                 ->select('master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_created_date')
        //                 ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
        //                 ->where('mt_users.mu_username',$email)
        //                 ->where('master_campaign.mc_order_hash',$unique_id)
        //                 ->get();

        if($auto_top_up == 1){
            $bonus = ($opens * 10) / 100;
        }else{
            $bonus = 0;
        }
        
                        

        // if($order_id != 0 && count($response) > 0){
        //     return Response::json(array(
        //         'error' => true,
        //         'response' => 'The order is exist',
        //         'status_code' => 200
        //     ));
        // }

        $user_id = DB::table('mt_users')
                    ->where('mt_users.mu_username',$email)   
                    ->get()[0]->mu_id;
   
        if($product_id != "55"){
            $mc_type = 0;
        }else{
            $mc_type = 1;
        }
        $mc_id = DB::table("master_campaign")->insertGetId([
            // 'mc_id' => null,
            'mc_name' => $name,
            'mc_company_id' => $user_id,
            'mc_order_id' => $order_id,
            'mc_order_hash' => $order_hash,
            'mc_opens' => $opens,
            'mc_bonus' => $bonus,              
            'mc_industry' => (int)$industry,
            'mc_auto_top_up' => $auto_top_up,
            'mc_type' => $mc_type,
            'mc_set_cap' => $set_cap
        ]);

        if($product_id != "55"){

                    $link_name = $request->input('link_name');
                    $link_address = $request->input('link_address');

                    $ad = $request->input('ad');

                    $max_cap = $request->input('number_of_opens');

                    // $email_service_tag = $request->input('email_service_tag');

                    if(!isset($max_cap)){
                        $max_cap = 0;
                    }

                    $li_id = DB::table("Linkly")->insertGetId([
                        // 'li_id' => null,
                        'li_name' => $link_name,
                        'li_url' => $link_address,
                        // 'li_url_ios' => '',
                        // 'li_url_android' => '',
                        'li_unique' => $this->randString(7),
                        // 'li_sid' => 1,    
                        'li_set_cap' => $max_cap,
                        'li_ad' => $ad,
                        'li_service_tag' => ''
                    ]);

                    // return Response::json(array(
                    //     'error' => false,
                    //     'response' => $li_id,                
                    //     'status_code' => 200
                    // ));

                    DB::table("master_campaign_link")->insert([
                        // 'mc_id' => null,
                        'mcl_li_id' => $li_id,
                        'mcl_mc_id' => $mc_id
                    ]);

        }else{

                    $email_name = $request->input('email_name');
                    $email_service = $request->input('email_service');
                    $email_type = $request->input('email_type');

                    $rate_list = $request->input('rate_list');
                    $open_rate = $request->input('open_rate');
                    $number_of_opens = $request->input('number_of_opens');
                    $number_of_people = $request->input('number_of_people');

                    $email_service_tag = $request->input('email_service_tag');

                    

                    $_camp_code = $this->randString(7);

                    // $url = 'https://www.mwiseapps.com/trackingpixel/Tracker/track';   
                    $url = 'https://www.msh.ai/Tracker/track';         
   
                    
                    $tracking_code = "<style data-ignore-inlining> @media print{ #_t { background-image: url('$url/p/$_camp_code?e=$email_service_tag');}} 
                                        div.OutlookMessageHeader {background-image:url('$url/f/$_camp_code?e=$email_service_tag')} 
                                        table.moz-email-headers-table {background-image:url('$url/f/$_camp_code?e=$email_service_tag')} 
                                        blockquote #_t {background-image:url('$url/f/$_camp_code?e=$email_service_tag')} 
                                        #MailContainerBody #_t {background-image:url('$url/f/$_camp_code?e=$email_service_tag')}</style> 
                                        <div id='_t'></div> <img src='$url/o/$_camp_code?e=$email_service_tag' width='0' height='0'  style='width:1px;height:1px;opacity:0.5;position:absolute; visibility:hidden;display:none' alt='IMG'/>";   
                    
                    if($this->checkCampCodeExist( $_camp_code) == 0)
                    {
            
                            $email_id = DB::table("Campaign")->insertGetId([
                                'Camp_Code_Id' => null,
                                'Camp_Code' => $_camp_code,
                                'Camp_Name' => $email_name,
                                'Tracking_Code' => $tracking_code,
                                'Camp_People_Sending' => $number_of_people,
                                'Camp_Open_Rate' => $open_rate,
                                'Camp_Tracked_Opens' => $number_of_opens,
                                'Camp_Email_Service' => $email_service,
                                'Camp_Email_Type' => $email_type,
                                'Camp_Rate_List' => $rate_list
                            ]);

                            DB::table("master_campaign_trackingpixel")->insert([
                                // 'mc_id' => null,
                                'mctp_mc_id' => $mc_id,
                                'mctp_mc_track_id' => $email_id
                            ]);
                            $links = $request->input('links');
                            if(count($links) != 0){


                                for ($i=0; $i < count($links); $i++) { 
                                    // $link_name = $links[$i];
                                    // $link_address = $links[$i];
                                    $link =  explode("***",$links[$i]);  
                                    $link_name = $link[0];
                                    $link_address = $link[1];
                
                                    $li_id = DB::table("Linkly")->insertGetId([
                                        // 'li_id' => null,
                                        'li_name' => $link_name,
                                        'li_url' => $link_address,
                                        // 'li_url_ios' => '',
                                        // 'li_url_android' => '',
                                        'li_unique' => $this->randString(7),
                                        // 'li_sid' => 1,
                                        'li_service_tag' => $email_service_tag
                                    ]);
                
                                    // return Response::json(array(
                                    //     'error' => false,
                                    //     'response' => $li_id,                
                                    //     'status_code' => 200
                                    // ));
                
                                    DB::table("master_campaign_link")->insert([
                                        // 'mc_id' => null,
                                        'mcl_li_id' => $li_id,
                                        'mcl_mc_id' => $mc_id
                                    ]);
                                }

                            }

                    }else{

                            return Response::json(array(
                                'error' => true,
                                'message' => 'The code is exist',
                                'status_code' => 200
                            ));
                    }

            
        }

        $master_camp = DB::table("master_campaign")->where('mc_id',$mc_id)->get();

        foreach ($master_camp as $row) {
            $date = date_create($row->mc_created_date);
            $date =  date_format($date,"d.m.y");

            $row->mc_created_date = $date;
        }

        $order = DB::table("order")
                ->select("id")
                ->where("order_code", $order_code)->get();

        if(count($order) > 0){
            foreach ($order as $row) {
                $order_id = $row->id;
            }
            
        }else{
            $order_id = DB::table("order")->insertGetId([
                "order_code" => $order_code
            ]);
        } 
        
        DB::table("campaign_order")->insert([
            "order_id" => $order_id,
            "camp_id" => $mc_id
        ]); 
        



        $response = array(
            'order_hash' => $order_hash,
            'master_camp' => $master_camp
        );

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),                
            'status_code' => 200
        ));
        
    }

    public function get_Order_Code(Request $request){

        $camp_id = $request->input('camp_id');

        $order_code = DB::table('order')
                    ->select("order.order_code")
                    ->join("campaign_order","campaign_order.order_id","=","order.id")
                    ->join("master_campaign","master_campaign.mc_id","=","campaign_order.camp_id")
                    ->where("campaign_order.camp_id", $camp_id)
                    ->get();

                    // $order_code = DB::table('order')->get();            
                    // $order_code = array("sdfsdf");
        return Response::json(array(
            'error' => false,
            'response' => json_encode($order_code),                
            'status_code' => 200
        ));
    }

    public function end_campaign(Request $request){

        $camp_id = $request->input('camp_id');
        $camp_type = $request->input('camp_type');

        DB::table('master_campaign')
                        ->where("mc_id", $camp_id)
                        ->update(["mc_end" => 1]);  

    } 

    public function update_Campaign(Request $request){

        $camp_id = $request->input('camp_id');
        $camp_name = $request->input('camp_name');
        $industry = $request->input('industry');

        DB::table('master_campaign')
                        ->where("mc_id", $camp_id)
                        ->update(["mc_name" => $camp_name, 'mc_industry' => $industry]);  

        $mc_type = $request->input('mc_type');

        if((int)$mc_type == 1){

                $camp_code_id = $request->input('camp_code_id');
                $email_type = $request->input('email_type');
                $rate_list = $request->input('rate_list');
                $link_array = $request->input('link_array');
                
        
                DB::table('Campaign')
                    ->where("Camp_Code_Id", $camp_code_id)
                    ->update([
                        "Camp_name" => $camp_name,
                        "Camp_Email_Type" => $email_type, 
                        'Camp_Rate_List' => $rate_list]);
                    // var_dump($link_array);
                for ($i=0; $i < count($link_array); $i++) { 
                    $link_id = $link_array[$i]['link_id'];
                    $link_name = $link_array[$i]['link_name'];
                    $link_address = $link_array[$i]['link_address'];
        
                    DB::table('Linkly')
                        ->where("li_id", $link_id)
                        ->update(["li_name" => $link_name, 'li_url' => $link_address]);  
                }

        }else{
            
            $ad_type = $request->input('ad_type');
            $link_id = $request->input('link_id');
            $link_name = $request->input('link_name');
            $link_address = $request->input('link_address');

            DB::table('Linkly')
                        ->where("li_id", $link_id)
                        ->update([
                            "li_name" => $link_name, 
                            'li_url' => $link_address,
                            'li_ad' => $ad_type
                            ]);  
                

        }

        



    }

    public function getIndustry(){

        $response = DB::table('Industry')->orderBy('ind_name', 'asc')->get();

        return Response::json(array(
        'error' => false,
        'response' => json_encode($response),
        'status_code' => 200
        ));
    }

    public function getMasterCampaign($id){
                        
                $response = DB::table('master_campaign')
                                ->select('master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_created_date')
                                ->where('master_campaign.mc_id',$id)
                                ->get();
        
                return Response::json(array(
                    'error' => false,
                    'response' => json_encode($response),
                    'status_code' => 200
                ));
    }

    public function getOpensCampaignsSubscriptions(Request $request){

            $email = $request->input('email');

            $response = DB::table('master_campaign')
                    ->select('master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_created_date','master_campaign.mc_order_id','master_campaign.mc_order_hash','master_campaign.mc_opens')
                    ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                    ->where('mt_users.mu_username',$email)
                    ->where('master_campaign.mc_order_id',0)
                    ->get();

            $total = 0;
            foreach ($response as $row) {
                $total += (int)$row->mc_opens;
            }

            return Response::json(array(
                'error' => false,
                'response' => $total,
                'status_code' => 200
            ));

    }

    public function getMasterCampaigns(Request $request){

        $email = $request->input('email');
            
        $response = DB::table('master_campaign')
                        ->select('master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_created_date','master_campaign.mc_order_id','master_campaign.mc_industry','master_campaign.mc_order_hash','master_campaign.mc_opens','master_campaign.mc_bonus','master_campaign.mc_auto_top_up','master_campaign.mc_type', 'master_campaign.mc_set_cap', 'master_campaign.mc_end', 'master_campaign.mc_html_heat_map', 'master_campaign.mc_is_sent_email')    
                        ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                        ->where('mt_users.mu_username',$email)
                        // ->limit(15)
                        ->orderBy('master_campaign.mc_created_date','asc')
                        ->get();

        // usort($data, function($a, $b)
        // {
        //     $date1 = strtotime($a->mc_created_date);
        //     $date2 = strtotime($b->mc_created_date);
        //     if ($date1 < $date2) return 1;
        //     if ($date1 == $date2) return 0;
        //     if ($date1 > $date2) return -1;
        // });

        $total_credits = $request->input('total_credits');
        
        $new_remaining_credits = $total_credits;
        
         $result = DB::table('mt_users')
                        ->select("verify_email", "mu_created_date", 'is_sent_email')
                        ->where('mt_users.mu_username',$email)
                        ->get();

        // $verify_email  =    $result[0]->verify_email;

        // $mu_created_date  =    $result[0]->mu_created_date;
        
        $verify_email  =    $result[0]->verify_email;

        $mu_created_date  =    $result[0]->mu_created_date;
        
        $is_sent_email  =    $result[0]->is_sent_email;  

        $current_date = date('Y-m-d H:i:s');
        $current_date = new \DateTime($current_date);


        $to = date('Y-m-d H:i:s', StrToTime ( $mu_created_date . ' + 1 MONTH'));
        $to = new \DateTime($to); 


        $new_total_credits = 0;
        
        $is_1month_free = 0;
        if($verify_email != NULL && $verify_email != "" && $verify_email == "1monthfree" && $to >= $current_date){
            
            $special_val = DB::table('special_event')
                        ->select("e_value")
                        ->where('e_code', $verify_email)
                        ->get();
                        
            $total_credits += (int)$special_val[0]->e_value - 1000;   
            
            
            
            
            // $total_credits += 999000; 
            
            
            $is_1month_free = 1;
        }  
        if($verify_email != NULL && $verify_email != "" && $verify_email == "1monthfree" && $current_date > $to){
            
           
                        
            $total_credits += 1000;   

        } 
        
        
        
        // $new_remaining_credits = $total_credits;
        
        
        
        $new_total_credits = $total_credits;
        
        
        // $is_sent_email  =    $result[0]->is_sent_email;
        
        $missed_campaigns = array();

        foreach ($response as $row1) {

            $total_opens_assigned = DB::table('master_campaign_detail')
                    ->select(DB::raw('SUM(balance) as total_opens_assigned'))
                    ->where('mc_id', $row1->mc_id)
                    ->groupBy('mc_id')
                    ->get();
            if(count($total_opens_assigned) == 0){
                $row1->total_opens_assigned = 0;
            }else{
                $row1->total_opens_assigned = $total_opens_assigned[0]->total_opens_assigned;
            }
           

            $ids = DB::table('master_campaign')
                    ->select('Campaign.Camp_Code_Id')
                    ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                    ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                    ->where('master_campaign.mc_id', $row1->mc_id)
                    ->get();
            $total = 0;        
            foreach ($ids as $row2) {
                $total += DB::table('Sent_Mail_Detail')
                        ->select(DB::raw('COUNT(Email) as Opens'))
                        ->where('Camp_Code_Id',$row2->Camp_Code_Id)->get()[0]->Opens ;
            }
            if(count($ids) > 0){
                $row1->email_id = $ids[0]->Camp_Code_Id;

                $launched_date = DB::table('Sent_Mail')
                        ->select('Open_Time')
                        ->where('Camp_Code_Id',$ids[0]->Camp_Code_Id)
                        ->limit(1)->get();

                if(count($launched_date) > 0){
                    $row1->mc_created_date = $launched_date[0]->Open_Time;
                }
                // $row1->launched_date = $launched_date[0]->Open_Time;

            }else{
                $ids = DB::table('master_campaign')
                    ->select('Linkly.li_id')
                    ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
                    ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
                    ->where('master_campaign.mc_id', $row1->mc_id)
                    ->get();

                $row1->link_id = $ids[0]->li_id;

                $launched_date = DB::table('linkly_details')
                        ->select('ld_timestamp')
                        ->where('ld_id',$ids[0]->li_id)
                        ->limit(1)->get();

                if(count($launched_date) > 0){
                    $row1->mc_created_date = $launched_date[0]->ld_timestamp;
                }
            }


            $ids = DB::table('master_campaign')
                    ->select('Linkly.li_id')
                    ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
                    ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
                    ->where('master_campaign.mc_id', $row1->mc_id)
                    ->get();

            $row1->total_link = count( $ids);

            // if($row1->total_link != 0){
            //     $row1->broken_links = $this->get_broken_link_list($row1->mc_id);
            // }else{
            //     $row1->broken_links = array();
            // }

            
            

            foreach ($ids as $row2) {
                $total += DB::table('linkly_details')
                        ->select(DB::raw('COUNT(ld_uid) as Opens'))
                        ->where('ld_id',$row2->li_id)->get()[0]->Opens ;
                
                
            }

            $row1->total =  $total;  
            
            // if($is_1month_free != 1 && $total > $total_credits && $row1->mc_set_cap == 1){
            //     $row1->status = 0;
            // }else{
            //      $row1->status = 1;
                 
            //      $total_credits -= $total;
            // }
            
            // if($verify_email != NULL && $verify_email != "" && $is_1month_free == 0 && $row1->mc_set_cap == 1){
            //     $total_credits += (int)$row1->total_opens_assigned + (int)$row1->mc_opens;
            //     $new_total_credits += (int)$row1->total_opens_assigned + (int)$row1->mc_opens;
                
            // }    
            
            $over_used = 0;
            if($row1->mc_set_cap == 0 && $row1->mc_opens == 0){

                 $over_used = $row1->total - $row1->mc_opens;

                 if($total_credits >= $over_used){
                    
                    $total_credits -= $over_used;

                 }else{
                     if($total_credits > 0){
                        $over_used = $total_credits;
                        $total_credits = 0;
                     }
                     else{
                        $over_used = 0;
                     }
                     
                     if($verify_email != NULL && $verify_email != "" && $is_1month_free != 1){
                             DB::table('master_campaign')
                        ->where("master_campaign.mc_id", $row1->mc_id)
                        ->update(["master_campaign.mc_bonus" => $over_used]); 
                     }
                         
                    
                }


                //  $per_jump = ($row1->mc_opens * 10 / 100);

                //  $x_times = (int)($over_used / $per_jump);
                 
                //  $over_used = (int)(($x_times + 1) * $per_jump);

                //  $row1->mc_opens = $over_used + $row1->mc_opens;

                if(isset($total_credits) && $total_credits > 0){
                    DB::table('master_campaign')
                        ->where("master_campaign.mc_id", $row1->mc_id)
                        ->update(["master_campaign.mc_bonus" => $over_used]);  
                }

                 
            }
            
            if($row1->mc_set_cap == 1 && $row1->total > $row1->total_opens_assigned + $row1->mc_opens && $row1->mc_end == 0 && $row1->mc_is_sent_email == 0){
                    
                array_push($missed_campaigns, $row1);
            }
            
            $new_remaining_credits = $new_remaining_credits - ((int)$row1->total_opens_assigned + (int)$row1->mc_opens + (int)$row1->mc_bonus);

        }

        // usort($response, function($a, $b)
        // {
        //     $date1 = strtotime($a->mc_created_date);
        //     $date2 = strtotime($b->mc_created_date);
        //     if ($date1 < $date2) return -1;
        //     if ($date1 == $date2) return 0;
        //     if ($date1 > $date2) return 1;
        // });
           
        // if($total_credits == 0){  
           
        // }else{
        //     DB::table('mt_users')
        //     ->where('mt_users.mu_username',$email)
        //      ->update(['is_sent_email' => 0]);
        // }
        
        // DB::table('mt_users')
        //     ->where('mt_users.mu_username',$email)
        //      ->update(['re_credits' => $new_remaining_credits]);

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
             'new_total_credits' => $new_total_credits,  
            'is_sent_email' => $is_sent_email,
            'missed_campaigns' => $missed_campaigns,
            'remaining_credits' => $new_remaining_credits,
            'status_code' => 200
        ));
    }
    
    public function update_is_sent_email(Request $request){
        
        $email = $request->input('email');
        
         DB::table('mt_users')
                ->where('mt_users.mu_username',$email)   
                 ->update(['is_sent_email' => 1]);
    }
    
    
    public function update_is_sent_email_campaigns(Request $request){
        $mc_ids = $request->input('mc_ids');
        
        for ($i=0; $i < count($mc_ids); $i++) { 
               DB::table('master_campaign')
                        ->where("master_campaign.mc_id", $mc_ids[$i])
                        ->update(["master_campaign.mc_is_sent_email" => 1]); 
        }
    }
    
    public function update_credits_for_starter(Request $request){
        $email = $request->input('email');

        // $email = 'gumbuya@merchantwise.com';
        $list_subs =  json_decode($request->input('list_subs'));
        
        // $list_subs = json_decode('[{"date_created":"2018-03-15 00:07:44","month":"11","year":"2017","amount":"25000","subtype":0}]');

        $amount_deduct = 0;
        $current_date = date('Y-m-d H:i:s');
        $current_date = new \DateTime($current_date);
        $two;
        for ($i=0; $i < count($list_subs); $i++) {

                $from = $list_subs[$i]->date_created;

                $subtype = $list_subs[$i]->subtype;

                if($subtype == 0){

                    $to = date('Y-m-d H:i:s', StrToTime ( $list_subs[$i]->date_created . ' + 1 MONTH'));      

                    // $to = strtotime("+1 MONTH", strtotime($list_subs[$i]->date_created));
                    // $to = date('Y-m-d H:i:s', StrToTime ( $to ));      
                    $to = $two = new \DateTime($to);  
                    $to = $to->format('Y-m-d H:i:s');
                }
                if($subtype == 1){
                    $to = date('Y-m-d H:i:s', StrToTime ( $list_subs[$i]->date_created . ' + 1 YEAR'));  
                    // $to = strtotime("+1 YEAR", strtotime($list_subs[$i]->date_created));
                    // $to = date('Y-m-d H:i:s', StrToTime ( $to ));      
                    $to = $two = new \DateTime($to);  
                    $to = $to->format('Y-m-d H:i:s');
                }

                // var_dump($from);var_dump($to);

                $ids = DB::table('master_campaign')
                    ->select('Campaign.Camp_Code_Id')
                    ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                    ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                    ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                    ->where('mt_users.mu_username',$email)
                    ->get();

                $total = 0;        
                foreach ($ids as $row2) {
                    $total += DB::table('Sent_Mail_Detail')
                            ->select(DB::raw('COUNT(Email) as Opens'))
                            ->where('Camp_Code_Id',$row2->Camp_Code_Id)
                            ->whereBetween('Sm_Open_Time', array($from, $to))
                            ->get()[0]->Opens ;

                }

                $ids = DB::table('master_campaign')
                            ->select('Linkly.li_id')
                            ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
                            ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
                            ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                            ->where('mt_users.mu_username',$email)
                            ->get();

                foreach ($ids as $row2) {
                    $total += DB::table('linkly_details')
                            ->select(DB::raw('COUNT(ld_uid) as Opens'))
                            ->where('ld_id',$row2->li_id)
                            ->whereBetween('ld_timestamp', array($from, $to))
                            ->get()[0]->Opens ;

                }

                $amount = $list_subs[$i]->amount;

                if($total < $amount && $current_date > $two){
                    $amount_deduct += (int)$amount - (int)($total);
                }

                $total = 0;
        }

        echo $amount_deduct;
    }
    

    public function createCampaign(Request $request){

        $name = $request->input('name');
        

        $_camp_code = $this->randString(7);

        $tracking_code = "<style data-ignore-inlining> @media print{ #_t { background-image: url('https://www.mwiseapps.com/trackingpixel/Tracker/track/p/$_camp_code?e=[email]');}} 
                            div.OutlookMessageHeader {background-image:url('https://www.mwiseapps.com/trackingpixel/Tracker/track/f/$_camp_code?e=[email]')} 
                            table.moz-email-headers-table {background-image:url('https://www.mwiseapps.com/trackingpixel/Tracker/track/f/$_camp_code?e=[email]')} 
                            blockquote #_t {background-image:url('https://www.mwiseapps.com/trackingpixel/Tracker/track/f/$_camp_code?e=[email]')} 
                            #MailContainerBody #_t {background-image:url('https://www.mwiseapps.com/trackingpixel/Tracker/track/f/$_camp_code?e=[email]')}</style> 
                            <div id='_t'></div> <img src='https://www.mwiseapps.com/trackingpixel/Tracker/track/o/$_camp_code?e=[email]' style='width:1px;height:1px;opacity:0.5'/>";
        
        if($this->checkCampCodeExist( $_camp_code) == 0)
        {

            DB::table("Campaign")->insert([

                'Camp_Code_Id' => null,
                'Camp_Code' => $_camp_code,
                'Camp_Name' => $name,
                'Tracking_Code' => $tracking_code

            ]);

            return Response::json(array(
                'error' => false,
                'message' => 'The campaign is sucessully created',                
                'status_code' => 200
            ));

        }else{
            return Response::json(array(
                'error' => true,
                'message' => 'The code is exist',
                'status_code' => 200
            ));
        }
        
    }

    public function getCampaign($id){

        $response = DB::table("Campaign")
                    ->join("master_campaign_trackingpixel", "master_campaign_trackingpixel.mctp_mc_track_id","=","Campaign.Camp_Code_Id")  
                    ->where('Camp_Code_Id', $id)->get();

        // $campaign = App\Campaign::all();

        $main_data = DB::select( DB::raw("Select (min(open_time)) as start, (max(open_time)) as end, DATEDIFF(  max(open_time), min(open_time)) as diff from `Sent_Mail` where `Camp_Code_Id` = :id"), array('id' => $id));

        $basic = $main_data[0];

        // return Response::json(array(
        //     'error' => false,
        //     'response' => json_encode($basic),                   
        //     'status_code' => 200
        // ));
        

        if($basic->start != null){
            $date = $basic->start; 
            
            foreach ($response as $row) {
           
                $UTC = new \DateTimeZone("Australia/Sydney");     
                $newTZ = new \DateTimeZone("America/New_York");
                $date = new \DateTime($date, $newTZ  );     
                $date->setTimezone( $UTC);    
                $date = $date->format("Y-m-d H:i:s");        
                
                $row->Created_Date = $date;        
            }
        }

        

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
    }

    public function getCampaign_2($id){

        $response = DB::table("Campaign")
                    ->join("master_campaign_trackingpixel", "master_campaign_trackingpixel.mctp_mc_track_id","=","Campaign.Camp_Code_Id")  
                    ->where('Camp_Code_Id', $id)->get();

        // $campaign = App\Campaign::all();

        $main_data = DB::select( DB::raw("Select (min(open_time)) as start, (max(open_time)) as end, DATEDIFF(  max(open_time), min(open_time)) as diff from `Sent_Mail` where `Camp_Code_Id` = :id"), array('id' => $id));

        $basic = $main_data[0];

        // return Response::json(array(
        //     'error' => false,
        //     'response' => json_encode($basic),                   
        //     'status_code' => 200
        // ));
        

        if($basic->start != null){
            $date = $basic->start; 
            
            foreach ($response as $row) {
           
                $UTC = new \DateTimeZone("Australia/Sydney");     
                $newTZ = new \DateTimeZone("America/New_York");
                $date = new \DateTime($date, $newTZ  );     
                $date->setTimezone( $UTC);    
                $date = $date->format("Y-m-d H:i:s");        
                
                $row->Created_Date = $date;        
            }
        }

        

        return $response;
    }
    public function getEmailTrackers_New(Request $request){

        // $email = $request->input('email');
        
        $id = $request->input('camp_id');

        
                $response = DB::table('master_campaign')
                                ->select('Campaign.Camp_Code_Id','Campaign.Camp_Name','Campaign.Camp_Email_Service','Campaign.Camp_Email_Type','Campaign.Camp_Rate_List','master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_industry','master_campaign.mc_type')
                                ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                                ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                                ->where('master_campaign.mc_id', $id)
                                ->get();


                $response2 = DB::table('master_campaign')
                            ->select('Linkly.li_id', 'Linkly.li_name', 'Linkly.li_url', 'Linkly.li_unique')
                            ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
                            ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
                            ->where('master_campaign.mc_id', $id)
                            // ->where('Linkly.li_sid',NULL)
                            ->get();
                $response[0]->links = $response2;
        
                return Response::json(array(
                    'error' => false,
                    'response' => json_encode($response),
                    'status_code' => 200
                ));
        
                // return $campaigns;
    }

    public function getEmailTrackers(Request $request){

        // $email = $request->input('email');
        
        $id = $request->input('camp_id');

        // $mc_ids = DB::table('master_campaign')
        //             ->select('master_campaign.mc_id')
        //             ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
        //             ->where('mt_users.mu_username',$email)
        //             ->get();
            // $mc_ids = DB::table('master_campaign')
            // ->select('master_campaign.mc_id')
            // ->where('master_campaign.mc_order_id', $order_id)
            // ->get();

            $list_mc = array();

            // foreach ($mc_ids as $row) {
                
            //     $id = $row->mc_id;
        
        
                $response = DB::table('master_campaign')
                                ->select('Campaign.Camp_Code_Id', 'Campaign.Camp_Name', 'Campaign.Tracking_Code')
                                ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                                ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                                ->where('master_campaign.mc_id', $id)
                                ->get();
                                

                                if(count($response) != 0){
                                    array_push($list_mc, $response);
                                }

                                // array_push($list_mc, $response);
                            // }
        
                return Response::json(array(
                    'error' => false,
                    'response' => json_encode($list_mc),
                    'status_code' => 200
                ));
        
                // return $campaigns;
    }

    public function getCampaigns(Request $request){

        $email = $request->input('email');

        $response = DB::table('master_campaign')
                        ->select('Campaign.Camp_Code_Id','Campaign.Camp_Name','Campaign.Tracking_Code','Created_Date')
                        ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                        ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                        ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                        ->where('mt_users.mu_username', $email)
                        ->get();

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));

        // return $campaigns;
    }

    public function getLastestCamp($email){

        return $response = DB::table('master_campaign')
                ->select('Campaign.Camp_Code_Id','Campaign.Camp_Name','Campaign.Tracking_Code','Created_Date')
                ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                ->where('mt_users.mu_username', $email)
                ->limit(4)
                ->orderBy('Created_Date','desc')
                ->get();
    } 
    public function getLastestCamp_2($email, $camp_id){

        return $response = DB::table('master_campaign')
                ->select('Campaign.Camp_Code_Id','Campaign.Camp_Name','Campaign.Tracking_Code','Created_Date')
                ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                ->where('mt_users.mu_username', $email)
                ->where('Campaign.Camp_Code_Id',"<", $camp_id)
                ->limit(3)
                ->orderBy('Created_Date','desc')
                ->get();
    }
    public function getLastestCamp_3($email, $camp_id){

        return $response = DB::table('master_campaign')
                ->select('Campaign.Camp_Code_Id','Campaign.Camp_Name','Campaign.Tracking_Code','Created_Date')
                ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                ->where('mt_users.mu_username', $email)
                ->where('Campaign.Camp_Code_Id',"<=", $camp_id)
                ->limit(4)
                ->orderBy('Created_Date','desc')
                ->get();
    }  

    public function getLatestCampaigns(Request $request){
        
                $email = $request->input('email');
        
                $camp_id = $request->input('camp_id');

                // $response = DB::table('master_campaign')
                //                 ->select('Campaign.Camp_Code_Id','Campaign.Camp_Name','Campaign.Tracking_Code','Created_Date')
                //                 ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                //                 ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                //                 ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                //                 ->where('mt_users.mu_username', $email)
                //                 ->limit(4)
                //                 ->orderBy('Created_Date','desc')
                //                 ->get();

                $response = $this->getLastestCamp_2($email, $camp_id);

                foreach ($response as $row) {
            
                    $query = DB::table('Sent_Mail_Detail')
                                ->select(DB::raw('COUNT(Email) as Opens'))
                                ->where('Camp_Code_Id',$row->Camp_Code_Id)->get();
        
                                $row->Total_Opens =  $query[0]->Opens;
        
        
                                $row->Reads = $this->getReadingTime($row->Camp_Code_Id, null);
                }
        
                return Response::json(array(
                    'error' => false,
                    'response' => json_encode($response),
                    'status_code' => 200
                ));
        
                // return $campaigns;
    }
    public function getLatestCampaignsComparision(Request $request){
        
        $email = $request->input('email');

        $camp_id = $request->input('camp_id');

        $datezz = $request->input('datezz');

        // $response = DB::table('master_campaign')
        //                 ->select('Campaign.Camp_Code_Id','Campaign.Camp_Name','Campaign.Tracking_Code','Created_Date')
        //                 ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
        //                 ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
        //                 ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
        //                 ->where('mt_users.mu_username', $email)
        //                 ->limit(4)
        //                 ->orderBy('Created_Date','desc')
        //                 ->get();

        $response = $this->getLastestCamp_3($email, $camp_id);

        $count = 0;
                foreach ($response as $row) {

                                if($count == 0){
                                    $row->Reads = $this->getReadingTime($row->Camp_Code_Id, $datezz);
                                    if(isset($datezz)){
                                        $query = DB::table('Sent_Mail_Detail')
                                            ->select(DB::raw('COUNT(Email) as Opens'))
                                            ->where('Camp_Code_Id',$row->Camp_Code_Id)
                                            ->where('Sm_Open_Time',"<=",$datezz)
                                            ->get();

                                        $row->Total_Opens =  $query[0]->Opens;
                                    }else{
                                        $query = DB::table('Sent_Mail_Detail')
                                            ->select(DB::raw('COUNT(Email) as Opens'))
                                            ->where('Camp_Code_Id',$row->Camp_Code_Id)
                                            ->get();

                                        $row->Total_Opens =  $query[0]->Opens;
                                    }
                                    
                                }else{
                                    $row->Reads = $this->getReadingTime($row->Camp_Code_Id, null);
                                    $query = DB::table('Sent_Mail_Detail')
                                            ->select(DB::raw('COUNT(Email) as Opens'))
                                            ->where('Camp_Code_Id',$row->Camp_Code_Id)->get();

                                    $row->Total_Opens =  $query[0]->Opens;
                                }
                                $count++;
                }
        
       

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));

        // return $campaigns;
}
    
public function getAdTrackers_New(Request $request){

                $id = $request->input('camp_id');
    

                $response = DB::table('master_campaign')
                                ->select('Linkly.li_id', 'Linkly.li_name', 'Linkly.li_url', 'Linkly.li_unique', 'Linkly.li_ad', 'master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_industry','master_campaign.mc_type')  
                                ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
                                ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
                                ->where('master_campaign.mc_id', $id)
                                ->get();

    
            return Response::json(array(
                'error' => false,
                'response' => json_encode($response),
                'status_code' => 200
            ));
    
            // return $campaigns;
}

    public function getAdTrackers(Request $request){

        $id = $request->input('camp_id');
        

        // $mc_ids = DB::table('master_campaign')
        //             ->select('master_campaign.mc_id')
        //             ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
        //             ->where('mt_users.mu_username',$email)
        //             ->get();
                // $mc_ids = DB::table('master_campaign')
                //             ->select('master_campaign.mc_id')
                //             ->where('master_campaign.mc_order_id', $order_id)
                //             ->get();

                $list_mc = array();
                // foreach ($mc_ids as $row) {
                    
                //     $id = $row->mc_id;

                    $response = DB::table('master_campaign')
                                    ->select('Linkly.li_id', 'Linkly.li_name', 'Linkly.li_url', 'Linkly.li_unique','Linkly.li_service_tag')
                                    ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
                                    ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
                                    ->where('master_campaign.mc_id', $id)
                                    // ->where('Linkly.li_sid',NULL)
                                    ->get();

                    if(count($response) != 0){
                        array_push($list_mc, $response);
                    }
                   
                // }
    
        
                return Response::json(array(
                    'error' => false,
                    'response' => json_encode($list_mc),
                    'status_code' => 200
                ));
        
                // return $campaigns;
    }

    public function getLinks(Request $request){

        $email = $request->input('email');
        
        $response = DB::table('master_campaign')
                            ->select('Linkly.li_id', 'Linkly.li_name', 'Linkly.li_url', 'Linkly.li_unique')
                            ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
                            ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
                            ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                            ->where('mt_users.mu_username', $email)
                            ->where('Linkly.li_sid',NULL)
                            ->get();

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
        
    }

    
    public function getPopularEmailClients($id, Request $request){

        $datezz = $request->input("datezz");

        if($datezz != null){

            $response = DB::table('Sent_Mail_Detail')
                            ->select('Mail_Service.Mail_Service_Name as EmailClient', 
                                    DB::raw('COUNT(Sent_Mail_Detail.Mail_Service_Id) as Total_Opens'), 'Mail_Service.Mail_Service_Id')
                            ->where('Camp_Code_Id', $id)
                            ->where('Sm_Open_Time',"<=", $datezz)
                            ->where('Mail_Service.Mail_Service_Name', '!=', 'Other')
                            ->join("Mail_Service",'Mail_Service.Mail_Service_Id','=','Sent_Mail_Detail.Mail_Service_Id')
                            ->groupBy('Mail_Service_Name')
                            ->orderBy('Total_Opens','desc')
                            ->limit(10)
                            ->get();

            foreach ($response as $row) {
                $mail_client_id = (int)$row->Mail_Service_Id;
                
                $result = DB::table('Sent_Mail_Detail')
                            ->select(DB::raw("COUNT(Reading_Time) as total"))
                            ->where('Sm_Open_Time',"<=", $datezz)
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '<', 2],
                                ['Mail_Service_Id',"=",$mail_client_id]
                            ])->get();

                $row->deleted_glanced = $result[0]->total;

                $result = DB::table('Sent_Mail_Detail')
                            ->select(DB::raw("COUNT(Reading_Time) as total"))
                            ->where('Sm_Open_Time',"<=", $datezz)
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '>=', 2],
                                ['Reading_Time', '<=', 8],
                                ['Mail_Service_Id',"=",$mail_client_id]
                            ])->get();

                $row->skim = $result[0]->total;

                $result = DB::table('Sent_Mail_Detail')
                            ->select(DB::raw("COUNT(Reading_Time) as total"))
                            ->where('Sm_Open_Time',"<=", $datezz)
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '>', 8],
                                ['Mail_Service_Id',"=",$mail_client_id]   
                            ])->get();

                $row->read = $result[0]->total;  
            } 

        }else{
            $response = DB::table('Sent_Mail_Detail')
                            ->select('Mail_Service.Mail_Service_Name as EmailClient', 
                                    DB::raw('COUNT(Sent_Mail_Detail.Mail_Service_Id) as Total_Opens'), 'Mail_Service.Mail_Service_Id')
                            ->where('Camp_Code_Id', $id)
                            ->where('Mail_Service.Mail_Service_Name', '!=', 'Other')
                            ->join("Mail_Service",'Mail_Service.Mail_Service_Id','=','Sent_Mail_Detail.Mail_Service_Id')
                            ->groupBy('Mail_Service_Name')
                            ->orderBy('Total_Opens','desc')
                            ->limit(10)   
                            ->get();

            foreach ($response as $row) {
                $mail_client_id = (int)$row->Mail_Service_Id;
                
                $result = DB::table('Sent_Mail_Detail')
                            ->select(DB::raw("COUNT(Reading_Time) as total"))
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '<', 2],
                                ['Mail_Service_Id',"=",$mail_client_id]
                            ])->get();

                $row->deleted_glanced = $result[0]->total;

                $result = DB::table('Sent_Mail_Detail')
                            ->select(DB::raw("COUNT(Reading_Time) as total"))
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '>=', 2],
                                ['Reading_Time', '<=', 8],
                                ['Mail_Service_Id',"=",$mail_client_id]
                            ])->get();

                $row->skim = $result[0]->total;

                $result = DB::table('Sent_Mail_Detail')
                            ->select(DB::raw("COUNT(Reading_Time) as total"))
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '>', 8],
                                ['Mail_Service_Id',"=",$mail_client_id]
                            ])->get();

                $row->read = $result[0]->total;  
            }    
        }

        

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));

    }

    public function getUniquePopularEmailClients($id, Request $request){       

        $datezz = $request->input("datezz");

        if($datezz != null){

            $response = DB::table('Sent_Mail_Detail')
                            ->select('Mail_Service.Mail_Service_Name as EmailClient', 
                                    DB::raw('COUNT(DISTINCT(Sent_Mail_Detail.Email)) as Total_Opens'))
                            ->where('Camp_Code_Id', $id)
                            ->where('Sm_Open_Time',"<=", $datezz)
                            ->where('Mail_Service.Mail_Service_Name', '!=', 'Other')
                            ->join("Mail_Service",'Mail_Service.Mail_Service_Id','=','Sent_Mail_Detail.Mail_Service_Id')
                            ->groupBy('Mail_Service_Name')
                            ->orderBy('Total_Opens','desc')
                            ->limit(10)
                            ->get();

        }else{
            $response = DB::table('Sent_Mail_Detail')
                            ->select('Mail_Service.Mail_Service_Name as EmailClient', 
                                    DB::raw('COUNT(DISTINCT(Sent_Mail_Detail.Email)) as Total_Opens'))
                            ->where('Camp_Code_Id', $id)
                            ->where('Mail_Service.Mail_Service_Name', '!=', 'Other')
                            ->join("Mail_Service",'Mail_Service.Mail_Service_Id','=','Sent_Mail_Detail.Mail_Service_Id')
                            ->groupBy('Mail_Service_Name')
                            ->orderBy('Total_Opens','desc')
                            ->limit(10)
                            ->get();

        }

        

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));

    }

    public function getPopularCountries($id, Request $request){

        $datezz = $request->input("datezz"); 
        
        $type = $request->input("type"); 

        if($datezz != null){
            $response = DB::table('Sent_Mail_Detail')
                            ->select('Customer.Country', DB::raw('COUNT(Sent_Mail_Detail.Camp_Code_Id) as Total_Opens'))
                            ->join("Campaign",'Campaign.Camp_Code_Id', '=', 'Sent_Mail_Detail.Camp_Code_Id')
                            ->where('Campaign.Camp_Code_Id', $id)
                            ->where('Sm_Open_Time','<=',$datezz)
                            ->where('Country', '!=','')        
                            ->join("Customer",'Sent_Mail_Detail.Ip_Address', '=' ,'Customer.Ip_Address')
                            ->groupBy('Customer.Country')
                            ->orderBy('Total_Opens','desc')
                            ->limit(6)
                            ->get();

        }else{
            $response = DB::table('Sent_Mail_Detail')
                            ->select('Customer.Country', DB::raw('COUNT(Sent_Mail_Detail.Camp_Code_Id) as Total_Opens'))
                            ->join("Campaign",'Campaign.Camp_Code_Id', '=', 'Sent_Mail_Detail.Camp_Code_Id')
                            ->where('Campaign.Camp_Code_Id', $id)
                            ->where('Country', '!=','')        
                            ->join("Customer",'Sent_Mail_Detail.Ip_Address', '=' ,'Customer.Ip_Address')
                            ->groupBy('Customer.Country')
                            ->orderBy('Total_Opens','desc')
                            ->limit(6)
                            ->get();

               
        }

        

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
    }

    public function getDetails($id){

        $response = DB::table('Sent_Mail_Detail')
                            ->select('Sent_Mail_Detail.Email', 'Sent_Mail_Detail.Reading_Time' ,'Device_Types.Device_Name', 'Mail_Service.Mail_Service_Name', 'Customer.Country','Customer.City')
                            ->join("Device_Types",'Sent_Mail_Detail.Device_Type_Id', '=' ,'Device_Types.Device_Type_Id')
                            ->where('Camp_Code_Id', $id)
                            ->join("Customer",'Sent_Mail_Detail.Ip_Address', '=' ,'Customer.Ip_Address')
                            ->join("Mail_Service",'Mail_Service.Mail_Service_Id', '=', 'Sent_Mail_Detail.Mail_Service_Id')
                            ->limit(5)
                            ->get();

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));

    }

    public function getTotalReadingTime($id, $datezz)
    {
        if($datezz != null){

            $total = DB::table('Sent_Mail_Detail')->select("*")
                                            ->where('Camp_Code_Id', $id)
                                            ->where("Sm_Open_Time","<=", $datezz)
                                            ->get();

            return count($total); 

        }else{
            $total = DB::table('Sent_Mail_Detail')->select(DB::raw("COUNT(Reading_Time) as Total"))
                                            ->where('Camp_Code_Id', $id)
                                            ->get();

            return $total[0]->Total;
        }
        
        
         
    }

    public function getMailService($id){

        $total = $this->getTotalReadingTime($id, null);      

        $response = DB::table('Sent_Mail_Detail')
                            ->select('Mail_Service.Mail_Service_Name as EmailClient', 
                                    DB::raw('ROUND(COUNT(Sent_Mail_Detail.Mail_Service_Id) * 100 / '.$total.') as Percentage'),
                                    DB::raw('COUNT(Sent_Mail_Detail.Mail_Service_Id) as OpenTracked'))
                            ->where('Camp_Code_Id', $id)
                            ->join("Mail_Service",'Mail_Service.Mail_Service_Id', '=' ,'Sent_Mail_Detail.Mail_Service_Id')
                            ->groupBy('Mail_Service_Name')
                            ->orderBy('Percentage')
                            ->get();

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
    }

    public function getUserLocation($id, Request $request){

        $datezz = $request->input("datezz");

        $type = $request->input("type");

        

        if($datezz != null){
            $response = DB::table('Sent_Mail_Detail')
                        ->select('Customer.Ip_Address', 'Customer.City', 'Customer.Region_Name', 'Customer.Postcode', 
                                DB::raw("CONCAT(Customer.Country,' (',Customer.CountryCode,')') as Country"),
                                DB::raw('LOWER(Customer.CountryCode) as CountryCode'), 
                                DB::raw('COUNT(Sent_Mail_Detail.Camp_Code_Id) as Total_Opens'), 'Sent_Mail_Detail.Camp_Code_Id')
                        ->join("Campaign",'Campaign.Camp_Code_Id', '=', 'Sent_Mail_Detail.Camp_Code_Id')
                        ->where('Campaign.Camp_Code_Id', $id)  
                        ->where('Sm_Open_Time','<=', $datezz)      
                        ->join("Customer",'Sent_Mail_Detail.Ip_Address', '=' ,'Customer.Ip_Address')
                        ->groupBy('Customer.Country')
                        ->get();

            // if($type == 2){
            //     $response = DB::table('Sent_Mail_Detail')
            //             ->select('Customer.Ip_Address', 'Customer.City', 'Customer.Region_Name', 'Customer.Postcode', 
            //                     DB::raw("CONCAT(Customer.Country,' (',Customer.CountryCode,')') as Country"),
            //                     DB::raw('LOWER(Customer.CountryCode) as CountryCode'), 
            //                     DB::raw('COUNT(Sent_Mail_Detail.Camp_Code_Id) as Total_Opens'), 'Sent_Mail_Detail.Camp_Code_Id', 'Sent_Mail_Detail.Email')
            //             ->join("Campaign",'Campaign.Camp_Code_Id', '=', 'Sent_Mail_Detail.Camp_Code_Id')
            //             ->where('Campaign.Camp_Code_Id', $id)  
            //             ->where('Sm_Open_Time','<=', $datezz)      
            //             ->join("Customer",'Sent_Mail_Detail.Ip_Address', '=' ,'Customer.Ip_Address')
            //             ->groupBy('Customer.Country','Sent_Mail_Detail.Email')
            //             ->get();
            // }

        }else{
            $response = DB::table('Sent_Mail_Detail')
                        ->select('Customer.Ip_Address', 'Customer.City', 'Customer.Region_Name', 'Customer.Postcode', 
                                DB::raw("CONCAT(Customer.Country,' (',Customer.CountryCode,')') as Country"),
                                DB::raw('LOWER(Customer.CountryCode) as CountryCode'), 
                                DB::raw('COUNT(Sent_Mail_Detail.Camp_Code_Id) as Total_Opens'), 'Sent_Mail_Detail.Camp_Code_Id')
                        ->join("Campaign",'Campaign.Camp_Code_Id', '=', 'Sent_Mail_Detail.Camp_Code_Id')
                        ->where('Campaign.Camp_Code_Id', $id)  
                        ->join("Customer",'Sent_Mail_Detail.Ip_Address', '=' ,'Customer.Ip_Address')
                        ->groupBy('Customer.Country')
                        ->get();
            // if($type == 2){
            //     $response = DB::table('Sent_Mail_Detail')
            //             ->select('Customer.Ip_Address', 'Customer.City', 'Customer.Region_Name', 'Customer.Postcode', 
            //                     DB::raw("CONCAT(Customer.Country,' (',Customer.CountryCode,')') as Country"),
            //                     DB::raw('LOWER(Customer.CountryCode) as CountryCode'), 
            //                     DB::raw('COUNT(Sent_Mail_Detail.Camp_Code_Id) as Total_Opens'), 'Sent_Mail_Detail.Camp_Code_Id', 'Sent_Mail_Detail.Email')
            //             ->join("Campaign",'Campaign.Camp_Code_Id', '=', 'Sent_Mail_Detail.Camp_Code_Id')
            //             ->where('Campaign.Camp_Code_Id', $id)  
            //             // ->where('Sm_Open_Time','<=', $datezz)      
            //             ->join("Customer",'Sent_Mail_Detail.Ip_Address', '=' ,'Customer.Ip_Address')
            //             ->groupBy('Customer.Country','Sent_Mail_Detail.Email')
            //             ->get();
            // }
        }

        

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));

    }

    public function getReadingTimebySecond($id, Request $request){

            $datezz = $request->input("datezz");      

            if($datezz != null){

                $response = DB::select( DB::raw("SELECT Time_Tracking.time ,COUNT(Sent_Mail_Detail.Reading_Time) as 'Total' 
                                from  Time_Tracking  inner join Sent_Mail_Detail  
                                where Sent_Mail_Detail.Camp_Code_ID = :id AND Sent_Mail_Detail.Reading_Time >= Time_Tracking.time AND Sent_Mail_Detail.Sm_Open_Time <= :datezz
                                group by Time_Tracking.time"), array(
                                    'id' => $id,
                                    "datezz" => $datezz
                                ));

            }else{
                $response = DB::select( DB::raw("SELECT Time_Tracking.time ,COUNT(Sent_Mail_Detail.Reading_Time) as 'Total' 
                                            from  Time_Tracking  inner join Sent_Mail_Detail  
                                            where Sent_Mail_Detail.Camp_Code_ID = :id AND Sent_Mail_Detail.Reading_Time >= Time_Tracking.time 
                                            group by Time_Tracking.time"), array(
                                                'id' => $id
                                            ));
            }    

            if(count($response) == 0)
            {
                return Response::json(array(
                    'error' => false,
                    'response' => json_encode($response),
                    'status_code' => 200
                ));
            }
            
            $first = null;
            $percent = [];
            $temp = $response;
            foreach ($temp as $row)
            {
                if($first == null){
                    $first = $row->Total;
                }
                $temp_v = round($row->Total * 100 / $first);
                $percent[] = array('time' => $row->time, 'number' => $row->Total, 'percentage' => $temp_v);
            }

            return Response::json(array(
                'error' => false,
                'response' => json_encode($percent),
                'status_code' => 200
            ));

    }

    public function getReadingStatistics($id, Request $request){

        $datezz = $request->input("datezz");

        
            $glanced = $this->getDeletedGlanced($id, $datezz);
            $skim = $this->getSkimTime($id, $datezz);
            $read = $this->getReadingTime($id, $datezz);
        

        
        
		if($glanced == null) $glanced = 0;
		if($skim == null) $skim = 0;
        if($read == null) $read = 0;
        
        $response = array(
            'Glanced' => $glanced,
            'Skim' => $skim,
            'Read' => $read
        );

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));


    }
    
    public function getReadingStatistics_22($id){

        $datezz = null;

        
            $glanced = $this->getDeletedGlanced($id, $datezz);
            $skim = $this->getSkimTime($id, $datezz);
            $read = $this->getReadingTime($id, $datezz);
        

        
        
		if($glanced == null) $glanced = 0;
		if($skim == null) $skim = 0;
        if($read == null) $read = 0;
        
        
        return $read * 100 / ($glanced + $skim + $read);


    }

    public function create_html_file(Request $request){

        $html_file = $request->input("html_file");
        $file_name = $request->input("file_name");
        $mc_id = $request->input("mc_id");

        // file_put_contents("heat_map_folder/".$file_name,$html_file);


        DB::table('master_campaign')
                    ->where("mc_id", $mc_id)
                    ->update(["mc_html_heat_map" => $file_name]);


        
    }

    public function show_all_time_open(Request $request){

                $datezz = $request->input("datezz"); 

                $id = $request->input("camp_id"); 

                if($datezz != null){

                    $main_data = DB::select( DB::raw("Select (min(open_time)) as start, (max(open_time)) as end, DATEDIFF(  max(open_time), min(open_time)) as diff from `Sent_Mail` 
                                                            where `Camp_Code_Id` = :id AND `Open_Time` <= :datezz"), array('id' => $id, 'datezz' => $datezz));

                }else{
                    $main_data = DB::select( DB::raw("Select (min(open_time)) as start, (max(open_time)) as end, DATEDIFF(  max(open_time), min(open_time)) as diff from `Sent_Mail` 
                                                            where `Camp_Code_Id` = :id"), array('id' => $id));
                
                }

                $basic = $main_data[0];


                $start_first = $basic->start;
                
                $end_first = $basic->end;

                $start_first = date('Y-m-d H:i:s', StrToTime ( $start_first ));      

                $start_first = new \DateTime($start_first);    
                // $start_first = $start_first->setTime(8,0,0);     

                $start_first = $start_first->format("Y-m-d H:i:s");

                $end_first = date('Y-m-d H:i:s', StrToTime ( $end_first ));         

                $end_first = new \DateTime($end_first);    
                // $end_first = $end_first->setTime(8,0,0);          

                $end_first = $end_first->format("Y-m-d H:i:s");  
    
                $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 * 24 );  
    
    
                $interval = 1;
                // $steps = 3;   
                $steps = (int)($hours / $interval); 

                $i = 0;

                while (($t1 = strtotime("+1 MONTH", $t1)) <= $t2) {
                    $i++;
                }

                return Response::json(array(
                    'error' => false,
                    'response' => $i,        
                    'status_code' => 200   
                ));



    }

    public function openHourlyActivity($id, $val, $datess, Request $request){  

        $datezz = $request->input("datezz"); 
        
        $extra_where = "";
        
        if($val == 5){
            
            // add extra where for filtering date range FROM ----- TO
            
            $datess = $request->input("datess"); 
            $from_d = $datess["from_d"];
            $to_d = $datess["to_d"];
            
            $extra_where = " AND `Open_Time` >= ".$from_d." AND `Open_Time` <= ".$to_d."";  
            
            // return Response::json(array(
            //             'error' => false,
            //             'response' => $extra_where, 
            //             'status_code' => 200
            //         ));
        }

        if($datezz != null){

            $main_data = DB::select( DB::raw("Select (min(open_time)) as start, (max(open_time)) as end, DATEDIFF(  max(open_time), min(open_time)) as diff from `Sent_Mail` 
                                                    where `Camp_Code_Id` = :id AND `Open_Time` <= :datezz"), array('id' => $id, 'datezz' => $datezz));

        }else{
            $main_data = DB::select( DB::raw("Select (min(open_time)) as start, (max(open_time)) as end, DATEDIFF(  max(open_time), min(open_time)) as diff from `Sent_Mail` 
                                                    where `Camp_Code_Id` = :id"), array('id' => $id));
                                                    
            // if($val == 5){
                
            //     $main_data = DB::select( DB::raw("Select (min(open_time)) as start, (max(open_time)) as end, DATEDIFF(  max(open_time), min(open_time)) as diff from `Sent_Mail` 
            //                                         where `Camp_Code_Id` = :id AND `Open_Time` >= :from_d AND `Open_Time` <= :to_d"), array('id' => $id, 'from_d' => $from_d, 'to_d' => $to_d)); 
                                                    
           
                
            // }
        
        }

        
        $basic = $main_data[0];

        $long = $basic->diff;

        $steps = 15;      

        $interval = 4;        

        

        $init = 0;
        $start_first = $basic->start;
        
        $end_first = $basic->end;

        // return Response::json(array(
        //     'error' => false,
        //     'response' => json_encode($start_first),          
        //     'status_code' => 200        
        // ));
        
        
        

        $result = array();
		
        // $result[] = array('time' => $start_first, 'unique_opens' => 0); 

        // $datess= $request->input("datess"); 
        
        $main_data3 = null;

        if($val == 0){   

            // $start_first = date("d M", StrToTime(implode(" ", explode("_", $datess))));   

        //     return Response::json(array(
        //     'error' => false,
        //     'response' => json_encode($start_first),            
        //     'status_code' => 200   
        // ));  

        $datess11 = $request->input("datess"); 
            
            $datess = date('Y-m-d H:i:s', StrToTime ( $datess11 ));
                    $datess = new \DateTime($datess);    
                    $datess = $datess->format("Y-m-d");

            $start_first = date('Y-m-d H:i:s', StrToTime ( $start_first ));
            $start_first = new \DateTime($start_first);    
            $start_first = $start_first->format("Y-m-d");             
            

            if($datess == $start_first){
                $start_first = $request->input("datess");     
            }
        else{
            $datess = date('Y-m-d H:i:s', StrToTime ( $datess11 ));                

            $datess = new \DateTime($datess);    
            $datess = $datess->setTime(8,0,0);     

            $datess = $datess->format("Y-m-d H:i:s");

            $start_first = $datess;   
        }   

        $t1 = StrToTime ( $start_first );
        $t2 = date('Y-m-d H:i:s', StrToTime ( $start_first . ' + 1days'));      

        $t2 = new \DateTime($t2);    
        $t2 = $t2->setTime(8,0,0);        

        $t2 = $t2->format("Y-m-d H:i:s");


        // return Response::json(array(
        //     'error' => false,
        //     'response' => json_encode($t2),                     
        //     'status_code' => 200   
        // ));  

        
        $t2 = StrToTime ( $t2 );   

        $diff = $t2 - $t1;
        $hours = (int)$diff / ( 60 * 60 );   
         

        $interval = 1;

        $steps = (int)($hours / $interval);     
        $status = "hours";  
        $date_format = "h a"; 

        /// Get details email in each day

        // $start = date('Y-m-d H:i:s', strtotime($start_first));
        // $end = date('Y-m-d H:i:s', strtotime($start_first. ' + 1'."days"));

        // $main_data4 = DB::table('Sent_Mail')        
        //             ->select('Cust_Email_uniqid as email')
        //             ->where('Camp_Code_Id',$id)
        //             ->whereBetween('Open_Time',array($start, $end))
        //             ->get();  

        // $temp_date = new \DateTime($start);     
        // $temp_date = $temp_date->format("Y-m-d H:i:s");   

        // $file_name = "report_daily_emails".$id."_".$start.".csv";              
     
                
        // $file = fopen("report_file/".$file_name,"w"); 

        // fputcsv($file, array("Email"));       

        // for ($i=0; $i < count($main_data4); $i++) { 

        //     fputcsv($file, array($main_data3[$i]->email));         

        // }

        // fclose($file);     
        


        }else{

          

            if($val == 1){

                $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 );   
    
                $interval = 1;
    
                $steps = (int)($hours / $interval);    
                $status = " hours";
                
                $steps = 20;
                $date_format = "d M, h a";  
            }
            if($val == 3){
    
                $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 );   
    
                $interval = 8;   
    
                $steps = (int)($hours / $interval);    
                $status = "hours";  
                $date_format = "d M, h a";    
            }
            if($val == 2){

                $original_first = $start_first;

                $start_first = date('Y-m-d H:i:s', StrToTime ( $start_first ));      

                $start_first = new \DateTime($start_first);    
                // $start_first = $start_first->setTime(8,0,0);     

                $start_first = $start_first->format("Y-m-d H:i:s");

                $end_first = date('Y-m-d H:i:s', StrToTime ( $end_first ));         

                $end_first = new \DateTime($end_first);    
                // $end_first = $end_first->setTime(8,0,0);          

                $end_first = $end_first->format("Y-m-d H:i:s");  
    
                $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 * 24 );  
    
                $interval = 1;
                
                // if($hours > 15){      
                    
                //     $interval = round($hours/10);
                    
                // }
                // $steps = 3;  
                
                $steps = (int)($hours / $interval); 
                // $steps = $steps + 1;   

                
                // $steps = 10;
                $status = "days";
                $date_format = "d M Y";          
            }
            if($val == 4){
                $original_first = $start_first;

                $start_first = date('Y-m-d H:i:s', StrToTime ( $start_first ));      

                $start_first = new \DateTime($start_first);    
                // $start_first = $start_first->setTime(8,0,0);     

                $start_first = $start_first->format("Y-m-d H:i:s");

                $end_first = date('Y-m-d H:i:s', StrToTime ( $end_first ));         

                $end_first = new \DateTime($end_first);    
                // $end_first = $end_first->setTime(8,0,0);          

                $end_first = $end_first->format("Y-m-d H:i:s");  
    
                $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 * 24 );  
    
    
                $interval = 1;
                // $steps = 3;   
                $steps = (int)($hours / $interval); 

                $i = 0;

                while (($t1 = strtotime("+1 MONTH", $t1)) <= $t2) {
                    $i++;
                }
                
                $steps = $i;
                $status = "month";
                $date_format = "M Y";  
            }
            
            if($val == 5){
                
                $datess = $request->input("datess"); 
                $from_d = $datess["from_d"];
                $to_d = $datess["to_d"];
                
                $start_first = $from_d . " 00:00:00";
                $end_first = $to_d. " 00:00:00"; 
                
                 $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 * 24 );
                
                 $interval = 1;
                // $steps = 3;   
                $steps = (int)($hours / $interval); 
                
                // $steps = 10;
                $status = "days";
                $date_format = "d M Y";
                
                // return Response::json(array(  
                //         'error' => false,
                //         'response' => $start_first."----".$end_first, 
                //         'status_code' => 200
                //     ));
                
            }
        }
        
        

        for ($i=0; $i <= $steps; $i++) { 
             
			$future = 0;
            $future = $init + (int)$interval;
            
            // $start = date('Y-m-d H:i:s', strtotime($start_first. ' + '.$init.$status));
            if($val == 4){

                $start = date('Y-m-d H:i:s', strtotime($start_first. ' + '.$init.$status));

                $start = new \DateTime($start);  
                $month = $start->format('m');
                $year = $start->format('Y');

                $start = date("$year-$month-01 00:00:00");

                $start = date('Y-m-d H:i:s', strtotime($start));

                $end = date('Y-m-d H:i:s', strtotime($start. ' + '.$future.$status));

                // return Response::json(array(
                //     'error' => false,
                //     'response' => $start."---".$end, 
                //     'status_code' => 200
                // ));
                
                // $end = $end->modify('first day of this month')->format("Y-m-d H:i:s");
                // $end = StrToTime ( $end );
            }else{
                $start = date('Y-m-d H:i:s', strtotime($start_first. ' + '.$init.$status));
                $end = date('Y-m-d H:i:s', strtotime($start_first. ' + '.$future.$status));

            }

            // $end = date('Y-m-d H:i:s', strtotime($start_first. ' + '.$future.$status));

            if($datezz != null){

                $main_data = DB::table('Sent_Mail_Detail')        
                        ->select( DB::raw('Count(*) as unique_opens'))
                        ->where('Camp_Code_Id',$id)
                        ->where('Sm_Open_Time','<=', $datezz)        
                        ->whereBetween('Sm_Open_Time',array($start, $end))
                        ->get();

                // $main_data2 = DB::table('Sent_Mail')        
                //     ->select( DB::raw('Count(*) as unique_opens_2'))
                //     ->where('Camp_Code_Id',$id)
                //     ->where('Open_Time','<=', $datezz) 
                //     ->whereBetween('Open_Time',array($start, $end))
                //     ->get();

                $main_data2 = DB::table('Sent_Mail_Detail')        
                        ->select( "Email")
                        ->where('Camp_Code_Id',$id)
                        ->where('Sm_Open_Time','<=', $datezz) 
                        ->whereBetween('Sm_Open_Time',array($start, $end))
                        ->groupBy("Email")   
                        ->get();
                
            
            }else{

                $main_data = DB::table('Sent_Mail_Detail')        
                        ->select( DB::raw('Count(*) as unique_opens'))
                        ->where('Camp_Code_Id',$id)        
                        ->whereBetween('Sm_Open_Time',array($start, $end))
                        ->get();

                $main_data2 = DB::table('Sent_Mail_Detail')        
                        ->select( "Email")
                        ->where('Camp_Code_Id',$id)
                        ->whereBetween('Sm_Open_Time',array($start, $end))
                        ->groupBy("Email")
                        ->get();

                
            }
            
                        
			
            // $main_data = DB::select( DB::raw("SELECT Count(*) as unique_opens  
            //                                     FROM `Sent_Mail` 
            //                                     WHERE `Camp_Code_Id` = :id And date(Open_Time) between :start  and :end "),
            //                                         array(
            //                                             'id' => $id,
            //                                             'start' => $start,
            //                                             'end' => $end
            //                                         ));
            $basic_second = $main_data[0];

            $s_start = $start;

            $start = date('Y-m-d H:i:s', strtotime($start));

            // date_default_timezone_set('Australia/Canberra');

            // $start = new \DateTime('2018-01-21 20:16:44', new \DateTimeZone('Australia/Canberra')); 
            
            // $test = new \DateTimeZone('Australia/Sydney');   
            // $gmt = new \DateTimeZone('AEDT');

            $UTC = new \DateTimeZone("Australia/Sydney");     
            $newTZ = new \DateTimeZone("America/New_York");
            $date = new \DateTime($start, $newTZ  );
            $date->setTimezone( $UTC);
            
            $date2 = $date->format("Y-m-d H:i:s");   
            $date = $date->format($date_format); 
            
            if($i == 0 && $val == 2) $start = $original_first;                    

            
            $result[] = array(
                'time' => $date, 
                'unique_opens' => $basic_second->unique_opens, 
                'timezzz' => $start, 
                'uniq_opens' => count($main_data2),
                's_start' => $s_start
            );                

            
			
			
			$init = $future;

        }   
        
        

        $file_name = "report_".$id.".csv";
        if($val == 2 ){  
              
                $file = fopen("report_file/".$file_name,"w");   

                fputcsv($file, array("Date Range", "Total Opens", "Unique Opens"));       

                for ($i=0; $i < count($result); $i++) { 

                    fputcsv($file, array($result[$i]["time"], $result[$i]["unique_opens"], $result[$i]["uniq_opens"])); 

                    $t2 = date('Y-m-d H:i:s', StrToTime ( $result[$i]["s_start"] ));   // -1   
                    
                    // $UTC = new \DateTimeZone("Australia/Sydney");     
                    // $newTZ = new \DateTimeZone("America/New_York");
                    
                    $t2 = new \DateTime($t2);    
                    // $t2 = $t2->setTime(8,0,0);          

                    $t2 = $t2->format("Y-m-d H:i:s");


                    
                    $start = date('Y-m-d H:i:s', strtotime($t2));
                    
                
                    $end = date('Y-m-d H:i:s', strtotime($start. ' + 1'."days"));         

                    
                    if($datezz != null){

                        $main_data3 = DB::table('Sent_Mail_Detail')        
                                ->select('Email')
                                ->where('Camp_Code_Id',$id)
                                ->where('Sm_Open_Time','<=', $datezz)  
                                ->whereBetween('Sm_Open_Time',array($start, $end))
                                ->groupBy("Email")
                                ->get();     

                    }else{
                        $main_data3 = DB::table('Sent_Mail_Detail')        
                                ->select('Email')
                                ->where('Camp_Code_Id',$id)
                                ->whereBetween('Sm_Open_Time',array($start, $end))   
                                ->groupBy("Email")   
                                ->get();  
                    }

                    

                    $temp_date = new \DateTime($end); 
                    $temp_date = $temp_date->format("Y-m-d");   

                    $file_name = "report_daily_emails_".$id."_".$temp_date.".csv";                  
                   
                            
                    $file2 = fopen("report_file/".$file_name,"w"); 

                    fputcsv($file2, array("Email"));       

                    for ($ii=0; $ii < count($main_data3); $ii++) { 

                        fputcsv($file2, array($main_data3[$ii]->Email));             

                    }

                    fclose($file2);   

                    $result[$i]["emails_file"] = $file_name;   

                }

                fclose($file);
        }   

        return Response::json(array(
            'error' => false,
            'response' => json_encode($result), 
            'report_file' => $file_name,  
            'status_code' => 200
        ));

    }

    public function openDailyActivity($id){

        $main_data = DB::select( DB::raw("Select date(min(open_time)) as start, date(max(open_time)) as end, DATEDIFF(  max(open_time), min(open_time)) as diff from `Sent_Mail` where `Camp_Code_Id` = :id"), array('id' => $id));
        
        $basic = $main_data[0];

        $long = $basic->diff;

        $steps = 10;

        $interval = 1;   

        

        $init = 0;
        $start_first = $basic->start;
        
        

        $result = array();
		
		// $result[] = array('time' => $start_first, 'unique_opens' => 0); 

        for ($i=0; $i <= $steps; $i++) { 
          
			$future = 0;
            $future = $init + (int)$interval;
            
            $start = date('Y-m-d', strtotime($start_first. ' + '.$init.' days'));
            $end = date('Y-m-d', strtotime($start_first. ' + '.$future.' days'));

            $main_data = DB::table('Sent_Mail')
                        ->select( DB::raw('Count(*) as unique_opens'))
                        ->where('Camp_Code_Id',$id)
                        ->whereBetween('Open_Time',array($start, $end))
                        ->get();
			
            // $main_data = DB::select( DB::raw("SELECT Count(*) as unique_opens  
            //                                     FROM `Sent_Mail` 
            //                                     WHERE `Camp_Code_Id` = :id And date(Open_Time) between :start  and :end "),
            //                                         array(
            //                                             'id' => $id,
            //                                             'start' => $start,
            //                                             'end' => $end
            //                                         ));
            $basic_second = $main_data[0];

            $start = date('M d', strtotime($start));
			$result[] = array('time' => $start, 'unique_opens' => $basic_second->unique_opens); 
			
			
			$init = $future;

        }

        return Response::json(array(
            'error' => false,
            'response' => json_encode($result),
            'status_code' => 200
        ));

    }

    public function getOpensBaseOnTimeline($id){

        $main_data = DB::select( DB::raw("SELECT COUNT(*) as total FROM `Sent_Mail_Detail` WHERE `Camp_Code_Id` = :id"), array('id' => $id));
		
		if($main_data[0]->total == 0)
		{
			return;
		}
		
		$main_data = DB::select( DB::raw("SELECT TIMESTAMPDIFF(second,MIN(Open_Time), MAX(Open_Time)) as 'Diff' , UNIX_TIMESTAMP(MIN(Open_Time)) as 'open_time',TIMEDIFF(MIN(Open_Time), MAX(Open_Time)) as tt  
                                            FROM `Sent_Mail` 
                                            WHERE `Camp_Code_Id` = :id"), array('id' => $id));
		
		$basic = $main_data[0];
		
		
		
		
		$long = $basic->Diff;
				
		$steps = 10;
		
		$interval = $long / $steps;
		
		$init = 0;
		$init = $basic->open_time;
		
		
		$result = array();
		
		$result[] = array('time' => date('Y-m-d h:i:s', $init), 'unique_opens' => 0); 
		
		for ($i = 0; $i < $steps; $i++){
				
			
			$future = 0;
			$future = $init + $interval;
						
			
            $main_data = DB::select( DB::raw("SELECT Count(*) as unique_opens  
                                                FROM `Sent_Mail` 
                                                WHERE `Camp_Code_Id` = :id And UNIX_TIMESTAMP(Open_Time) between :init and :future"),
                                                    array(
                                                        'id' => $id,
                                                        'init' => $init,
                                                        'future' => $future
                                                    ));
			$basic_second = $main_data[0];
			
			$result[] = array('time' => date('Y-m-d h:i:s', $future), 'unique_opens' => $basic_second->unique_opens); 
			
			
			$init = $future;
		
		}
		
		$result[] = array('time' => date('Y-m-d h:i:s', $future + $interval), 'unique_opens' => 0); 
		
        return Response::json(array(
            'error' => false,
            'response' => json_encode($result),
            'status_code' => 200
        ));
    }

    public function getDeviceType($id){

            $response = DB::table('Device_Types')
                                ->select('Device_Types.Device_Name', DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
                                ->join("Sent_Mail_Detail",'Device_Types.Device_Type_Id', '=' ,'Sent_Mail_Detail.Device_Type_Id')
                                ->groupBy('Device_Name')
                                ->where('Camp_Code_Id', $id)
                                ->get();


            return Response::json(array(
                'error' => false,
                'response' => json_encode($response),
                'status_code' => 200
            ));
    }

    public function getTotalOpened($id){


                    $unique_opens = DB::table('Sent_Mail')
                                ->select(DB::raw("COUNT(Cust_Email_uniqid) as total"), DB::raw("SUM(Forward) as forward_total"), DB::raw("SUM(Print) as print_total"))
                                ->where('Camp_Code_Id', $id)
                                ->get();

                    $response = DB::table('master_campaign')
                            ->select('master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_created_date','master_campaign.mc_order_id','master_campaign.mc_order_hash','master_campaign.mc_opens','master_campaign.mc_bonus','master_campaign.mc_set_cap')
                            ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                            ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                            ->join('Campaign','Campaign.Camp_Code_Id','=','master_campaign_trackingpixel.mctp_mc_track_id')                
                            ->where('Campaign.Camp_Code_Id',$id)
                            ->get();

                    foreach ($response as $row1) {

                            $total_opens_assigned = DB::table('master_campaign_detail')
                                ->select(DB::raw('SUM(balance) as total_opens_assigned'))
                                ->where('mc_id', $row1->mc_id)
                                ->groupBy('mc_id')
                                ->get();
                            if(count($total_opens_assigned) == 0){
                                $row1->total_opens_assigned = 0;
                            }else{
                                $row1->total_opens_assigned = $total_opens_assigned[0]->total_opens_assigned;
                                
                            }
                    
                            $row1->total_opens_assigned = $row1->total_opens_assigned + $row1->mc_opens;
                    }

                    $bonus = $response[0]->mc_bonus;   
                    $limit = $response[0]->total_opens_assigned; 

                    $response = DB::table('Sent_Mail_Detail')
                        ->select(DB::raw('COUNT(Email) as Opens'))
                        ->where('Camp_Code_Id',$id)     
                        ->get(); 
                        
                        $limit += $bonus;

                    if($limit < $response[0]->Opens){

                        // $limit += $bonus;                     

                                for ($i = $limit; $i >= 0 ; $i = $i - 5) {                
                            
                                    $response = DB::table('Sent_Mail_Detail')
                                        ->select(DB::raw("DATE_FORMAT(Sm_Open_Time, '%Y-%m-%d %H:%i:%s') as Sm_Open_Time"))             
                                        ->where('Camp_Code_Id',$id) 
                                        ->orderBy("Sm_Open_Time")   
                                        ->limit($i)  
                                        ->get(); 
                                        
                                        
            
                                $date = $response[count($response) - 1]->Sm_Open_Time;
                
                                $response = DB::table('master_campaign')
                                        ->select('linkly_details.ld_timestamp')          
                                        ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                                        ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                                        ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                        ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                                        ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                                        ->where('li_sid' ,'=', null)    
                                        ->where('linkly_details.ld_timestamp' ,'<=', $date)   
                                        ->get();   
            
                                if($i + count($response) <= $limit){    

                                    $data = array(
                                        "date" => $date,                             
                                        "opens" => $i,
                                        "clicks" => count($response),
                                        "total_opens_assigned" => $limit     
                                    );
                                    return Response::json(array(
                                        'error' => false,
                                        'response' => json_encode($data),              
                                        'status_code' => 200      
                                    ));
                                }      
                            }

                    }else{

                            return Response::json(array(
                                'error' => false,
                                'response' => json_encode(array("Opens" => $response[0]->Opens, "date" => null, "total_opens_assigned" => $limit)),              
                                'status_code' => 200
                            ));
                    }
   
                        
                        // $response = DB::table('master_campaign')
                        //     ->select('linkly_details.ld_timestamp')          
                        //     ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                        //     ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                        //     ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                        //     ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                        //     ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                        //     ->where('li_sid' ,'=', null)    
                        //     ->limit(200)           
                        //     ->get();     
       
                        //     $response = DB::table('Sent_Mail_Detail')
                        // ->select("*")
                        // ->where('Camp_Code_Id',$id)  
                        // ->where(DB::raw("Date(Sm_Open_Time)"), "<=", "2018-01-21 23:25:10" )      
                        // ->get();   
                            
                            
                        
                        // $response = DB::select(DB::raw("SELECT *  from Sent_Mail_Detail where `Camp_Code_Id` = '".$id."' limit 0,10"));                 
        
                // return Response::json(array(
                //     'error' => false,
                //     'response' => json_encode($response),
                //     'status_code' => 200
                // ));
            }
            
    public function getUniqueOpened($id, Request $request){

        $date = $request->input("date");

        if($date != null){
            $unique_opens = DB::table('Sent_Mail')
                    ->select(DB::raw("COUNT(DISTINCT(Cust_Email_uniqid)) as total"), DB::raw("SUM(Forward) as forward_total"), DB::raw("SUM(Print) as print_total"))
                    ->where('Camp_Code_Id', $id)
                    ->where("Open_Time","<=",$date)
                    ->get();  


            $unique_forward = DB::table('Sent_Mail')
                ->select(DB::raw("COUNT(Forward) as forward_unique"))
                ->where('Camp_Code_Id', $id)
                ->where("Open_Time","<=",$date)
                ->where('Forward',"!=", 0)
                ->get();  

            $unique_print = DB::table('Sent_Mail')
                ->select(DB::raw("COUNT(Print) as print_unique"))
                ->where('Camp_Code_Id', $id)
                ->where("Open_Time","<=",$date)    
                ->where('Print',"!=", 0)
                ->get(); 
                      

        }else{
            $unique_opens = DB::table('Sent_Mail')
                    ->select(DB::raw("COUNT(DISTINCT(Cust_Email_uniqid)) as total"), DB::raw("SUM(Forward) as forward_total"), DB::raw("SUM(Print) as print_total"))
                    ->where('Camp_Code_Id', $id)
                    ->get();  
             
        
            $unique_forward = DB::table('Sent_Mail')
                ->select(DB::raw("COUNT(Forward) as forward_unique"))
                ->where('Camp_Code_Id', $id)
                ->where('Forward',"!=", 0)
                ->get();  

            $unique_print = DB::table('Sent_Mail')
                ->select(DB::raw("COUNT(Print) as print_unique"))
                ->where('Camp_Code_Id', $id)
                ->where('Print',"!=", 0)
                ->get(); 
                    
        }       
         

        $response = DB::table('master_campaign')
                ->select('master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_created_date','master_campaign.mc_order_id','master_campaign.mc_order_hash','master_campaign.mc_opens')
                ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                ->join('Campaign','Campaign.Camp_Code_Id','=','master_campaign_trackingpixel.mctp_mc_track_id')                
                ->where('Campaign.Camp_Code_Id',$id)
                ->get();

        foreach ($response as $row1) {

                $total_opens_assigned = DB::table('master_campaign_detail')
                    ->select(DB::raw('SUM(balance) as total_opens_assigned'))
                    ->where('mc_id', $row1->mc_id)
                    ->groupBy('mc_id')
                    ->get();
                if(count($total_opens_assigned) == 0){
                    $row1->total_opens_assigned = 0;
                }else{
                    $row1->total_opens_assigned = $total_opens_assigned[0]->total_opens_assigned;
                    
                }
                $row1->unique_opens = $unique_opens[0]->total;
                $row1->forward_total = $unique_opens[0]->forward_total;
                $row1->print_total = $unique_opens[0]->print_total;  
                
                $row1->unique_forward = $unique_forward[0]->forward_unique;
                $row1->unique_print = $unique_print[0]->print_unique;  

                $row1->total_opens_assigned = $row1->total_opens_assigned + $row1->mc_opens;
        }

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
    }

    // tracking pixels
    public function getEmailClientsPlatform($id, Request $request){

        $datezz = $request->input("datezz");

        $platform = array();
        if($datezz != null){

            $response = DB::table('Device_Types')
                            ->select(DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
                            ->join('Sent_Mail_Detail', 'Device_Types.Device_Type_Id','=','Sent_Mail_Detail.Device_Type_Id')
                            ->where('Sent_Mail_Detail.Camp_Code_Id',$id)
                            ->where('Sm_Open_Time',"<=", $datezz)
                            ->where('Device_Types.Device_Type_Id', "=",5)
                            ->get();
            
            array_push($platform, $response[0]);

            $response = DB::table('Device_Types')
                            ->select(DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
                            ->join('Sent_Mail_Detail', 'Device_Types.Device_Type_Id','=','Sent_Mail_Detail.Device_Type_Id')
                            ->where('Sent_Mail_Detail.Camp_Code_Id',$id)
                            ->where('Sm_Open_Time',"<=", $datezz)
                            ->where('Device_Types.Device_Type_Id', "=",2)
                            ->get();

                            array_push($platform, $response[0]);

            $response = DB::table('Device_Types')
                        ->select(DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
                        ->join('Sent_Mail_Detail', 'Device_Types.Device_Type_Id','=','Sent_Mail_Detail.Device_Type_Id')
                        ->where('Sent_Mail_Detail.Camp_Code_Id',$id)
                        ->where('Sm_Open_Time',"<=", $datezz)
                        ->where('Device_Types.Device_Type_Id', "!=",2)
                        ->where('Device_Types.Device_Type_Id', "!=",5)
                        // ->whereIn('Device_Types.Device_Type_Id',[1,3])
                        ->get();

            array_push($platform, $response[0]);

        }else{

            $response = DB::table('Device_Types')
                        ->select(DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
                        ->join('Sent_Mail_Detail', 'Device_Types.Device_Type_Id','=','Sent_Mail_Detail.Device_Type_Id')
                        ->where('Sent_Mail_Detail.Camp_Code_Id',$id)
                        ->where('Device_Types.Device_Type_Id', "=",5)
                        ->get();

            array_push($platform, $response[0]);

            $response = DB::table('Device_Types')
                            ->select(DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
                            ->join('Sent_Mail_Detail', 'Device_Types.Device_Type_Id','=','Sent_Mail_Detail.Device_Type_Id')
                            ->where('Sent_Mail_Detail.Camp_Code_Id',$id)
                            ->where('Device_Types.Device_Type_Id', "=",2)
                            ->get();

                            array_push($platform, $response[0]);

            $response = DB::table('Device_Types')
                        ->select(DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
                        ->join('Sent_Mail_Detail', 'Device_Types.Device_Type_Id','=','Sent_Mail_Detail.Device_Type_Id')
                        ->where('Sent_Mail_Detail.Camp_Code_Id',$id)
                        ->where('Device_Types.Device_Type_Id', "!=",2)
                        ->where('Device_Types.Device_Type_Id', "!=",5)
                        // ->whereIn('Device_Types.Device_Type_Id',[1,3])

                        ->get();

            array_push($platform, $response[0]);
        }

        
                            
        return Response::json(array(
            'error' => false,
            'response' => json_encode($platform),
            'status_code' => 200
        ));
    }

    // tracking pixels
    public function getEmailClients($id, Request $request){

        $datezz = $request->input("datezz");

        if($datezz != null){

            $response = DB::table('Device_Types')
                            ->select('Device_Types.Device_Name',DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
                            ->join('Sent_Mail_Detail', 'Device_Types.Device_Type_Id','=','Sent_Mail_Detail.Device_Type_Id')
                            ->where('Sent_Mail_Detail.Camp_Code_Id',$id)
                            ->where('Sm_Open_Time',"<=", $datezz)
                            ->groupBy('Device_Name') 
                            ->orderBy('Device_Name', 'desc')      
                            ->get();

        }else{

            $response = DB::table('Device_Types')
            ->select('Device_Types.Device_Name',DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
            ->join('Sent_Mail_Detail', 'Device_Types.Device_Type_Id','=','Sent_Mail_Detail.Device_Type_Id')
            ->where('Sent_Mail_Detail.Camp_Code_Id',$id)
            ->groupBy('Device_Name')
            ->orderBy('Device_Name', 'desc')      
            ->get();
        }

        
                            
        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
    }

    public function getEmailClientsReadingTimeDevice($id, $device, $datezz){

            $condition = "=";
            // if($device == 1){
            //     $condition = "<";
            //     $device = 5;
            // }
    
            if($datezz != null){

                        $glanced = DB::table('Sent_Mail_Detail')
                                ->select("*")
                                ->where([
                                    ['Camp_Code_Id','=',$id],
                                    ['Reading_Time', '<', 2],
                                    ['Device_Type_Id',$condition, $device]
                                ])
                                ->where("Sm_Open_Time","<=", $datezz)
                                ->get();
                        
                        
                        $skim = DB::table('Sent_Mail_Detail')
                                ->select("*")
                                ->where([
                                    ['Camp_Code_Id','=',$id],
                                    ['Reading_Time', '>=', 2],
                                    ['Reading_Time', '<=', 8],
                                    ['Device_Type_Id',$condition, $device]
                                ])
                                ->where("Sm_Open_Time","<=", $datezz)
                                ->get();

                        $read = DB::table('Sent_Mail_Detail')
                                ->select("*")
                                ->where([
                                    ['Camp_Code_Id','=',$id],
                                    ['Reading_Time', '>', 8],
                                    ['Device_Type_Id',$condition, $device]
                                ])
                                ->where("Sm_Open_Time","<=", $datezz)
                                ->get();
                                

                }else{

                        $glanced = DB::table('Sent_Mail_Detail')
                                ->select("*")
                                ->where([
                                    ['Camp_Code_Id','=',$id],
                                    ['Reading_Time', '<', 2],
                                    ['Device_Type_Id',$condition, $device]
                                ])
                                ->get();

                        $skim = DB::table('Sent_Mail_Detail')
                                ->select("*")
                                ->where([
                                    ['Camp_Code_Id','=',$id],
                                    ['Reading_Time', '>=', 2],
                                    ['Reading_Time', '<=', 8],
                                    ['Device_Type_Id',$condition, $device]
                                ])
                                ->get();

                        $read = DB::table('Sent_Mail_Detail')
                            ->select("*")
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '>', 8],
                                ['Device_Type_Id',$condition, $device]
                            ])
                            ->get();
                }


                return $response = array(
                    'Glanced' => count($glanced),
                    'Skim' => count($skim),
                    'Read' => count($read),
                    'Total' => count($glanced) + count($read) + count($skim)
                );

                // var_dump($response);
    }

    // tracking pixels
    public function getEmailClientsReadingTimeDesktop($id, Request $request){

        $datezz = $request->input("datezz");


        $response = $this->getEmailClientsReadingTimeDevice($id, 5, $datezz);

        
                            
        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
    }

    public function getEmailClientsReadingTimeMobile($id, Request $request){

        $datezz = $request->input("datezz");


        $response = $this->getEmailClientsReadingTimeDevice($id, 1, $datezz);

        $response2 = $this->getEmailClientsReadingTimeDevice($id, 3, $datezz);

        $response["Glanced"] =  $response["Glanced"] +  $response2["Glanced"];
        $response["Skim"] =  $response["Skim"] +  $response2["Skim"];
        $response["Read"] =  $response["Read"] +  $response2["Read"];
        $response["Total"] =  $response["Total"] +  $response2["Total"];


                            
        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
    }
    
    public function getEmailClientsReadingTimeMobile_22($id){

        $datezz = null;


        $response = $this->getEmailClientsReadingTimeDevice($id, 1, $datezz);

        $response2 = $this->getEmailClientsReadingTimeDevice($id, 3, $datezz);

        $response["Glanced"] =  $response["Glanced"] +  $response2["Glanced"];
        $response["Skim"] =  $response["Skim"] +  $response2["Skim"];
        $response["Read"] =  $response["Read"] +  $response2["Read"];
        $response["Total"] =  $response["Total"] +  $response2["Total"];


                            
        return $response["Read"] * 100 / $response["Total"];
    }

    public function getEmailClientsReadingTimeTablet($id, Request $request){

        $datezz = $request->input("datezz");


        $response = $this->getEmailClientsReadingTimeDevice($id, 2, $datezz);

        
                            
        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
    }

    function getDeletedGlanced($id, $datezz){

        $total = $this->getTotalReadingTime($id, $datezz);

        if($datezz != null){

            $result = DB::table('Sent_Mail_Detail')
                    ->select("*")
                    ->where([
                        ['Camp_Code_Id','=',$id],
                        ['Reading_Time', '<', 2]
                    ])
                    ->where("Sm_Open_Time","<=", $datezz)
                    ->get();

                    return count($result);

        }else{

            $result = DB::table('Sent_Mail_Detail')
                            ->select(DB::raw("COUNT(Reading_Time) as Percentage"))
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '<', 2]
                            ])->get();

                            return $result[0]->Percentage;
        }
        
        
        
    }

    function getSkimTime($id, $datezz){        

        $total = $this->getTotalReadingTime($id, $datezz);

        if($datezz != null){

            $result = DB::table('Sent_Mail_Detail')
                            ->select("*")
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '>=', 2],
                                ['Reading_Time', '<=', 8],
                            ])
                            ->where("Sm_Open_Time","<=", $datezz)
                            ->get();

             return count($result);


        }else{
            $result = DB::table('Sent_Mail_Detail')
                            ->select(DB::raw("COUNT(Reading_Time) as Percentage"))
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '>=', 2],
                                ['Reading_Time', '<=', 8],
                            ])->get();

            return $result[0]->Percentage;
        }
                                        
    }



    function getReadingTime($id, $datezz){  

        $total = $this->getTotalReadingTime($id, $datezz);

        if($datezz != null){

            $result = DB::table('Sent_Mail_Detail')
                            ->select("*")
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '>', 8],
                            ])
                            ->where("Sm_Open_Time","<=", $datezz)
                            ->get();
                            
            return count($result);


        }else{

            $result = DB::table('Sent_Mail_Detail')
                            ->select(DB::raw("COUNT(Reading_Time) as Percentage"))
                            ->where([
                                ['Camp_Code_Id','=',$id],
                                ['Reading_Time', '>', 8],
                            ])->get();

            return $result[0]->Percentage;
        }
        
        

    }

    public function createReport(Request $request){

        $email =  $request->input('email');
        $camp_id =  $request->input('camp_id');
        $options =  $request->input('options');
        $logo =  $request->input('logo');   
        $type =  $request->input('type');   
        $code = $this->randString(7); 

        $created_date = $request->input('created_date');

        $img = explode(',', $logo)[1];
        $img = base64_decode($img);

        $file_name = 'logo_'.$code.'.png';
        $path = public_path() .'/'. "logos/" . $file_name;
        
        // $fileStream= fopen($path , "wb"); 
        
        // fwrite($fileStream, $img); 
        
        // fclose($fileStream); 
        file_put_contents($path, $img);
        // Image::make($img->getRealPath())->save($path);

        $id = DB::table("report_manager")->insertGetId([
            // 'mc_id' => null,
            're_selected' => implode(',',$options),
            're_code' => $code,
            're_logo' => $file_name,
            're_camp_id' => $camp_id,
            're_user_email' => $email,
            // 're_created_date' => $created_date, 
            're_type' => $type
        ]);

        $opt = '';
        for ($j=0; $j < count($options); $j++) { 
            $opt .= implode(' ', explode('-', $options[$j])) . ', ';
        }

        $response = array();
        array_push($response,$id,$opt,$code);

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
        
    }

    public function getReports($re_code){
        $response =  DB::table("report_manager")
                        ->select('*')
                        ->where('re_code',$re_code)
                        ->get();

        
        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
    }

    public function getAllReports(Request $request){

        $email =  $request->input('email');
        
        $re_type =  $request->input('re_type');

        $response =  DB::table("report_manager")
                        ->select('*')
                        ->where('re_user_email',$email)
                        ->where('re_type',$re_type)
                        ->limit(5)
                        ->orderBy('re_id','desc')
                        ->get();


        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));
    }

    // public function getHotLeadsReading($email_id){
        
    //                 $list = array();
        
    //                 $response =  DB::table("Sent_Mail")->select('Sent_Mail.Cust_Email_uniqid')
    //                                             ->join('Sent_Mail_Detail','Sent_Mail.Camp_Code_Id','=','Sent_Mail_Detail.Camp_Code_Id')
    //                                             ->where('Sent_Mail_Detail.Reading_Time','>',10)
    //                                             ->where('Sent_Mail.Camp_Code_Id',$email_id)
    //                                             // ->orderBy('Sent_Mail.Accumulator', 'desc')
    //                                             ->groupBy('Sent_Mail.Cust_Email_uniqid')
    //                                             ->get();
    //                 if(count($response) > 0){
    //                     array_push($list, $response, count($response));
    //                 }
        
    //                 return Response::json(array(
    //                     'error' => false,
    //                     'response' => json_encode($list),
    //                     'status_code' => 200
    //                 ));
    //         }

    public function getHotLeadsData($email_id){
        
        $hot_leads = $this->getHotLeads($email_id);
        $could_spark = $this->getCouldSpark($email_id);
        $gone_cold = $this->getGoneCold($email_id);

        $list = array();

        array_push($list, $hot_leads,  $could_spark, $gone_cold );

        return Response::json(array(
                'error' => false,
                'response' => json_encode($list),
                'status_code' => 200
            ));
    }

    public function getHotLeads($email_id){

            $list = array();

            $response =  DB::table("Sent_Mail")->select('Sent_Mail.Cust_Email_uniqid', 'Sent_Mail.Accumulator','Customer.Country', 'Customer.City', 'Sent_Mail.Reading_Time', 'Sent_Mail.Open_Time')
                                        // ->join('Sent_Mail_Detail','Sent_Mail.Camp_Code_Id','=','Sent_Mail_Detail.Camp_Code_Id')
                                        ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')
                                        ->where('Sent_Mail.Reading_Time','>=',7)
                                        ->where('Sent_Mail.Camp_Code_Id',$email_id)
                                        ->orderBy('Sent_Mail.Accumulator', 'desc')
                                        ->get();

                     

            if(count($response) > 0){
                array_push($list, $response, count($response));

            }

            return $list;

            // return Response::json(array(
            //     'error' => false,
            //     'response' => json_encode($list),
            //     'status_code' => 200
            // ));
    }

    public function getCouldSpark($email_id){

            $list = array();
        
            $response =  DB::table("Sent_Mail")->select('Sent_Mail.Cust_Email_uniqid', 'Sent_Mail.Accumulator','Customer.Country', 'Customer.City', 'Sent_Mail.Reading_Time', 'Sent_Mail.Open_Time')
                                        // ->join('Sent_Mail_Detail','Sent_Mail.Camp_Code_Id','=','Sent_Mail_Detail.Camp_Code_Id')
                                        ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')
                                        ->whereBetween('Sent_Mail.Reading_Time',[3,6])
                                        ->where('Sent_Mail.Camp_Code_Id',$email_id)
                                        ->orderBy('Sent_Mail.Accumulator', 'desc')
                                        ->get();

            if(count($response) > 0){
                array_push($list, $response, count($response));
            }

            return $list;
            
            // return Response::json(array(
            //     'error' => false,
            //     'response' => json_encode($list),
            //     'status_code' => 200
            // ));
    }

    public function getGoneCold($email_id){
            $list = array();
        
            $response =  DB::table("Sent_Mail")->select('Sent_Mail.Cust_Email_uniqid', 'Sent_Mail.Accumulator','Customer.Country', 'Customer.City', 'Sent_Mail.Reading_Time', 'Sent_Mail.Open_Time')
                                            // ->join('Sent_Mail_Detail','Sent_Mail.Camp_Code_Id','=','Sent_Mail_Detail.Camp_Code_Id')
                                            ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')
                                        ->whereBetween('Sent_Mail.Accumulator',[1,2])
                                        ->where('Sent_Mail.Camp_Code_Id',$email_id)
                                        ->orderBy('Sent_Mail.Accumulator', 'desc')
                                        ->get();

            if(count($response) > 0){
                array_push($list, $response, count($response));
            }

            return $list;
            
            // return Response::json(array(
            //     'error' => false,
            //     'response' => json_encode($list),
            //     'status_code' => 200
            // ));

    }
    public function getHotLeadsAndGoneCold_22($datezz, $current_camp_id, $email){

        // $current =  DB::table("Sent_Mail")->select('Sent_Mail.Cust_Email_uniqid', 'Sent_Mail.Reading_Time')
        //                     ->where('Sent_Mail.Reading_Time','>=',7)
        //                     ->where('Sent_Mail.Camp_Code_Id',21)
        //                     ->get();

        // $datezz = $request->input('datezz');

        // $current_camp_id = $request->input('camp_id');

        // $email_id = 'nestor202@merchantwise.com';

        $result =  $this->getLastestCamp_2($email, $current_camp_id);

              

        $list_ids = array();
        if(count($result) >= 2){    
            foreach ($result as $row) {
                array_push($list_ids, $row->Camp_Code_Id);
            }

        }   
        array_push($list_ids,  $current_camp_id);

        // return var_dump($list_ids);   

        $list_ids = array_unique($list_ids);
        
        

        $response =  DB::table("Sent_Mail")
                                            ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'))     
                                            // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')   
                                            ->where('Sent_Mail.Camp_Code_Id',$current_camp_id)
                                            ->get();   

                                            $current_emails = array() ;   
        foreach ($response as $row) {
           array_push($current_emails, $row->Cust_Email_uniqid);
        }

        if($datezz != null){
            $response =  DB::table("Sent_Mail")
                            ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'), DB::raw('COUNT(Sent_Mail.Cust_Email_uniqid) as Total'), 'Sent_Mail.Accumulator','Customer.Country', 'Customer.City', 'Sent_Mail.Reading_Time', 'Sent_Mail.Open_Time')
                            ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')
                            ->whereIn('Sent_Mail.Camp_Code_Id',$list_ids)
                            ->whereIn('Sent_Mail.Cust_Email_uniqid',$current_emails)  
                            ->where('Sent_Mail.Reading_Time','>=',7)
                            ->where("Open_Time", "<=", $datezz)
                            ->orderBy('Sent_Mail.Reading_Time', 'desc')
                            ->groupBy('Sent_Mail.Cust_Email_uniqid')
                            ->get();
        }else{

            $response =  DB::table("Sent_Mail")
                            ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'), DB::raw('COUNT(Sent_Mail.Cust_Email_uniqid) as Total'), 'Sent_Mail.Accumulator', 'Sent_Mail.Reading_Time', 'Sent_Mail.Open_Time')     
                            // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')   
                            ->whereIn('Sent_Mail.Camp_Code_Id',$list_ids)
                            ->whereIn('Sent_Mail.Cust_Email_uniqid',$current_emails)     
                            ->where('Sent_Mail.Reading_Time','>=',7)
                            // ->where("Open_Time", "<=", $datezz)
                            ->orderBy('Sent_Mail.Reading_Time', 'desc')
                            ->groupBy('Sent_Mail.Cust_Email_uniqid')  
                            ->get();
        }
        
        

                            // return Response::json(array(
                            //     'error' => false,
                            //     'response' => json_encode($list_ids),
                            //     'status_code' => 200
                            // ));


        $responses = array();
        $hot_leads = array();
        $gone_cold = array();

        $emails = array();
        if(count( $response) > 0){
            foreach ($response as $row) {     
                if($row->Total > 2){                
                    array_push($hot_leads, $row);  
                    // array_push($emails, $row->Cust_Email_uniqid);
                }else{
                    array_push($gone_cold, $row);
                    // array_push($emails, $row->Cust_Email_uniqid);
                }

                
            }
        }
        
        return count($hot_leads);
    }

    public function getHotLeadsAndGoneCold_2(Request $request){

            $list = $request->input('list');

            $temp_list = base64_decode($request->input('temp_list')) ;  
            
            $datezz = $request->input('datezz');

            $camp_id = $request->input('camp_id');
            $new_list = array();

            $temp_list = explode(",", $temp_list);

            if(count($temp_list) > 1000){
                $length = count($temp_list) / 2;
            }else{
                $length = count($temp_list);
            }

            $result =  $this->getLastestCamp_2($request->input('email'), $camp_id);

            $list_ids = array();
            foreach ($result as $row) {
                array_push($list_ids, $row->Camp_Code_Id);
            }
            array_push($list_ids,  (int)$camp_id);

        
            $list_ids = array_unique($list_ids);

            // return Response::json(array(
            //     'error' => false,
            //     'response' => $list_ids,
            //     'status_code' => 200
            // ));

            for ($i=0; $i < $length; $i++) { 


                    $Cust_Email_uniqid = $temp_list[$i];

                    if(strpos($Cust_Email_uniqid, '@') !== false){
                            $response =  DB::table("Sent_Mail")
                                ->select(DB::raw("DISTINCT(Campaign.Camp_Name)"),"Sent_Mail.Reading_Time",'Sent_Mail.Cust_Email_uniqid')
                                ->join('Campaign','Campaign.Camp_Code_Id','=','Sent_Mail.Camp_Code_Id')
                                ->where('Sent_Mail.Cust_Email_uniqid','=',$Cust_Email_uniqid) 
                                // ->where('Sent_Mail.Camp_Code_Id','<=',$camp_id) 
                                ->whereIn('Sent_Mail.Camp_Code_Id', $list_ids)
                                ->orderBy('Sent_Mail.Camp_Code_Id', 'desc')
                                ->limit(3)
                                ->get();   

                            // $list[$i]["pre_camp_data"] = $response;
                            array_push($new_list, $response);
                    }

                    
            }

            return Response::json(array(
                'error' => false,
                'response' => $new_list,
                'status_code' => 200
            ));

    }

    
    public function get_HotLeads(Request $request){
        $current_camp_id = $request->input('camp_id');

        $order = $request->input('order');


        $result =  $this->getLastestCamp_2($request->input('email'), $current_camp_id);

        $list_ids = array();
        // if(count($result) >= 2){    
            foreach ($result as $row) {
                array_push($list_ids, $row->Camp_Code_Id);
            }

        // }   
        array_push($list_ids,  $current_camp_id);

        // return var_dump($list_ids);   

        $list_ids = array_unique($list_ids);

        $current_emails_list =  DB::table("Sent_Mail")
                                            ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'))     
                                            // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')   
                                            ->where('Sent_Mail.Camp_Code_Id',$current_camp_id)
                                            // ->groupBy('Sent_Mail.Cust_Email_uniqid')
                                            ->get();   

                                            // $current_emails = array() ;   
        // foreach ($current_emails_list as $row) {
        //    array_push($current_emails, $row->Cust_Email_uniqid);
        // }

        // $current_emails = array_unique($current_emails);

        $responses = array();
        $hot_leads = array();
        $gone_cold = array();

        $emails = array();

        $email_hot_lead = array();

    //     return Response::json(array(
    //         'error' => false,
    //         'response' => json_encode($list_ids),
    //         // 'file_list' => $file_list,
    //         // 'date' => $datezz,
    //         'status_code' => 200
    // ));

        $list_ids = array($list_ids[$order]);

        foreach ($current_emails_list as $row) {
                    $count = 0;
                    foreach ($list_ids as $list_id) {
                        $response =  DB::table("Sent_Mail")
                                ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'))     
                                // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')   
                                ->where('Sent_Mail.Camp_Code_Id',$list_id)
                                ->where('Sent_Mail.Cust_Email_uniqid',$row->Cust_Email_uniqid)        
                                ->where('Sent_Mail.Reading_Time','>=',7)
                                ->orderBy('Sent_Mail.Reading_Time', 'desc')
                                ->get();

                        if(count($response) > 0){
                            array_push($hot_leads, $row->Cust_Email_uniqid);    
                        }
                    }

                    // if($count >= 3){
                    //     array_push($hot_leads, $row->Cust_Email_uniqid);    
                    // }
                    // else{
                    //     array_push($gone_cold, $row->Cust_Email_uniqid); 
                    // }
            }

            // array_push($responses, $hot_leads);
            array_push($responses, $gone_cold);
            return Response::json(array(
                'error' => false,
                'response' => json_encode(count($hot_leads)),
                // 'file_list' => $file_list,
                // 'date' => $datezz,
                'status_code' => 200
        ));


    }


    public function getHotLeadsAndGoneCold(Request $request){

        // $current =  DB::table("Sent_Mail")->select('Sent_Mail.Cust_Email_uniqid', 'Sent_Mail.Reading_Time')
        //                     ->where('Sent_Mail.Reading_Time','>=',7)
        //                     ->where('Sent_Mail.Camp_Code_Id',21)
        //                     ->get();

        $datezz = $request->input('datezz');

        $current_camp_id = $request->input('camp_id');
        
        // $current_camp_id = 314;

        // $email_id = 'nestor202@merchantwise.com';

        $result =  $this->getLastestCamp_2($request->input('email'), $current_camp_id);

      
              

        $list_ids = array();
        // if(count($result) >= 2){    
            foreach ($result as $row) {
                array_push($list_ids, $row->Camp_Code_Id);
            }

        // }   
        array_push($list_ids,  (int)$current_camp_id);

        // return var_dump($list_ids);   

        $list_ids = array_unique($list_ids);
        
        // var_dump($list_ids);  die();
        
        if($datezz != null){
        
            $current_emails_list =  DB::table("Sent_Mail")
                                            ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'))     
                                            // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')   
                                            ->where('Sent_Mail.Camp_Code_Id',$current_camp_id)
                                            // ->groupBy('Sent_Mail.Cust_Email_uniqid')
                                            ->where("Open_Time", "<=", $datezz)
                                            ->get();   
        }else{
            $current_emails_list =  DB::table("Sent_Mail")
                                            ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'))     
                                            // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')   
                                            ->where('Sent_Mail.Camp_Code_Id',$current_camp_id)
                                            // ->groupBy('Sent_Mail.Cust_Email_uniqid')
                                            ->get();   
        }

        

                                            $current_emails = array() ;   
        foreach ($current_emails_list as $row) {
            
            if(strpos($row->Cust_Email_uniqid, "@") > 0){
                array_push($current_emails, $row->Cust_Email_uniqid);

            }
        }

        $current_emails = array_unique($current_emails);
        

        // return Response::json(array(
        //     'error' => false,
        //     'response' => json_encode($list_ids),
        //     // 'file_list' => $file_list,
        //     // 'date' => $datezz,
        //     'status_code' => 200
        // ));

        // return Response::json(array(
        //     'error' => false,
        //     'response' => json_encode(count($current_emails)),
        //     // 'file_list' => $file_list,
        //     'date' => $datezz,
        //     'status_code' => 200
        // ));

        $responses = array();
        $hot_leads = array();
        $gone_cold = array();

        $emails = array();

        $email_hot_lead = array();

        $gone_cold_temp = array();

        if($datezz != null){
            $response =  DB::table("Sent_Mail")
                            ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'), DB::raw('COUNT(Sent_Mail.Cust_Email_uniqid) as Total'))
                            // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')
                            ->whereIn('Sent_Mail.Camp_Code_Id',$list_ids)
                            // ->whereIn('Sent_Mail.Cust_Email_uniqid',$current_emails)  
                            ->where('Sent_Mail.Reading_Time','>=',7)
                            ->where("Open_Time", "<=", $datezz)
                            ->orderBy('Sent_Mail.Reading_Time', 'desc')
                            ->groupBy('Sent_Mail.Cust_Email_uniqid')
                            ->get();
        }else{

            // foreach ($current_emails as $email_uniq) {
            //         $count = 0;
            //         foreach ($list_ids as $list_id) {
            //             $response =  DB::table("Sent_Mail")
            //                     ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'))     
            //                     // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')   
            //                     ->where('Sent_Mail.Camp_Code_Id',$list_id)
            //                     ->where('Sent_Mail.Cust_Email_uniqid',$email_uniq)        
            //                     ->where('Sent_Mail.Reading_Time','>=',7)
            //                     ->orderBy('Sent_Mail.Reading_Time', 'desc')
            //                     ->get();

            //             if(count($response) > 0){
            //                 $count++;
            //             }
            //         }

            //         if($count >= 3){
            //             array_push($hot_leads, $email_uniq); 
            //         }
            //         else{
            //             array_push($gone_cold, $email_uniq); 
            //         }
            // }

            

            $response =  DB::table("Sent_Mail")
                            ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'), DB::raw('COUNT(Sent_Mail.Cust_Email_uniqid) as Total'), DB::raw('SUM(Sent_Mail.Reading_Time) as Sum_Total'),  DB::raw('Sent_Mail.Reading_Time'))     
                            // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')   
                            ->whereIn('Sent_Mail.Camp_Code_Id',$list_ids)
                            // ->whereIn('Sent_Mail.Cust_Email_uniqid',$current_emails)        
                            ->where('Sent_Mail.Reading_Time','>=',7)
                            // ->where("Open_Time", "<=", $datezz)
                            ->orderBy('Sent_Mail.Reading_Time', 'desc')
                            ->groupBy('Sent_Mail.Cust_Email_uniqid')  
                            ->get();
        }
        

                            // return Response::json(array(
                            //     'error' => false,
                            //     'response' => json_encode($list_ids),
                            //     'status_code' => 200
                            // ));

        // var_dump($response);die();
        
        if(count($list_ids) <= 2){
            
            // foreach ($current_emails_list as $row) {
            
            // if(strpos($row->Cust_Email_uniqid, "@") > 0){
            //     array_push($gone_cold, $row);

            // }
        // }
        
            // $gone_cold = $current_emails_list;
        }else{

            if(count( $response) > 0){
                foreach ($response as $row) {     
                    if($row->Total >= 3){   
                        
                        // $Cust_Email_uniqid = $row->Cust_Email_uniqid;
    
                        // $response =  DB::table("Sent_Mail")
                        //         // ->join('Campaign','Campaign.Camp_Code_Id','=','Sent_Mail.Camp_Code_Id')
                        //         // ->select("Sent_Mail.Reading_Time")
                        //         ->where('Sent_Mail.Cust_Email_uniqid','=',$Cust_Email_uniqid) 
                        //         ->orderBy('Sent_Mail.Camp_Code_Id', 'desc')
                        //         ->get();   
    
                        // $row->pre_camp_data = $response;
                        // var_dump($row->Cust_Email_uniqid);
                        
                        $row->Cust_Email_uniqid = strtolower($row->Cust_Email_uniqid);
                        
                        if(in_array(strtolower($row->Cust_Email_uniqid), $current_emails) && 
                        strpos($row->Cust_Email_uniqid, "@") > 0 && !in_array($row->Cust_Email_uniqid, $email_hot_lead)){
                            array_push($hot_leads, $row);  
                            array_push($email_hot_lead, $row->Cust_Email_uniqid);

                        }
    
                        // array_push($hot_leads, $row);  
                        // array_push($emails, $row->Cust_Email_uniqid);
                    }else{
                        if(in_array($row->Cust_Email_uniqid, $current_emails) && strpos($row->Cust_Email_uniqid, "@") > 0){
                            array_push($gone_cold, $row);
                            array_push($gone_cold_temp, $row->Cust_Email_uniqid);
                            
                        }
                       
                        // array_push($emails, $row->Cust_Email_uniqid);
                    }
    
                    
                }

                // $response =  DB::table("Sent_Mail")
                //         ->select(DB::raw('DISTINCT(Sent_Mail.Cust_Email_uniqid)'))     
                //         // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')   
                //         // ->whereIn('Sent_Mail.Camp_Code_Id',$list_ids)
                //         ->where('Sent_Mail.Camp_Code_Id',$current_camp_id)
                //         // ->whereNotIn('Sent_Mail.Cust_Email_uniqid',$email_hot_lead)            
                //         ->where('Sent_Mail.Reading_Time','<',7)
                //         // ->where("Open_Time", "<=", $datezz)
                //         ->orderBy('Sent_Mail.Reading_Time', 'desc')
                //         ->get();

                // $gone_cold = $response;

            }

           
        }

        
                // var_dump($email_hot_lead);die();  


        // foreach ($hot_leads as $row) {

        //     $Cust_Email_uniqid = $row->Cust_Email_uniqid;

        //     $response =  DB::table("Sent_Mail")
        //                     ->join('Campaign','Campaign.Camp_Code_Id','=','Sent_Mail.Camp_Code_Id')
        //                     ->where('Sent_Mail.Cust_Email_uniqid','=',$Cust_Email_uniqid) 
        //                     ->orderBy('Sent_Mail.Camp_Code_Id', 'desc')
        //                     ->get();   

        //     $row->pre_camp_data = $response;
        // }
        
        array_push($responses, $hot_leads); 

        // return Response::json(array(
        //     'error' => false,
        //     'response' => json_encode($responses),
        //     // 'file_list' => $file_list,
        //     'status_code' => 200
        // ));

        $file_list = array();
        
        $file_name = "hot_lead_".$current_camp_id.".csv";
        $file = fopen("report_file/".$file_name,"w"); 

        fputcsv($file, array("Email")); 
                
                
        foreach ($hot_leads as $row) {
            fputcsv($file, array($row->Cust_Email_uniqid));
        }

        fclose($file);

        array_push($file_list, $file_name);
        

        // $emails = array_unique($emails);     


        // return var_dump($hot_leads);     

        // unset($list_ids[count($list_ids) - 1]);
        if($datezz != null){

            $could_spark =  DB::table("Sent_Mail")->select("Sent_Mail.Cust_Email_uniqid","Sent_Mail.Reading_Time",'Sent_Mail.Camp_Code_Id')
        // ->join('Sent_Mail_Detail','Sent_Mail.Camp_Code_Id','=','Sent_Mail_Detail.Camp_Code_Id')
                            // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')
                            ->whereBetween('Sent_Mail.Reading_Time',[3,6])
                            ->where('Sent_Mail.Camp_Code_Id',$current_camp_id)
                            // ->whereIn('Sent_Mail.Camp_Code_Id',$list_ids)
                            // ->whereIn('Sent_Mail.Cust_Email_uniqid',$current_emails)  
                            ->where("Open_Time", "<=", $datezz)
                            ->orderBy('Sent_Mail.Reading_Time', 'desc')
                            ->get();

        }else{
            $could_spark =  DB::table("Sent_Mail")->select("Sent_Mail.Cust_Email_uniqid", "Sent_Mail.Reading_Time",'Sent_Mail.Camp_Code_Id')
        // ->join('Sent_Mail_Detail','Sent_Mail.Camp_Code_Id','=','Sent_Mail_Detail.Camp_Code_Id')
                            // ->join('Customer','Customer.Ip_Address','=','Sent_Mail.Ip_Address')     
                            ->whereBetween('Sent_Mail.Reading_Time',[3,6])
                            ->where('Sent_Mail.Camp_Code_Id',$current_camp_id)
                            ->whereIn('Sent_Mail.Camp_Code_Id',$list_ids)   
                            // ->whereIn('Sent_Mail.Cust_Email_uniqid',$current_emails)  
                            // ->whereNotIn('Sent_Mail.Cust_Email_uniqid', $emails)   
                            ->orderBy('Sent_Mail.Reading_Time', 'desc')         
                            ->get();
        }

        // return Response::json(array(
        //     'error' => false,
        //     'response' => count($could_spark),   
        //     'status_code' => 200
        // ));

        

        if(count($list_ids) <= 2){
            // foreach ($could_spark as $row) {
            //         array_push($gone_cold, $row);
            // }
            $could_spark = array();

        foreach ($current_emails_list as $row) {
            
            if(strpos($row->Cust_Email_uniqid, "@") > 0){    
                array_push($could_spark, $row);

            }
        }
        
            
        }else{

            $temp_could_spark = array();
                $temp_emails = array();
                foreach ($could_spark as $row) {

                    // $abc = DB::table("Sent_Mail")
                    //         // ->where('Sent_Mail.Camp_Code_Id',$current_camp_id)
                    //         ->where('Sent_Mail.Cust_Email_uniqid',$current_camp_id)
                    //         ->get();
                     $row->Cust_Email_uniqid = strtolower($row->Cust_Email_uniqid);
                    if( !in_array($row->Cust_Email_uniqid, $hot_leads) && 
                        !in_array($row->Cust_Email_uniqid, $gone_cold) &&
                        strpos($row->Cust_Email_uniqid, "@") > 0){
                            
                            $count = 0;
                            for($i = 0; $i < count($list_ids); $i++){
                                
                                $temp =  DB::table("Sent_Mail")->select("Sent_Mail.Cust_Email_uniqid")
                                                    ->where('Sent_Mail.Camp_Code_Id',$list_ids[$i])
                                                    ->where('Sent_Mail.Cust_Email_uniqid',$row->Cust_Email_uniqid)
                                                    ->whereBetween('Sent_Mail.Reading_Time',[3,6])   
                                                ->get();      
                                
                                if(count($temp) > 0){
                                    $count = $count + 1;   
                                }
                            }

                            if($count >= 3){
                                array_push($temp_emails, $row->Cust_Email_uniqid);  
                                array_push($temp_could_spark, $row);
                            }

                       

                    }
                    // else{
                    //     array_push($gone_cold, $row);
                    // }
                }

                $could_spark = $temp_could_spark;
        }
        


        array_push($responses, $could_spark);

        $file_name = "could_spark_".$current_camp_id.".csv";
        $file = fopen("report_file/".$file_name,"w"); 

        fputcsv($file, array("Email")); 
                
                
        foreach ($could_spark as $row) {
            fputcsv($file, array($row->Cust_Email_uniqid));
        }

        fclose($file);

        array_push($file_list, $file_name);


        //  return Response::json(array(
        //     'error' => false,
        //     'response' => json_encode($current_emails_list),   
        //     'status_code' => 200
        // ));

        if(count($hot_leads) + count($could_spark) + count($gone_cold) < count($current_emails_list)){
            foreach ($current_emails_list as $row) {
                 $row->Cust_Email_uniqid = strtolower($row->Cust_Email_uniqid);
                if(!in_array($row->Cust_Email_uniqid, $gone_cold_temp) && strpos($row->Cust_Email_uniqid, "@") > 0){
                    // array_push($gone_cold, $row);
                }
            }
        }


        array_push($responses, $gone_cold); 

        $file_name = "gone_cold_".$current_camp_id.".csv";
        $file = fopen("report_file/".$file_name,"w"); 

        fputcsv($file, array("Email")); 
                
                
        foreach ($gone_cold as $row) {
            fputcsv($file, array($row->Cust_Email_uniqid));
        }

        fclose($file);

        array_push($file_list, $file_name);
        
        // return var_dump($responses);      

        return Response::json(array(
            'error' => false,
            'response' => json_encode($responses),
            'file_list' => $file_list,
            'status_code' => 200
        ));
         
    } 

    public function get_open_rate($camp_id, $date){
        
            
            if($date != null){

                $unique_opens = DB::table('Sent_Mail_Detail')
                        ->select(DB::raw("COUNT(Email) as total"))
                        ->where('Camp_Code_Id', $camp_id)
                        ->where("Sm_Open_Time","<=",$date)
                        ->get();
            }else{

                $unique_opens = DB::table('Sent_Mail_Detail')
                        ->select(DB::raw("COUNT(Email) as total"))
                        ->where('Camp_Code_Id', $camp_id)
                        ->get();
            }

            $unique_opens = $unique_opens[0]->total;
            
            $total_people_sending = DB::table('Campaign')->where('Camp_Code_Id', $camp_id)
                        ->get()[0]->Camp_People_Sending;

            return (int)($unique_opens / $total_people_sending *100);
            
    }
    public function get_click_rate($camp_id, $date){

        if($date != null){

            $response = DB::table('master_campaign')
                        ->select('linkly_details.ld_ip')          
                        ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                        ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                        ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                        ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                        ->where("master_campaign_trackingpixel.mctp_mc_track_id",$camp_id)
                        ->groupBy("linkly_details.ld_ip")
                        // ->where('li_sid' ,'=', null)    
                        ->where('linkly_details.ld_timestamp' ,'<=', $date)   
                        ->get(); 


           
        }else{

            $response = DB::table('master_campaign')
                        ->select('linkly_details.ld_ip')          
                        ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                        ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                        ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                        ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                        ->where("master_campaign_trackingpixel.mctp_mc_track_id",$camp_id)
                        ->groupBy("linkly_details.ld_ip")
                        // ->where('li_sid' ,'=', null)    
                        // ->where('linkly_details.ld_timestamp' ,'<=', $date)   
                        ->get(); 
        }
        
        $unique_clicks = count($response);

        $total_people_sending = DB::table('Campaign')->where('Camp_Code_Id', $camp_id)
                                    ->get()[0]->Camp_People_Sending;

            return (int)($unique_clicks / $total_people_sending *100);
        
        
    }

    public function get_industry_range($camp_id){

       $result =  DB::table('master_campaign')
                        ->select('Industry.ind_average_open_rate_range', 'Industry.ind_average_clickthrough_rate_range')
                        ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                        ->join('Campaign','Campaign.Camp_Code_Id','=','master_campaign_trackingpixel.mctp_mc_track_id')
                        ->join('Industry','Industry.ind_id','=','master_campaign.mc_industry')
                        ->where('Campaign.Camp_Code_Id','=',$camp_id)->get();

        return $result;
    }

    public function get_links($camp_id){

        return $response = DB::table('master_campaign')
                    ->select('Linkly.li_id','Linkly.li_name','Linkly.li_unique','Linkly.li_url')             
                    ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                    ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                    ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                    ->where("master_campaign_trackingpixel.mctp_mc_track_id",$camp_id)
                    // ->where('li_sid' ,'!=', null)
                    // ->take(5)
                    ->get();
    }

    public function get_previous_camp($camp_id){
        
        return $previous_camp = DB::table('master_campaign')
                        ->select('Campaign.Camp_Code_Id','Campaign.Camp_Name','Campaign.Tracking_Code','Created_Date')
                        ->join('master_campaign_trackingpixel','master_campaign.mc_id','=' ,'master_campaign_trackingpixel.mctp_mc_id')
                        ->join('Campaign','master_campaign_trackingpixel.mctp_mc_track_id','=','Campaign.Camp_Code_Id')
                        ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                        // ->where('mt_users.mu_username', $email)
                        ->where('Campaign.Camp_Code_Id',"<", $camp_id)
                        ->limit(1)
                        ->orderBy('Created_Date','desc')
                        ->get();
    }

    public function calculate_rate_new(Request $request){

        $camp_id = $request->input('camp_id');

        $email = $request->input('email');

        $open_rate = $this->get_open_rate($camp_id, null);
        $click_rate = $this->get_click_rate($camp_id, null);
        $industry_range = $this->get_industry_range($camp_id);
        $links = $this->get_links($camp_id);

        

        $previous_camp = $this->get_previous_camp($camp_id);


        if(count($previous_camp) > 0){
            $previous_camp = $previous_camp[0]->Camp_Code_Id;
        }else{
            $previous_camp = null;
        }
        
       

 
        
        $Camp_Code_Id = $camp_id;

  

        $link_count = count($links);  

        $link_list = $links;

        // var_dump($link_list);

        foreach ($industry_range as $row) {
            $ind_average_open_rate_range = $row->ind_average_open_rate_range;
            $ind_average_clickthrough_rate_range = $row->ind_average_clickthrough_rate_range;
    }

    $ava_open_rate_from = explode("-", $ind_average_open_rate_range)[0];
    $ava_open_rate_to = explode("-", $ind_average_open_rate_range)[1];

//    var_dump((double)trim($ava_open_rate_from));
//    var_dump((double)($open_rate));  

   $ava_open_rate_to = trim($ava_open_rate_to);
   $ava_open_rate_from = trim($ava_open_rate_from);
    $final_rate = 0;

    // OPEN RATE
    // $open_rate = 0;
    $open_rate_status = 0;
    $open_rate_result = 0;
    if((double)($open_rate) > (double)($ava_open_rate_to)){
        $open_rate_result += 30;
        $open_rate_status = 1;
    }else if((double)($open_rate) >= (double)($ava_open_rate_from) && (double)($open_rate) <= (double)($ava_open_rate_to)){
        $open_rate_result += 20;
        $open_rate_status = 2;
    }else if((double)($open_rate) >= (double)(($ava_open_rate_from) * 75 / 100) && (double)($open_rate) <= (double)(($ava_open_rate_from) * 99.9 / 100)){
        $open_rate_result += 10;
        $open_rate_status = 3;
    }else if((double)($open_rate) < (double)(($ava_open_rate_from) * 75 / 100)){
        $open_rate_result += 0;
        $open_rate_status = 3;
    }

    // var_dump($open_rate_status);

    //TRACTION
    $traction = 0;
    $traction_status = 0;
    $response = DB::table("Sent_Mail_Detail")
                    ->select(DB::raw("COUNT(Email) as total_email"), DB::raw("SUM(Reading_Time) as total_reading_email") )
                    ->where("Camp_Code_Id","=",$Camp_Code_Id)
                    ->groupBy("Camp_Code_Id")
                    ->get();

    foreach ($response as $row) {
        $total_email = $row->total_email;
        $total_reading_email = $row->total_reading_email;
    }

    $overall_reading_time = (int)($total_reading_email / $total_email);

    if($overall_reading_time >= 20){
        $traction += 25;
        $traction_status = 1;
    }
    if($overall_reading_time >= 15 && $overall_reading_time <= 19.99){
        $traction += 20;
        $traction_status = 2;
    }
    if($overall_reading_time >= 11 && $overall_reading_time <= 14.99){
        $traction += 16;
        $traction_status = 2;
    }
    if($overall_reading_time >= 8 && $overall_reading_time <= 10.99){
        $traction += 13;
        $traction_status = 2;
    }
    if($overall_reading_time >= 5 && $overall_reading_time <= 7.99){
        $traction += 10;
        $traction_status = 3;
    }
    if($overall_reading_time >= 3 && $overall_reading_time <= 4.99){
        $traction += 5;    
        $traction_status = 3;
    }
    if($overall_reading_time < 3){
        $traction += 0;
        $traction_status = 3;
    }

    // MOBILE EFFECTIVENESS
    $mobile_affectiveness = 0;
    // $response = DB::table('Device_Types')
    //             ->select('Device_Types.Device_Name',DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
    //             ->join('Sent_Mail_Detail', 'Device_Types.Device_Type_Id','=','Sent_Mail_Detail.Device_Type_Id')
    //             ->where('Sent_Mail_Detail.Camp_Code_Id',$Camp_Code_Id)
    //             ->groupBy('Device_Name')
    //             ->get();
    // $total = 0;
    // $total_mobile = 0;
    // $total_no_mobile = 0;
    // foreach ($response as $row) {
    //     $total += $row->Total_Clicks;
    //     if($row->Device_Name != "Desktop"){
    //         $total_mobile += $row->Total_Clicks;
    //     }else{
    //         $total_no_mobile += $row->Total_Clicks;
    //     }
    // }
    // $mobile_effec = (int)($total_mobile * 100 / $total_no_mobile);
    
    
     $none_mobile_percentage = $this->getReadingStatistics_22($Camp_Code_Id);
        $mobile_percentage = $this->getEmailClientsReadingTimeMobile_22($Camp_Code_Id);

        $mobile_effec = $mobile_percentage / $none_mobile_percentage * 100;
        
    $mobile_effec_status = 0;
    if($mobile_effec > 110){
        $mobile_affectiveness += 20;
        $mobile_effec_status = 1;
    }
    if($mobile_effec >= 100 && $mobile_effec <= 110){
        $mobile_affectiveness += 15;
        $mobile_effec_status = 2;
    }
    if($mobile_effec >= 90 && $mobile_effec <= 99.9){
        $mobile_affectiveness += 10;
        $mobile_effec_status = 2;
    }
    if($mobile_effec >= 60 && $mobile_effec <= 89.9){
        $mobile_affectiveness += 5;
        $mobile_effec_status = 3;
    }
    if($mobile_effec < 60){
        $mobile_affectiveness += 0;
        $mobile_effec_status = 3;
    }

    //BROKEN LINKS
    // $broken_links = 10;
    $broken_link_status = 1;
    
    

    // CLICKTHROUGH

    $ava_click_rate_from = (double)explode("-", $ind_average_clickthrough_rate_range)[0];
    $ava_click_rate_to = (double)explode("-", $ind_average_clickthrough_rate_range)[1];

    $click_through_rate = (double)$click_rate;
    $click_through_points = 0;
    $click_through_status = 4;
    if($click_through_rate > (int)$ava_click_rate_to){
        $click_through_status = 1;
        $click_through_points += 15;
    }
    if($click_through_rate >= (double)$ava_click_rate_from && $click_through_rate <= (double)$ava_click_rate_to){
        $click_through_status = 2;
        $click_through_points += 10;
    }
    if((double)($click_through_rate) >= (double)(($ava_click_rate_from) * 75 / 100) && (double)($click_through_rate) <= (double)(($ava_click_rate_from) * 99.9 / 100)){
        $click_through_points += 5;
        $click_through_status = 3;
    }
    if((double)($click_through_rate) < (double)(($ava_click_rate_from) * 75 / 100)){
        $click_through_points += 0;
        $click_through_status = 3;
    }
    // $click_through_points = 0;

    if(!isset($link_list) || count($link_list) == 0){
        $click_through_status = 4;
        $click_through_points = 0;
    }
    
   

    $response = array();
    
    
    $tip_options = array();
    $title = "";
    if($open_rate_status == 3){
        $tip_options =  DB::table("tip_options")  
                    ->where("type","=",1)  
                    ->get();

        $tips_email_campaign =  DB::table("tips_email_campaign")  
                ->where("email_camp_id","=",$Camp_Code_Id)  
                ->whereBetween("tip_options_id", array(1,15))
                ->get();

        // var_dump($tips_email_campaign);

        if(count($tips_email_campaign) > 0){

            foreach ($tip_options as $row) {
                if($row->id == (int)$tips_email_campaign[0]->tip_options_id){
                    $description = $row->description;
                    $tip_options = array();
                    array_push( $tip_options, $description);
                    break;
                }
            }
            
            // $description = $tip_options[(int)$tips_email_campaign[0]->tip_options_id - 1];
            // $tip_options = array();
            // array_push( $tip_options, $description);
        }else{
            // $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

            // DB::table('tips_email_campaign')->insert(
            //     ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
            // );

            $temp = $tip_options[rand ( 0 , count($tip_options) - 1 )];

            $tip_id = $temp->id;

            DB::table('tips_email_campaign')->insert(
                ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
            );

            $description = $temp->description;
            $tip_options = array();
            array_push( $tip_options, $description);
        }
        
        // $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

        // DB::table('tips_email_campaign')->insert(
        //     ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
        // );
        $title = "POOR OPEN RATE";
    }if($open_rate_status == 1){
        $title = "GREAT OPEN RATE";
    }if($open_rate_status == 2){
        $title = "GOOD OPEN RATE";
    }
    array_push($response, array("open_rate_status" => $open_rate_status,"title" => $title , "tip_options" =>  $tip_options));
    

    $tip_options = array();
    $title = "";
    if($traction_status == 3){
        $tip_options =  DB::table("tip_options")  
                    ->where("type","=",2)  
                    ->get();
        $title = "POOR TRACTION";

        $tips_email_campaign =  DB::table("tips_email_campaign")  
                    ->where("email_camp_id","=",$Camp_Code_Id)  
                    ->whereBetween("tip_options_id", array(16,21))
                    ->get();
        
        if(count($tips_email_campaign) > 0){

        }else{
            $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

            DB::table('tips_email_campaign')->insert(
                ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
            );
        }

        

    }if($traction_status == 1){
        $title = "GREAT TRACTION";
    }if($traction_status == 2){
        $title = "GOOD TRACTION";
    }

    array_push($response, array("traction_status" => $traction_status,"title" => $title , "tip_options" =>  $tip_options));

    $tip_options = array();
    $title = "";

    if($mobile_effec_status == 3){
        $tip_options =  DB::table("tip_options")  
                    ->where("type","=",4)  
                    ->get();
        $title = "POOR MOBILE EFFECTIVENESS";

        $tips_email_campaign =  DB::table("tips_email_campaign")  
                    ->where("email_camp_id","=",$Camp_Code_Id)  
                    ->where("tip_options_id", "=",25)
                    ->get();
        
        if(count($tips_email_campaign) > 0){
            foreach ($tip_options as $row) {
                if($row->id == (int)$tips_email_campaign[0]->tip_options_id){
                    $description = $row->description;
                    $tip_options = array();
                    array_push( $tip_options, $description);
                    break;
                }
            }

        }else{
            // $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

            // DB::table('tips_email_campaign')->insert(
            //     ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
            // );

            $temp = $tip_options[rand ( 0 , count($tip_options) - 1 )];

            $tip_id = $temp->id;

            DB::table('tips_email_campaign')->insert(
                ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
            );

            $description = $temp->description;
            $tip_options = array();
            array_push( $tip_options, $description);
        }

        // $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

        // DB::table('tips_email_campaign')->insert(
        //     ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
        // );

    }if($mobile_effec_status == 1){
        $title = "GREAT MOBILE EFFECTIVENESS";
    }if($mobile_effec_status == 2){
        $title = "GOOD MOBILE EFFECTIVENESS";
    }
    
    array_push($response, array("mobile_effec_status" => $mobile_effec_status,"title" => $title, "tip_options" =>  $tip_options));

    $tip_options = array();
    $title = "";
    $tip_options =  DB::table("tip_options")  
                    ->where("type","=",5)  
                    ->get();

    
    if(isset($link_list)){
       
        $broken_link_list = array();
        for ($i=0; $i < count($link_list); $i++) { 
        // foreach ($link_list as $row) {
            $check_url_status = $this->check_url($link_list[$i]->li_url);
            if ($check_url_status == '200' || $check_url_status == '301' || $check_url_status == '302'){
                // echo "Link Works";
                // $broken_link_status == 0;
            } else{
                // echo "Broken Link";
                if($check_url_status == '999' && strpos($link_list[$i]->li_url, "https://www.linkedin.com/") >= 0){
                }else{

                    $broken_link_status = 0;
                    array_push($broken_link_list, $link_list[$i]->li_name);

                }
                
            }
        }
    }
    
    if($broken_link_status == 1){

        $broken_links = 10;
        
        $title = "NO BROKEN LINKS*";
        if($link_count != 0){
            $tip_options = "All tracked links are functioning correctly.";
        }else{
            $tip_options = "You are not tracking any links";
        }
        
    }else{
        $broken_links = 0;
        $title = "BROKEN LINKS*";
        $links = "<ul style='margin:0 auto'>";
        if(count($broken_link_list) != 0){
            for ($i=0; $i < count($broken_link_list); $i++) { 
                $links .= "<li styel='text-decoration: underline;'>".$broken_link_list[$i]."</li>";
            }
        }
        $links .= "</ul>";
        $tip_options = "The following links are broken. Tell us the correct URL and we can fix it for all future opens.".$links;
    }

    array_push($response, array("broken_link_status" => $broken_link_status,"title" => $title, "tip_options" =>  $tip_options, 'link_count' => $link_count));

    // HOT LEADS SHRINKING
    

    if(isset($previous_camp) && $previous_camp != NULL){
        $curent_camp = (int)$this->getHotLeadsAndGoneCold_22(null, $Camp_Code_Id, $email);

        $pre_camp = (int)$this->getHotLeadsAndGoneCold_22(null, $previous_camp, $email);

    }else{
        $curent_camp = 0;
        $pre_camp = 0;
    }
    

    $hot_lead_status = 0;
    if($curent_camp > $pre_camp){
        $hot_lead_status = 1;
    }
    if($curent_camp == $pre_camp){
        $hot_lead_status = 2;
    }
    if($curent_camp < $pre_camp){
        $hot_lead_status = 3;
    }

    $tip_options = array();
    $title = "";
    $sub_title = "";
    if($hot_lead_status == 3){

        $title = "HOT LEADS SHRINKING";
        $tip_options = "You have lost Hot Leads since your last email. A special offer may win them&nbsp;back.";
        $sub_title = "See Who's Gone Cold";
    }if($hot_lead_status == 1){

        $title = "HOT LEADS GROWING";
        $tip_options = "You have gained Hot Leads since your last&nbsp;email.";
        $sub_title = "See Who's Hot";

    }if($hot_lead_status == 2){

        $title = "HOT LEADS STEADY";
        $tip_options = "A special offer to readers who've demonstrated interest may convert them to Hot&nbsp;Leads.";
        $sub_title = "See Who Could Spark";

    }
    array_push($response, array(
        "hot_lead_status" => $hot_lead_status,
        "title" => $title, 
        "tip_options" =>  $tip_options, 
        "sub_title" => $sub_title
    ));

    

    // $click_through_status = 4;
    $tip_options = array();
    $title = "";
    if($click_through_status == 3){

        

       
                    $title = "POOR CLICKTHROUGH RATE*";

                $tip_options =  DB::table("tip_options")  
                            ->where("type","=",3)  
                            ->get();

                // $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

                // DB::table('tips_email_campaign')->insert(
                //     ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
                // );

                $tips_email_campaign =  DB::table("tips_email_campaign")  
                    ->where("email_camp_id","=",$Camp_Code_Id)  
                    ->whereBetween("tip_options_id", array(22,24))
                    ->get();
        
        if(count($tips_email_campaign) > 0){

            foreach ($tip_options as $row) {
                if($row->id == (int)$tips_email_campaign[0]->tip_options_id){
                    $description = $row->description;
                    $tip_options = array();
                    array_push( $tip_options, $description);
                    break;
                }
            }

            // $description = $tip_options[(int)$tips_email_campaign[0]->tip_options_id - 1];
            // $tip_options = array();
            // array_push( $tip_options, $description);

        }else{
            $temp = $tip_options[rand ( 0 , count($tip_options) - 1 )];

            $tip_id = $temp->id;

            DB::table('tips_email_campaign')->insert(
                ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
            );

            $description = $temp->description;
            $tip_options = array();
            array_push( $tip_options, $description);
        }
        
       


    }if($click_through_status == 1){

        $title = "GREAT CLICKTHROUGH RATE*";
      

    }if($click_through_status == 2){

        $title = "GOOD CLICKTHROUGH RATE*";
    }
    if($click_through_status == 4){

        if($link_count != 0){
        }else{
                    $title = "NO CLICKTHROUGH RATE*";
                    $tip_options =  DB::table("tip_options")  
                    ->where("id","=",31)  
                    ->get();
        }
        
    }

    array_push($response, array("click_through_status" => $click_through_status,"title" => $title, "tip_options" =>  $tip_options,'link_count' => $link_count));


    if($link_count == 0){
        $final_rate = $open_rate_result + $traction + $mobile_affectiveness;     
        $final_rate = round($final_rate * 100 / 75);         
    }else{
        $final_rate = $open_rate_result + $traction + $mobile_affectiveness + $broken_links + $click_through_points;
    }
    array_unshift($response, array("final_rate" => (int)$final_rate));
    

    return Response::json(array(
        'error' => false,
        'response' => $response,
        'status_code' => 200  
    ));
    }

    public function calculate_rate(Request $request){    
        $industry_id = $request->input("industry_id");

        $open_rate = $request->input("open_rate");

        $click_rate = $request->input("click_rate");
        
        $Camp_Code_Id = $request->input("Camp_Code_Id");

        $industry =  DB::table("Industry")
                        ->where("ind_id","=",$industry_id)  
                        ->get();

        $link_count = $request->input("link_count");

        $link_list = $request->input("link_list");

        foreach ($industry as $row) {
                $ind_average_open_rate_range = $row->ind_average_open_rate_range;
                $ind_average_clickthrough_rate_range = $row->ind_average_clickthrough_rate_range;
        }

        $ava_open_rate_from = explode("-", $ind_average_open_rate_range)[0];
        $ava_open_rate_to = explode("-", $ind_average_open_rate_range)[1];

    //    var_dump((double)trim($ava_open_rate_from));
    //    var_dump((double)($open_rate));  

       $ava_open_rate_to = trim($ava_open_rate_to);
       $ava_open_rate_from = trim($ava_open_rate_from);
        $final_rate = 0;

        // OPEN RATE
        // $open_rate = 0;
        $open_rate_status = 0;
        $open_rate_result = 0;
        if((double)($open_rate) > (double)($ava_open_rate_to)){
            $open_rate_result += 30;
            $open_rate_status = 1;
        }else if((double)($open_rate) >= (double)($ava_open_rate_from) && (double)($open_rate) <= (double)($ava_open_rate_to)){
            $open_rate_result += 20;
            $open_rate_status = 2;
        }else if((double)($open_rate) >= (double)(($ava_open_rate_from) * 75 / 100) && (double)($open_rate) <= (double)(($ava_open_rate_from) * 99.9 / 100)){
            $open_rate_result += 10;
            $open_rate_status = 3;
        }else if((double)($open_rate) < (double)(($ava_open_rate_from) * 75 / 100)){
            $open_rate_result += 0;
            $open_rate_status = 3;
        }

        // var_dump($open_rate_status);

        //TRACTION
        $traction = 0;
        $traction_status = 0;
        $response = DB::table("Sent_Mail_Detail")
                        ->select(DB::raw("COUNT(Email) as total_email"), DB::raw("SUM(Reading_Time) as total_reading_email") )
                        ->where("Camp_Code_Id","=",$Camp_Code_Id)
                        ->groupBy("Camp_Code_Id")
                        ->get();

        foreach ($response as $row) {
            $total_email = $row->total_email;
            $total_reading_email = $row->total_reading_email;
        }

        $overall_reading_time = (double)($total_reading_email / $total_email);

        if($overall_reading_time >= 20){
            $traction += 25;
            $traction_status = 1;
        }
        if($overall_reading_time >= 15 && $overall_reading_time <= 19.99){
            $traction += 20;
            $traction_status = 2;
        }
        if($overall_reading_time >= 11 && $overall_reading_time <= 14.99){
            $traction += 16;
            $traction_status = 2;
        }
        if($overall_reading_time >= 8 && $overall_reading_time <= 10.99){
            $traction += 13;
            $traction_status = 2;
        }
        if($overall_reading_time >= 5 && $overall_reading_time <= 7.99){
            $traction += 10;
            $traction_status = 3;
        }
        if($overall_reading_time >= 3 && $overall_reading_time <= 4.99){
            $traction += 5;    
            $traction_status = 3;
        }
        if($overall_reading_time < 3){
            $traction += 0;
            $traction_status = 3;
        }

        // MOBILE EFFECTIVENESS
        $mobile_affectiveness = 0;
        // $response = DB::table('Device_Types')
        //             ->select('Device_Types.Device_Name',DB::raw('COUNT(Sent_Mail_Detail.Device_Type_Id) as Total_Clicks'))
        //             ->join('Sent_Mail_Detail', 'Device_Types.Device_Type_Id','=','Sent_Mail_Detail.Device_Type_Id')
        //             ->where('Sent_Mail_Detail.Camp_Code_Id',$Camp_Code_Id)
        //             // ->where('Sent_Mail_Detail.Reading_Time > ', 8 )
        //             ->groupBy('Device_Name')
        //             ->get();
        // $total = 0;
        // $total_mobile = 0;
        // $total_no_mobile = 0;
        // foreach ($response as $row) {
        //     $total += $row->Total_Clicks;
        //     if($row->Device_Name != "Desktop"){
        //         $total_mobile += $row->Total_Clicks;
        //     }else{
        //         $total_no_mobile += $row->Total_Clicks;
        //     }
        // }
        
        // var_dump( $total_mobile ."-----" . $total_no_mobile); die();   
        
        // getReadingStatistics_22
        
        $none_mobile_percentage = $this->getReadingStatistics_22($Camp_Code_Id);
        $mobile_percentage = $this->getEmailClientsReadingTimeMobile_22($Camp_Code_Id);

        $mobile_effec = $mobile_percentage / $none_mobile_percentage * 100;
        
        // $mobile_effec = ($total_mobile * 100 / $total_no_mobile);
        $mobile_effec_status = 0;
        if($mobile_effec > 110){
            $mobile_affectiveness += 20;
            $mobile_effec_status = 1;
        }
        if($mobile_effec >= 100 && $mobile_effec <= 110){
            $mobile_affectiveness += 15;
            $mobile_effec_status = 2;
        }
        if($mobile_effec >= 90 && $mobile_effec <= 99.9){
            $mobile_affectiveness += 10;
            $mobile_effec_status = 2;
        }
        if($mobile_effec >= 60 && $mobile_effec <= 89.9){
            $mobile_affectiveness += 5;
            $mobile_effec_status = 3;
        }
        if($mobile_effec < 60){
            $mobile_affectiveness += 0;
            $mobile_effec_status = 3;
        }

        //BROKEN LINKS
        // $broken_links = 10;
        $broken_link_status = 1;
        
        

        // CLICKTHROUGH

        $ava_click_rate_from = (double)explode("-", $ind_average_clickthrough_rate_range)[0];
        $ava_click_rate_to = (double)explode("-", $ind_average_clickthrough_rate_range)[1];

        $click_through_rate = (double)$click_rate;
        $click_through_points = 0;
        $click_through_status = 4;
        if($click_through_rate > (double)$ava_click_rate_to){
            $click_through_status = 1;
            $click_through_points += 15;
        }
        if($click_through_rate >= (double)$ava_click_rate_from && $click_through_rate <= (double)$ava_click_rate_to){
            $click_through_status = 2;
            $click_through_points += 10;
        }
        if((double)($click_through_rate) >= (double)(($ava_click_rate_from) * 75 / 100) && (double)($click_through_rate) <= (double)(($ava_click_rate_from) * 99.9 / 100)){
            $click_through_points += 5;
            $click_through_status = 3;
        }
        if((double)($click_through_rate) < (double)(($ava_click_rate_from) * 75 / 100)){
            $click_through_points += 0;
            $click_through_status = 3;
        }
        // $click_through_points = 0;

        if(!isset($link_list) || count($link_list) == 0){
            $click_through_status = 4;
            $click_through_points = 0;
        }
        
       

        $response = array();
        
        
        $tip_options = array();
        $title = "";
        if($open_rate_status == 3){
            $tip_options =  DB::table("tip_options")  
                        ->where("type","=",1)  
                        ->get();

            $tips_email_campaign =  DB::table("tips_email_campaign")  
                    ->where("email_camp_id","=",$Camp_Code_Id)  
                    ->whereBetween("tip_options_id", array(1,15))
                    ->get();

            // var_dump($tips_email_campaign);

            if(count($tips_email_campaign) > 0){

                foreach ($tip_options as $row) {
                    if($row->id == (int)$tips_email_campaign[0]->tip_options_id){
                        $description = $row->description;
                        $tip_options = array();
                        array_push( $tip_options, $description);
                        break;
                    }
                }
                
                // $description = $tip_options[(int)$tips_email_campaign[0]->tip_options_id - 1];
                // $tip_options = array();
                // array_push( $tip_options, $description);
            }else{
                // $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

                // DB::table('tips_email_campaign')->insert(
                //     ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
                // );

                $temp = $tip_options[rand ( 0 , count($tip_options) - 1 )];

                $tip_id = $temp->id;

                DB::table('tips_email_campaign')->insert(
                    ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
                );

                $description = $temp->description;
                $tip_options = array();
                array_push( $tip_options, $description);
            }
            
            // $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

            // DB::table('tips_email_campaign')->insert(
            //     ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
            // );
            $title = "POOR OPEN RATE";
        }if($open_rate_status == 1){
            $title = "GREAT OPEN RATE";
        }if($open_rate_status == 2){
            $title = "GOOD OPEN RATE";
        }
        array_push($response, array("open_rate_status" => $open_rate_status,"title" => $title , "tip_options" =>  $tip_options));
        

        $tip_options = array();
        $title = "";
        if($traction_status == 3){
            $tip_options =  DB::table("tip_options")  
                        ->where("type","=",2)  
                        ->get();
            $title = "POOR TRACTION";

            $tips_email_campaign =  DB::table("tips_email_campaign")  
                        ->where("email_camp_id","=",$Camp_Code_Id)  
                        ->whereBetween("tip_options_id", array(16,21))
                        ->get();
            
            if(count($tips_email_campaign) > 0){

            }else{
                $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

                DB::table('tips_email_campaign')->insert(
                    ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
                );
            }

            

        }if($traction_status == 1){
            $title = "GREAT TRACTION";
        }if($traction_status == 2){
            $title = "GOOD TRACTION";
        }

        array_push($response, array("traction_status" => $traction_status,"title" => $title , "tip_options" =>  $tip_options));

        $tip_options = array();
        $title = "";

        if($mobile_effec_status == 3){
            $tip_options =  DB::table("tip_options")  
                        ->where("type","=",4)  
                        ->get();
            $title = "POOR MOBILE EFFECTIVENESS";

            $tips_email_campaign =  DB::table("tips_email_campaign")  
                        ->where("email_camp_id","=",$Camp_Code_Id)  
                        ->where("tip_options_id", "=",25)
                        ->get();
            
            if(count($tips_email_campaign) > 0){
                foreach ($tip_options as $row) {
                    if($row->id == (int)$tips_email_campaign[0]->tip_options_id){
                        $description = $row->description;
                        $tip_options = array();
                        array_push( $tip_options, $description);
                        break;
                    }
                }

            }else{
                // $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

                // DB::table('tips_email_campaign')->insert(
                //     ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
                // );

                $temp = $tip_options[rand ( 0 , count($tip_options) - 1 )];

                $tip_id = $temp->id;

                DB::table('tips_email_campaign')->insert(
                    ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
                );

                $description = $temp->description;
                $tip_options = array();
                array_push( $tip_options, $description);
            }

            // $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

            // DB::table('tips_email_campaign')->insert(
            //     ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
            // );

        }if($mobile_effec_status == 1){
            $title = "GREAT MOBILE EFFECTIVENESS";
        }if($mobile_effec_status == 2){
            $title = "GOOD MOBILE EFFECTIVENESS";
        }
        
        array_push($response, array("mobile_effec_status" => $mobile_effec_status,"title" => $title, "tip_options" =>  $tip_options));

        $tip_options = array();
        $title = "";
        $tip_options =  DB::table("tip_options")  
                        ->where("type","=",5)  
                        ->get();

        
        if(isset($link_list)){
           
            $broken_link_list = array();
            for ($i=0; $i < count($link_list); $i++) { 
            // foreach ($link_list as $row) {
                $check_url_status = $this->check_url($link_list[$i]["li_url"]);
                if ($check_url_status == '200' || $check_url_status == '301' || $check_url_status == '302'){
                    // echo "Link Works";
                    // $broken_link_status == 0;
                } else{
                    // echo "Broken Link";
                    if($check_url_status == '999' && strpos($link_list[$i]["li_url"], "https://www.linkedin.com/") >= 0){
                    }else{

                        $broken_link_status = 0;
                        array_push($broken_link_list, $link_list[$i]["li_name"]);

                    }
                    
                }
            }
        }
        
        if($broken_link_status == 1){

            $broken_links = 10;
            
            $title = "NO BROKEN LINKS*";
            if($link_count != 0){
                $tip_options = "All tracked links are functioning correctly.";
            }else{
                $tip_options = "You are not tracking any links";
            }
            
        }else{
            $broken_links = 0;
            $title = "BROKEN LINKS*";
            $links = "<ul style='margin:0 auto'>";
            if(count($broken_link_list) != 0){
                for ($i=0; $i < count($broken_link_list); $i++) { 
                    $links .= "<li styel='text-decoration: underline;'>".$broken_link_list[$i]."</li>";
                }
            }
            $links .= "</ul>";
            $tip_options = "The following links are broken. Tell us the correct URL and we can fix it for all future opens.".$links;
        }

        array_push($response, array("broken_link_status" => $broken_link_status,"title" => $title, "tip_options" =>  $tip_options));

        // HOT LEADS SHRINKING
        $previous_camp = $request->input("previous_camp");
        $email = $request->input("email");

        if(isset($previous_camp) && $previous_camp != NULL){
            $curent_camp = (int)$this->getHotLeadsAndGoneCold_22(null, $Camp_Code_Id, $email);

            $pre_camp = (int)$this->getHotLeadsAndGoneCold_22(null, $previous_camp, $email);

        }else{
            $curent_camp = 0;
            $pre_camp = 0;
        }
        

        $hot_lead_status = 0;
        if($curent_camp > $pre_camp){
            $hot_lead_status = 1;
        }
        if($curent_camp == $pre_camp){
            $hot_lead_status = 2;
        }
        if($curent_camp < $pre_camp){
            $hot_lead_status = 3;
        }

        $tip_options = array();
        $title = "";
        $sub_title = "";
        if($hot_lead_status == 3){

            $title = "HOT LEADS SHRINKING";
            $tip_options = "You have lost Hot Leads since your last email. A special offer may win them&nbsp;back.";
            $sub_title = "See Who's Gone Cold";
        }if($hot_lead_status == 1){

            $title = "HOT LEADS GROWING";
            $tip_options = "You have gained Hot Leads since your last&nbsp;email.";
            $sub_title = "See Who's Hot";

        }if($hot_lead_status == 2){

            $title = "HOT LEADS STEADY";
            $tip_options = "A special offer to readers who've demonstrated interest may convert them to Hot&nbsp;Leads.";
            $sub_title = "See Who Could Spark";

        }
        array_push($response, array(
            "hot_lead_status" => $hot_lead_status,
            "title" => $title, 
            "tip_options" =>  $tip_options, 
            "sub_title" => $sub_title
        ));

        

        // $click_through_status = 4;
        $tip_options = array();
        $title = "";
        if($click_through_status == 3){

            

           
                        $title = "POOR CLICKTHROUGH RATE*";

                    $tip_options =  DB::table("tip_options")  
                                ->where("type","=",3)  
                                ->get();

                    // $tip_id = $tip_options[rand ( 0 , count($tip_options) - 1 )]->id;

                    // DB::table('tips_email_campaign')->insert(
                    //     ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
                    // );

                    $tips_email_campaign =  DB::table("tips_email_campaign")  
                        ->where("email_camp_id","=",$Camp_Code_Id)  
                        ->whereBetween("tip_options_id", array(22,24))
                        ->get();
            
            if(count($tips_email_campaign) > 0){

                foreach ($tip_options as $row) {
                    if($row->id == (int)$tips_email_campaign[0]->tip_options_id){
                        $description = $row->description;
                        $tip_options = array();
                        array_push( $tip_options, $description);
                        break;
                    }
                }

                // $description = $tip_options[(int)$tips_email_campaign[0]->tip_options_id - 1];
                // $tip_options = array();
                // array_push( $tip_options, $description);

            }else{
                $temp = $tip_options[rand ( 0 , count($tip_options) - 1 )];

                $tip_id = $temp->id;

                DB::table('tips_email_campaign')->insert(
                    ['email_camp_id' => $Camp_Code_Id, 'tip_options_id' => $tip_id]
                );

                $description = $temp->description;
                $tip_options = array();
                array_push( $tip_options, $description);
            }
            
           


        }if($click_through_status == 1){

            $title = "GREAT CLICKTHROUGH RATE*";
          

        }if($click_through_status == 2){

            $title = "GOOD CLICKTHROUGH RATE*";
        }
        if($click_through_status == 4){

            if($link_count != 0){
            }else{
                        $title = "NO CLICKTHROUGH RATE*";
                        $tip_options =  DB::table("tip_options")  
                        ->where("id","=",31)  
                        ->get();
            }
            
        }

        array_push($response, array("click_through_status" => $click_through_status,"title" => $title, "tip_options" =>  $tip_options));


        if($link_count == 0){
            $final_rate = $open_rate_result + $traction + $mobile_affectiveness;
            $final_rate = round($final_rate * 100 / 75);               
        }else{
            $final_rate = $open_rate_result + $traction + $mobile_affectiveness + $broken_links + $click_through_points;
        }
        array_unshift($response, array("final_rate" => (int)$final_rate ,  "open_rate" =>  $open_rate_result, "traction" =>  $traction, "mobile_affectiveness"=> $mobile_affectiveness));
        

        return Response::json(array(
            'error' => false,
            'response' => $response,
            'status_code' => 200  
        ));
        
    }

    public function get_broken_link_list($mc_id){
       
        $result = DB::table('master_campaign')
                ->select('Linkly.li_id','Linkly.li_name','Linkly.li_url')
                ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
                ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
                ->where('master_campaign.mc_id', $mc_id)
                ->get();

        

                $broken_link_list = array();
                // for ($i=0; $i < count($result); $i++) { 
                foreach ($result as $row) {
                        $check_url_status = $this->check_url($row->li_url);
                        if ($check_url_status == '200' || $check_url_status == '301' || $check_url_status == '302'){
                            // echo "Link Works";
                        } else{
                            // echo "Broken Link";
                            if($check_url_status == '999' && strpos($row->li_url, "https://www.linkedin.com/") >= 0){
                            }else{

                                $row->error_status = $check_url_status;
                                array_push($broken_link_list, $row);

                            }
                            
                        }
                }

                return $broken_link_list;
    }

    public function check_Broken_Links_From_Campaign(Request $request){

        $mc_id = $request->input("mc_id");

        $broken_link_list = $this->get_broken_link_list($mc_id);

        // $result = DB::table('master_campaign')
        //             ->select('Linkly.li_id','Linkly.li_name','Linkly.li_url')
        //             ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
        //             ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
        //             ->where('master_campaign.mc_id', $mc_id)
        //             ->get();

        //             // return Response::json(array(
        //             //     'error' => false,
        //             //     'response' => (object)$result,
        //             //     'status_code' => 200  
        //             // ));

        // $broken_link_list = array();
        // // for ($i=0; $i < count($result); $i++) { 
        // foreach ($result as $row) {
        //     $check_url_status = $this->check_url($row->li_url);
        //         if ($check_url_status == '200' || $check_url_status == '301' || $check_url_status == '302'){
        //             // echo "Link Works";
        //         } else{
        //             // echo "Broken Link";
        //             array_push($broken_link_list, $row);
        //         }
        // }
        $response = array($mc_id, $broken_link_list);
        
        return Response::json(array(
            'error' => false,
            'response' => $response,
            'status_code' => 200  
        ));

    }

    public function fixed_Broken_Links_From_Campaign(Request $request){
        $link_list = $request->input("link_list");

        if(isset($link_list) && count($link_list) != 0){
            for ($i=0; $i < count($link_list); $i++) { 
              
                $link_id = $link_list[$i]["link_id"];
                $link_url = $link_list[$i]["link_url"];

                DB::table('Linkly')
                    ->where("Linkly.li_id", $link_id)
                    ->update(["Linkly.li_url" => $link_url]); 

            }
        }

    }

    public function check_Link_From_Campaign(Request $request){

        $link_obj = $request->input("link_obj");  

        $check_url_status = $this->check_url($link_obj["link_url"]);
        
        if ($check_url_status == '200' || $check_url_status == '301' || $check_url_status == '302'){
            echo json_encode(array("id"=>$link_obj["link_id"], "status" => 1,'code' => $check_url_status));
        } else{

            if($check_url_status == '999' && strpos($link_obj["link_url"], "https://www.linkedin.com/") >= 0){    

                echo json_encode(array("id"=>$link_obj["link_id"], "status" => 1,'code' => $check_url_status));

            }else{
                echo json_encode(array("id"=>$link_obj["link_id"], "status" => 0, 'code' => $check_url_status));

            }
            
        }
    }

    function check_url($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch , CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $headers = curl_getinfo($ch);
        curl_close($ch);
    
        return $headers['http_code'];
    }



    function randomString($_length)
	{
		for($i = 0; $i < 20 ; $i++)
		{
			echo $this->randString($_length).'</br>';
		}
	}
	function randString($length) {
		$char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$char = str_shuffle($char);
		for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
			$rand .= $char{mt_rand(0, $l)};
		}
		return $rand;
    }

    function checkCampCodeExist($_camp_code)
	{
        $count = DB::table("Campaign")->where('Camp_Code',$_camp_code)->count();
			
		if($count >= 1){
			return 1;
		}
		else{
			
			return 0;
		}
    }
    
    public function getReportNew(Request $request){

        $re_code = $request->input("re_code");  

        $response =  DB::table("report_manager")
                        ->select('*')
                        ->where('re_code',$re_code)
                        ->get();

        $options = $response[0]->re_selected;
        $logo = $response[0]->re_logo;
        $created_date = $response[0]->re_created_date;
        $email = $response[0]->re_user_email; 
        $camp_id = $response[0]->re_camp_id; 

        $camp = $this->getCampaign_2($camp_id);

        $camp_name = $camp[0]->Camp_Name; 
        $master_camp_id = $camp[0]->mctp_mc_id;
        $Camp_People_Sending = $camp[0]->Camp_People_Sending;
        
        $master_camp = DB::table("master_campaign")
                        ->select('*')
                        ->where('mc_id',$master_camp_id)
                        ->get();
        $mc_auto_top_up = $master_camp[0]->mc_auto_top_up;
        $camp_industry = $master_camp[0]->mc_industry;

        $response = array(
            're_selected' => $options,
            're_logo' => $logo,
            're_created_date' => $created_date,
            're_user_email' => $email,
            're_camp_id' => $camp_id,
            'Camp_Name' => $camp_name,
            'mctp_mc_id' => $master_camp_id,
            'mc_auto_top_up' => $mc_auto_top_up,
            'mc_industry' => $camp_industry,
            'Camp_People_Sending' => $Camp_People_Sending
        );

        return Response::json(array(
            'error' => false,
            'response' => $response,
            'status_code' => 200  
        ));

    }

    public function getTotal_Unique_Open($id){


        $unique_opens = DB::table('Sent_Mail')
        ->select(DB::raw("COUNT(Cust_Email_uniqid) as total"), DB::raw("SUM(Forward) as forward_total"), DB::raw("SUM(Print) as print_total"))
        ->where('Camp_Code_Id', $id)
        ->get();

$response = DB::table('master_campaign')
    ->select('master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_created_date','master_campaign.mc_order_id','master_campaign.mc_order_hash','master_campaign.mc_opens','master_campaign.mc_bonus','master_campaign.mc_set_cap')
    ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
    ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
    ->join('Campaign','Campaign.Camp_Code_Id','=','master_campaign_trackingpixel.mctp_mc_track_id')                
    ->where('Campaign.Camp_Code_Id',$id)
    ->get();

foreach ($response as $row1) {

                    $total_opens_assigned = DB::table('master_campaign_detail')
                        ->select(DB::raw('SUM(balance) as total_opens_assigned'))
                        ->where('mc_id', $row1->mc_id)
                        ->groupBy('mc_id')
                        ->get();
                    if(count($total_opens_assigned) == 0){
                        $row1->total_opens_assigned = 0;
                    }else{
                        $row1->total_opens_assigned = $total_opens_assigned[0]->total_opens_assigned;
                        
                    }

                    $row1->total_opens_assigned = $row1->total_opens_assigned + $row1->mc_opens;
                }

                $bonus = $response[0]->mc_bonus;   
                $limit = $response[0]->total_opens_assigned; 

                $response = DB::table('Sent_Mail_Detail')
                ->select(DB::raw('COUNT(Email) as Opens'))
                ->where('Camp_Code_Id',$id)     
                ->get(); 

                $limit += $bonus;

                if($limit < $response[0]->Opens){

                // $limit += $bonus;                     

                        for ($i = $limit; $i >= 0 ; $i = $i - 5) {                
                    
                            $response = DB::table('Sent_Mail_Detail')
                                ->select(DB::raw("DATE_FORMAT(Sm_Open_Time, '%Y-%m-%d %H:%i:%s') as Sm_Open_Time"))             
                                ->where('Camp_Code_Id',$id) 
                                ->orderBy("Sm_Open_Time")   
                                ->limit($i)  
                                ->get(); 
                                
                                

                        $date = $response[count($response) - 1]->Sm_Open_Time;

                        $response = DB::table('master_campaign')
                                ->select('linkly_details.ld_timestamp')          
                                ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                                ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                                ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                                ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                                // ->where('li_sid' ,'=', null)    
                                ->where('linkly_details.ld_timestamp' ,'<=', $date)   
                                ->get();   

                        if($i + count($response) <= $limit){    

                            $data = array(
                                "date" => $date,                             
                                "opens" => $i,
                                "clicks" => count($response),
                                "total_opens_assigned" => $limit     
                            );
                            $total_opens_assigned = $limit;
                            $clicks = count($response);

                            $response = DB::table('master_campaign')
                                ->select('linkly_details.ld_ip')          
                                ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                                ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                                ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                                ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                                ->groupBy("linkly_details.ld_ip")
                                // ->where('li_sid' ,'=', null)    
                                // ->where('linkly_details.ld_timestamp' ,'<=', $date)   
                                ->get(); 


                    $unique_clicks = count($response);


                            $opens = $i;
                            // return Response::json(array(
                            //     'error' => false,
                            //     'response' => json_encode($data),              
                            //     'status_code' => 200      
                            // ));
                        }      
                    }

                }else{

                    

                    $date = null;
                    $opens = $response[0]->Opens;
                    $total_opens_assigned = $limit;
                    $response = DB::table('master_campaign')
                                ->select('linkly_details.ld_timestamp')          
                                ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                                ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                                ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                                ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                                // ->where('li_sid' ,'=', null)    
                                // ->where('linkly_details.ld_timestamp' ,'<=', $date)   
                                ->get();  
                    $clicks = count($response);

                    $response = DB::table('master_campaign')
                                ->select('linkly_details.ld_ip')          
                                ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                                ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                                ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                                ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                                ->groupBy("linkly_details.ld_ip")
                                // ->where('li_sid' ,'=', null)    
                                // ->where('linkly_details.ld_timestamp' ,'<=', $date)   
                                ->get(); 


                    $unique_clicks = count($response);
                    $data = array(
                        "date" => $date,                             
                        "opens" => $opens,
                        "total_opens_assigned" => $limit     
                    );
                }
                

                if($date != null){
                    $unique_opens = DB::table('Sent_Mail')
                            ->select(DB::raw("COUNT(DISTINCT(Cust_Email_uniqid)) as total"), DB::raw("SUM(Forward) as forward_total"), DB::raw("SUM(Print) as print_total"))
                            ->where('Camp_Code_Id', $id)
                            ->where("Open_Time","<=",$date)
                            ->get();  
        
        
                    $unique_forward = DB::table('Sent_Mail')
                        ->select(DB::raw("COUNT(Forward) as forward_unique"))
                        ->where('Camp_Code_Id', $id)
                        ->where("Open_Time","<=",$date)
                        ->where('Forward',"!=", 0)
                        ->get();  
        
                    $unique_print = DB::table('Sent_Mail')
                        ->select(DB::raw("COUNT(Print) as print_unique"))
                        ->where('Camp_Code_Id', $id)
                        ->where("Open_Time","<=",$date)    
                        ->where('Print',"!=", 0)
                        ->get(); 
                              
        
                }else{
                    $unique_opens = DB::table('Sent_Mail')
                            ->select(DB::raw("COUNT(DISTINCT(Cust_Email_uniqid)) as total"), DB::raw("SUM(Forward) as forward_total"), DB::raw("SUM(Print) as print_total"))
                            ->where('Camp_Code_Id', $id)
                            ->get();  
                     
                
                    $unique_forward = DB::table('Sent_Mail')
                        ->select(DB::raw("COUNT(Forward) as forward_unique"))
                        ->where('Camp_Code_Id', $id)
                        ->where('Forward',"!=", 0)
                        ->get();  
        
                    $unique_print = DB::table('Sent_Mail')
                        ->select(DB::raw("COUNT(Print) as print_unique"))
                        ->where('Camp_Code_Id', $id)
                        ->where('Print',"!=", 0)
                        ->get(); 
                            
                }       
                 
        
                $response = DB::table('master_campaign')
                        ->select('master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_created_date','master_campaign.mc_order_id','master_campaign.mc_order_hash','master_campaign.mc_opens','Campaign.Camp_People_Sending')
                        ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                        ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                        ->join('Campaign','Campaign.Camp_Code_Id','=','master_campaign_trackingpixel.mctp_mc_track_id')                
                        ->where('Campaign.Camp_Code_Id',$id)
                        ->get();
        
                foreach ($response as $row1) {
        
                        $total_opens_assigned = DB::table('master_campaign_detail')
                            ->select(DB::raw('SUM(balance) as total_opens_assigned'))
                            ->where('mc_id', $row1->mc_id)
                            ->groupBy('mc_id')
                            ->get();
                        if(count($total_opens_assigned) == 0){
                            $row1->total_opens_assigned = 0;
                        }else{
                            $row1->total_opens_assigned = $total_opens_assigned[0]->total_opens_assigned;
                            
                        }
                        $row1->unique_opens = $unique_opens[0]->total;
                        $row1->forward_total = $unique_opens[0]->forward_total;
                        $row1->print_total = $unique_opens[0]->print_total;  
                        
                        $row1->unique_forward = $unique_forward[0]->forward_unique;
                        $row1->unique_print = $unique_print[0]->print_unique;  
        
                        $row1->total_opens_assigned = $row1->total_opens_assigned + $row1->mc_opens;
                        $row1->opens = $opens;
                        $row1->clicks = $clicks;
                        $row1->unique_clicks = $unique_clicks;

                        
                        

                }

                return Response::json(array(
                                'error' => false,
                                'response' => json_encode($response),              
                                'status_code' => 200      
                            ));
    }

}
