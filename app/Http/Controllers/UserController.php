<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

use Illuminate\Contracts\Encryption\DecryptException;



class UserController extends Controller
{
    
    public function checkUserExist(Request $request){
        
            $email = $request->input('email');
            
            $result = DB::table('mt_users')->where('mu_username', $email)->get();
           
            if(count($result) == 0){

                return Response::json(array(
                    'error' => false,
                    'response' => 0,
                    'message' => "User is not exist",
                    'status_code' => 200
                ));

            }else{

                echo json_encode(array(
                    'error' => false,
                    'response' => 1,
                    'message' => "User is exist",
                    'status_code' => 200
                ));
            }

        }

    public function createUser(Request $request){

        $name = $request->input('name');
		$password = $request->input('password');
		$profile = $request->input('profile');
		$active = $request->input('active');
        $email = $request->input('email');

        $user = DB::table('mt_users')->where('mu_username',$email)->get();
        
        $verify_email = $request->input('verify_email');


        if($user->count() == 0){

            DB::table('mt_users')->insert([
                'mu_id' => null,
                'mu_name' => $name,
                'mu_username' => $email,
                'mu_profile' => $profile,
                'mu_active' => $active,
                'mu_password' => encrypt($password),
                'mu_hash' => Hash::make($password),
                'mu_api_key' => hash("tiger192,4",$email),
                'verify_email' => $verify_email 
            ]);

            return Response::json(array(
                'error' => false,
                'message' => 'Create user successfully',
                'email' => $email,
                'status_code' => 200
            ));

        }else{

            return Response::json(array(
                'error' => true,
                'message' => 'Create user unsuccessfully',
                'status_code' => 200
            ));

        }
    }

    public function updateUser(Request $request){

		$email = $request->input('email');
		$password_old = $request->input('password_old');
        $password_new = $request->input('password_new');
        
        $user = DB::table('mt_users')->where('mu_username',$email)->get();

        
        if($user->count() > 0 && $password_old != $password_new){

            DB::table('mt_users')
                ->where('mu_username','=',$email)
                ->update([
                    'mu_password' => encrypt($password_new)
                ]);

                return Response::json(array(
                    'error' => false,
                    'message' => 'Update user successfully',
                    'status_code' => 200
                ));

        }else{

            return Response::json(array(
                'error' => true,
                'message' => 'Update user unsuccessfully',
                'status_code' => 200
            ));
        }
    }

    public function deleteUser(Request $request){

        $id = $request->input('id');

        DB::table('mt_users')->where('mu_id',$id)->delete();

        return Response::json(array(
            'error' => false,
            'message' => 'Delete user unsuccessfully',
            'status_code' => 200
        ));
    }   

}
