<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use Illuminate\Support\Facades\DB;
// use App\Campaign;

class AdTrackerController extends Controller
{
    // Get Number of clicks from linkly detail table 
    function getLinklyActivitiesById($id, Request $request){

        $response = array();

        // $number_of_clicks = DB::table('linkly_details')
        //         ->select(DB::raw('DATE(ld_timestamp) as click'),DB::raw('COUNT(DATE(ld_timestamp)) as click_total'))
        //         ->where("ld_id",$id)
        //         ->groupBy('click')
        //         ->get();

        // array_push($response, $number_of_clicks);

        $datezz = $request->input("datezz");


        $open_timeline = $this->getOpensBaseOnTimeline($id, $datezz);      
        
        array_push($response, $open_timeline);

        if($datezz != null){

            $browser = DB::table('linkly_details')
                    ->select(DB::raw('COUNT(ld_browser) as browser_total'),'ld_browser as browser')
                    ->where("ld_id",$id)
                    ->where("ld_timestamp","<=",$datezz )
                    ->groupBy('browser')
                    ->orderBy('browser_total','desc')
                    ->get();

        array_push($response, $browser);

        $referer = DB::select(DB::raw('select SUM(new_table.referer_total) as referer_total, new_table.referer from 
        
                                        (select COUNT(ld_referer) as referer_total , SUBSTRING_INDEX(replace(replace(replace(replace(ld_referer,"http://",""),"https://",""), "wwww.", ""), ".com", ""),"/",1) as referer 
                                        from linkly_details where ld_id = '.$id.' AND ld_timestamp <= "'.$datezz.'" group by referer) as new_table 
                                        
                                        group by new_table.referer order by referer_total desc '));                             
                // ->where("ld_id",$id)
                // ->groupBy('referer')     
                // ->get();     

                // $new_array = array();
                // foreach ($referer as $row) {
                //     // $row->referer = str_replace("http://", "", $row->referer);       
				// 	// $row->referer = str_replace("https://", "", $row->referer);
				// 	// $row->referer = str_replace("wwww.", "", $row->referer);
				// 	// $row->referer = str_replace(".com", "", $row->referer);
                //     $row->referer = explode("/", $row->referer)[0];              
                    
                //     $object = new \stdClass();
                //     $object->referer = $row->referer;
                //     $object->referer_total = $row->referer_total;

                //     array_push($new_array, $object);      
                // }


        array_push($response, $referer);

        $platform = DB::table('linkly_details')
        ->select(DB::raw('COUNT(ld_platform) as platform_total'),'ld_platform as platform')
        ->where("ld_id",$id)
        ->where("ld_timestamp","<=",$datezz )
        ->groupBy('platform')
        ->orderBy('platform_total','desc')
        ->get();

        array_push($response, $platform);

        $country = DB::table('linkly_details')
            ->select('ld_country as countryname',DB::raw('COUNT(ld_country_id) as country_total'),DB::raw('lower(ld_country_id) as countrycode'))
            ->where("ld_id",$id)
            ->where("ld_timestamp","<=",$datezz )
            ->where("ld_country", '!=', 'NULL')
            ->groupBy('countryname')   
            ->orderBy('country_total','desc')
            ->get();

        array_push($response, $country);

        }else{

            $browser = DB::table('linkly_details')
                    ->select(DB::raw('COUNT(ld_browser) as browser_total'),'ld_browser as browser')
                    ->where("ld_id",$id)
                    ->groupBy('browser')
                    ->orderBy('browser_total','desc')
                    ->get();

        array_push($response, $browser);

        $referer = DB::select(DB::raw('select SUM(new_table.referer_total) as referer_total, new_table.referer from 
        
                                        (select COUNT(ld_referer) as referer_total , SUBSTRING_INDEX(replace(replace(replace(replace(ld_referer,"http://",""),"https://",""), "wwww.", ""), ".com", ""),"/",1) as referer from linkly_details where ld_id = '.$id.' group by referer) as new_table 
                                        
                                        group by new_table.referer order by referer_total desc '));                          
                // ->where("ld_id",$id)
                // ->groupBy('referer')     
                // ->get();     

                // $new_array = array();
                // foreach ($referer as $row) {
                //     // $row->referer = str_replace("http://", "", $row->referer);       
				// 	// $row->referer = str_replace("https://", "", $row->referer);
				// 	// $row->referer = str_replace("wwww.", "", $row->referer);
				// 	// $row->referer = str_replace(".com", "", $row->referer);
                //     $row->referer = explode("/", $row->referer)[0];              
                    
                //     $object = new \stdClass();
                //     $object->referer = $row->referer;
                //     $object->referer_total = $row->referer_total;

                //     array_push($new_array, $object);      
                // }


        array_push($response, $referer);

        $platform = DB::table('linkly_details')
        ->select(DB::raw('COUNT(ld_platform) as platform_total'),'ld_platform as platform')
        ->where("ld_id",$id)
        ->groupBy('platform')
        ->orderBy('platform_total','desc')
        ->get();

        array_push($response, $platform);

        $country = DB::table('linkly_details')
            ->select('ld_country as countryname',DB::raw('COUNT(ld_country_id) as country_total'),DB::raw('lower(ld_country_id) as countrycode'))
            ->where("ld_id",$id)
            ->where("ld_country", '!=', 'NULL')
            ->groupBy('countryname')
            ->orderBy('country_total','desc')
            ->get();

        array_push($response, $country);
        
        }
        


        

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));

    }

    public function get_email_click($id, $link_id, Request $request){ 
        
        $datezz = $request->input("datezz"); 

        if($datezz != null){
            $data = DB::select( DB::raw("Select linkly_details.ld_email as email, COUNT(linkly_details.ld_email) as total from linkly_details
                        where linkly_details.ld_id = :ld_id AND linkly_details.ld_timestamp <= :datezz GROUP BY linkly_details.ld_email ORDER BY total desc "), 
                        array('ld_id' => $link_id, 'datezz' => $datezz));      
        }else{
            $data = DB::select( DB::raw("Select linkly_details.ld_email as email, COUNT(linkly_details.ld_email) as total from linkly_details 
                        WHERE linkly_details.ld_id = :ld_id GROUP BY linkly_details.ld_email ORDER BY total desc "), array('ld_id' => $link_id)); 
        }

        
                        
        $file_name = "report_".$id."_".$link_id.".csv";       

        $file = fopen("report_file/".$file_name,"w");                
        foreach ($data as $row) {
            fputcsv($file, array($row->email, $row->total));          
        }
        fclose($file);

        return Response::json(array(  
            'error' => false,
            'response' => json_encode($data),
            'report_file' => $file_name,                      
            'status_code' => 200
        ));    
    }


    public function openHourlyClickActivity($id, $val, $datess, Request $request){ 
        
        $datezz = $request->input("datezz"); 

        if($datezz != null){

            $main_data = DB::select( DB::raw("Select (min(ld_timestamp)) as start, (max(ld_timestamp)) as end, DATEDIFF(  max(ld_timestamp), min(ld_timestamp)) as diff from linkly_details 

                                        join Linkly on Linkly.li_id = linkly_details.ld_id join master_campaign_link on master_campaign_link.mcl_li_id = Linkly.li_id
                                        
                                        where master_campaign_link.mcl_mc_id = :id AND linkly_details.ld_timestamp <= :datezz"), array('id' => $id, 'datezz' => $datezz)); 
        
        }else{
            $main_data = DB::select( DB::raw("Select (min(ld_timestamp)) as start, (max(ld_timestamp)) as end, DATEDIFF(  max(ld_timestamp), min(ld_timestamp)) as diff from linkly_details 

                                        join Linkly on Linkly.li_id = linkly_details.ld_id join master_campaign_link on master_campaign_link.mcl_li_id = Linkly.li_id
                                        
                                        where master_campaign_link.mcl_mc_id = :id"), array('id' => $id));      
        }

          
        
        $basic = $main_data[0];   

        $long = $basic->diff;

        $steps = 15;

        $interval = 4;              

        

        $init = 0;
        $start_first = $basic->start;
        $end_first = $basic->end;   
        
        // return Response::json(array(
        //     'error' => false,
        //     'response' => json_encode($start_first),           
        //     'status_code' => 200
        // ));
        
        $link_ids = DB::select( DB::raw("Select Distinct(Linkly.li_id) from linkly_details 

                                        join Linkly on Linkly.li_id = linkly_details.ld_id join master_campaign_link on master_campaign_link.mcl_li_id = Linkly.li_id
                                        
                                        where master_campaign_link.mcl_mc_id = :id"), array('id' => $id)); 
        $new_link_array = array();                                
        foreach ($link_ids as $row) {
            array_push($new_link_array, $row->li_id);    
        }

        $result = array();

        // $datess= $request->input("datess");   
        $main_data3 = null; 

        if($val == 0){   

            $datess11 = $request->input("datess"); 
            
            $datess = date('Y-m-d H:i:s', StrToTime ( $datess11 ));
                    $datess = new \DateTime($datess);    
                    $datess = $datess->format("Y-m-d");

            $start_first = date('Y-m-d H:i:s', StrToTime ( $start_first ));
            $start_first = new \DateTime($start_first);    
            $start_first = $start_first->format("Y-m-d");         
            

            if($datess == $start_first){     
                $start_first = $request->input("datess");     
            }
            else{
                $datess = date('Y-m-d H:i:s', StrToTime ( $datess11 ));          

                $datess = new \DateTime($datess);    
                $datess = $datess->setTime(8,0,0);     

                $datess = $datess->format("Y-m-d H:i:s");

                $start_first = $datess;       
            }

            $t1 = StrToTime ( $start_first );
            $t2 = date('Y-m-d H:i:s', StrToTime ( $start_first . ' + 1days'));      

            $t2 = new \DateTime($t2);    
            $t2 = $t2->setTime(8,0,0);     

            $t2 = $t2->format("Y-m-d H:i:s");


            // return Response::json(array(
            //     'error' => false,
            //     'response' => json_encode($t2),                     
            //     'status_code' => 200   
            // ));  

            
            $t2 = StrToTime ( $t2 );   

            $diff = $t2 - $t1;
            $hours = (int)$diff / ( 60 * 60 );   
             

            $interval = 1;

            $steps = (int)($hours / $interval);     
            $status = "hours";  
            $date_format = "h a";  

            $start = date('Y-m-d H:i:s', strtotime($start_first));
            $end = date('Y-m-d H:i:s', strtotime($start_first. ' + 1'."days"));

            $main_data3 = DB::table('Sent_Mail')        
                    ->select('Cust_Email_uniqid as email')
                    ->join('linkly_details','Sent_Mail.Ip_Address','=','linkly_details.ld_ip')   
                    ->join("Linkly","linkly_details.ld_id","=","Linkly.li_id")
                    ->whereIn('Linkly.li_id',$new_link_array) 
                    ->whereBetween('Open_Time',array($start, $end))    
                    ->groupBy('email')
                    ->orderBy('email')     
                    ->get();       
        
        }

        else{

             

            if($val == 1){

                $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 );   
    
                $interval = 1;
    
                $steps = (int)($hours / $interval);  
    
                $steps = 20;
                $status = " hours";
                $date_format = "d M, h a";
            }
            if($val == 3){
    
                $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 );   
    
                $interval = 8;
    
                $steps = (int)($hours / $interval);    
                $status = "hours";  
                $date_format = "d M, h a";  
            }
            if($val == 2){

                $start_first = date('Y-m-d H:i:s', StrToTime ( $start_first ));      

                $start_first = new \DateTime($start_first);    
                // $start_first = $start_first->setTime(8,0,0);     

                $start_first = $start_first->format("Y-m-d H:i:s");

                $end_first = date('Y-m-d H:i:s', StrToTime ( $end_first ));      

                $end_first = new \DateTime($end_first);    
                // $end_first = $end_first->setTime(8,0,0);          

                $end_first = $end_first->format("Y-m-d H:i:s");          

    
                $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 * 24 );  
                
                $interval = 1;
                
                // if($hours > 15){  
                    
                //   $interval = round($hours/10);
                // }
    
                $steps = 3;
                
                $steps = (int)($hours / $interval);       
                // $steps = $steps + 1;
    
                // $steps = 10;  

                $status = " days";  
                $date_format = "d M"; 
            }
            if($val == 4){

                $start_first = date('Y-m-d H:i:s', StrToTime ( $start_first ));      

                $start_first = new \DateTime($start_first);    
                // $start_first = $start_first->setTime(8,0,0);     

                $start_first = $start_first->format("Y-m-d H:i:s");

                $end_first = date('Y-m-d H:i:s', StrToTime ( $end_first ));      

                $end_first = new \DateTime($end_first);    
                // $end_first = $end_first->setTime(8,0,0);          

                $end_first = $end_first->format("Y-m-d H:i:s");          

    
                $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 * 24 ); 
    
                $interval = 1;
                $steps = 3;
                $steps = (int)($hours / $interval);        
    
                $i = 0;
                while (($t1 = strtotime("+1 MONTH", $t1)) <= $t2) {
                    $i++;
                }
                
                $steps = $i;
                $status = "month";
                $date_format = "M Y"; 
            }
            
             if($val == 5){     
                
                $datess = $request->input("datess"); 
                $from_d = $datess["from_d"];
                $to_d = $datess["to_d"];
                
                $start_first = $from_d . " 00:00:00";
                $end_first = $to_d. " 00:00:00"; 
                
                 $t1 = StrToTime ( $start_first );
                $t2 = StrToTime ( $end_first );
                
                $diff = $t2 - $t1;
                $hours = $diff / ( 60 * 60 * 24 );
                
                 $interval = 1;
                // $steps = 3;   
                $steps = (int)($hours / $interval); 
                
                // $steps = 10;
                $status = "days";
                $date_format = "d M Y";
                
                // return Response::json(array(  
                //         'error' => false,
                //         'response' => $start_first."----".$end_first, 
                //         'status_code' => 200
                //     ));
                
            }
        }
       
		
		// $result[] = array('time' => $start_first, 'unique_opens' => 0); 

        for ($i=0; $i <= $steps; $i++) { 
             
			$future = 0;
            $future = $init + (int)$interval;  
            
            
            if($val == 4){

                $start = date('Y-m-d H:i:s', strtotime($start_first. ' + '.$init.$status));

                $start = new \DateTime($start);  
                $month = $start->format('m');
                $year = $start->format('Y');

                $start = date("$year-$month-01 00:00:00");

                $start = date('Y-m-d H:i:s', strtotime($start));

                $end = date('Y-m-d H:i:s', strtotime($start. ' + '.$future.$status));

                // return Response::json(array(
                //     'error' => false,
                //     'response' => $start."---".$end, 
                //     'status_code' => 200
                // ));
                
                // $end = $end->modify('first day of this month')->format("Y-m-d H:i:s");
                // $end = StrToTime ( $end );
            }else{
                $start = date('Y-m-d H:i:s', strtotime($start_first. ' + '.$init.$status));
                $end = date('Y-m-d H:i:s', strtotime($start_first. ' + '.$future.$status));
            }   

            if($datezz != null){

                $main_data = DB::table('linkly_details')
                        ->select( DB::raw('Count(*) as unique_opens'))
                        ->whereIn('ld_id',$new_link_array)
                        ->where('ld_timestamp','<=', $datezz)   
                        ->whereBetween('ld_timestamp',array($start, $end))
                        ->get();   

            $main_data2 = DB::table('Linkly')         
                    ->select( "ld_ip")           
                    ->join("linkly_details","linkly_details.ld_id","=","Linkly.li_id")
                    ->whereIn('Linkly.li_id',$new_link_array) 
                    ->where('ld_timestamp','<=', $datezz)               
                    ->whereBetween('ld_timestamp',array($start, $end))     
                    ->groupBy("linkly_details.ld_ip")    
                    ->get();  

            }else{

                $main_data = DB::table('linkly_details')
                        ->select( DB::raw('Count(*) as unique_opens'))
                        ->whereIn('ld_id',$new_link_array)   
                        ->whereBetween('ld_timestamp',array($start, $end))
                        ->get();   

            $main_data2 = DB::table('Linkly')         
                    ->select( "ld_ip")           
                    ->join("linkly_details","linkly_details.ld_id","=","Linkly.li_id")
                    ->whereIn('Linkly.li_id',$new_link_array)   
                    ->whereBetween('ld_timestamp',array($start, $end))     
                    ->groupBy("linkly_details.ld_ip")    
                    ->get();  
            }

            

                    // return Response::json(array(
                    //     'error' => false,
                    //     'response' => json_encode($main_data2),     
                    //     'status_code' => 200
                    // ));       
			
            // $main_data = DB::select( DB::raw("SELECT Count(*) as unique_opens  
            //                                     FROM `Sent_Mail` 
            //                                     WHERE `Camp_Code_Id` = :id And date(Open_Time) between :start  and :end "),
            //                                         array(
            //                                             'id' => $id,
            //                                             'start' => $start,
            //                                             'end' => $end
            //                                         ));
            $basic_second = $main_data[0];

           

            $start = date('Y-m-d H:i:s', strtotime($start));

            // date_default_timezone_set('Australia/Canberra');

            // $start = new \DateTime('2018-01-21 20:16:44', new \DateTimeZone('Australia/Canberra')); 
            
            // $test = new \DateTimeZone('Australia/Sydney');   
            // $gmt = new \DateTimeZone('AEDT');

            $UTC = new \DateTimeZone("Australia/Sydney");     
            $newTZ = new \DateTimeZone("America/New_York");
            $date = new \DateTime($start, $newTZ  );
            $date->setTimezone( $UTC);    
            $date = $date->format($date_format);         



			$result[] = array('time' => $date, 'unique_opens' => $basic_second->unique_opens, 'uniq_opens' => count($main_data2));          
			
			
			$init = $future;

        }

        return Response::json(array(
            'error' => false,
            'response' => json_encode($result),
            'emails' => $main_data3,
            'status_code' => 200
        ));

    }



    public function getOpensBaseOnTimeline($code, $datezz){

        

        $main_data = DB::table('linkly_details')
                ->select(DB::raw('COUNT(*) as total'))
                ->where("ld_id",$code)
                ->get();
		
		// $main_data = $this->db->query("SELECT COUNT(*) as total FROM `linkly_details` WHERE `ld_id` = $code");
		
		if(count($main_data) == 0)
		{
			return array();
        }

        if($datezz != null){
            $main_data = DB::table('linkly_details')
                ->select(DB::raw('TIMESTAMPDIFF(second,MIN(ld_timestamp)'), DB::raw('MAX(ld_timestamp)) as Diff'), 
                DB::raw('UNIX_TIMESTAMP(MIN(ld_timestamp)) as open_time'), DB::raw('TIMEDIFF(MIN(ld_timestamp)'), 
                DB::raw('MAX(ld_timestamp)) as tt'))
                ->where("ld_id",$code)
                ->where("ld_timestamp","<=", $datezz)
                ->get();
        }else{
            $main_data = DB::table('linkly_details')
                ->select(DB::raw('TIMESTAMPDIFF(second,MIN(ld_timestamp)'), DB::raw('MAX(ld_timestamp)) as Diff'), DB::raw('UNIX_TIMESTAMP(MIN(ld_timestamp)) as open_time'), DB::raw('TIMEDIFF(MIN(ld_timestamp)'), DB::raw('MAX(ld_timestamp)) as tt'))
                ->where("ld_id",$code)
                ->get();
        }
        
        
		
		// $main_data = $this->db->query("SELECT TIMESTAMPDIFF(second,MIN(ld_timestamp), MAX(ld_timestamp)) as 'Diff' , UNIX_TIMESTAMP(MIN(ld_timestamp)) as 'open_time',TIMEDIFF(MIN(ld_timestamp), MAX(ld_timestamp)) as tt  FROM `linkly_details` WHERE `ld_id` = $code");
		
		// $basic = $main_data->row();
		 
		$basic = $main_data->first();
		
		
		$long = $basic->Diff;
				
		$steps = 10;
		
		$interval = $long / $steps;
		
		$init = 0;
		$init = $basic->open_time;
		
		
		$result = array();
		
		$result[] = array('time' => date('d M, h a', $init), 'unique_opens' => 0);     
		
		for ($i = 0; $i < $steps; $i++){
				
			
			$future = 0;
            $future = $init + $interval;
            
            if($datezz != null){
                
                $main_data = DB::table('linkly_details')
                    ->select(DB::raw('Count(*) as unique_opens'))
                    ->where("ld_id",$code)
                    ->where("ld_timestamp","<=", $datezz)   
                    ->whereBetween(DB::raw('UNIX_TIMESTAMP(ld_timestamp)'), [$init, $future])
                    ->get();

            }else{
                $main_data = DB::table('linkly_details')
                    ->select(DB::raw('Count(*) as unique_opens'))
                    ->where("ld_id",$code)
                    ->whereBetween(DB::raw('UNIX_TIMESTAMP(ld_timestamp)'), [$init, $future])
                    ->get();
            }
                        
            
			
            // $main_data = $this->db->query("SELECT Count(*) as unique_opens  FROM `linkly_details` WHERE `ld_id` = $code And UNIX_TIMESTAMP(ld_timestamp) between $init and $future");
            
            // $basic_second = $main_data->row();
            
            $basic_second = $main_data->first();
            
			
			$result[] = array('time' => date('d M, h a', $future), 'unique_opens' => $basic_second->unique_opens);     
			
			
			$init = $future;
		
		}
		
		$result[] = array('time' => date('d M, h a', $future + $interval), 'unique_opens' => 0); 
		
//		header('Content-Type: application/json');
//		echo json_encode($result);
		
		return $result;

    }
    
    public function getLinklyNew($id, Request $request)
    {
        $datezz = $request->input("datezz"); 


        $response = DB::table('master_campaign')
                    ->select('Linkly.li_id','Linkly.li_name','Linkly.li_unique','Linkly.li_url')             
                    ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                    ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                    ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                    ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                    ->where('li_sid' ,'=', null)
                    ->orderBy("Total_Opens","desc")   
                    ->take(5)
                    ->get();

        
        if($datezz != null){

            foreach ($response as $row) {

                $response3 = DB::table('master_campaign')
                                ->select("linkly_details.ld_ip")          
                                ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                                ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                                ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                                ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                                ->where("linkly_details.ld_id",$row->li_id)  
                                ->where('li_sid' ,'=', null)
                                ->where("linkly_details.ld_timestamp","<=",$date)     
                                ->groupBy('linkly_details.ld_ip')   
                                // ->groupBy('linkly_details.ld_id')                     
                                // ->take(5)
                                ->get();  

                    $row->unique_clicks = count($response3);
               
            }


        }else{
  
        }

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));

        
    }
    // tracking pixel id
    public function getLinkly($id, Request $request){

        // $mc_id = DB::table('master_campaign_trackingpixel')
        //         ->select('mctp_mc_id')->where('mctp_mc_track_id','=',$id)->get()[0]->mctp_mc_id;

        $date = $request->input("date");           

        $response = DB::table('master_campaign')
                    ->select('Linkly.li_id','Linkly.li_name','Linkly.li_unique','Linkly.li_url', DB::raw('( select COUNT(linkly_details.ld_id) from linkly_details where ld_id = li_id) as Total_Opens'))             
                    ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                    ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                    ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                    ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                    // ->where('li_sid' ,'!=', null)
                    ->orderBy("Total_Opens","desc")   
                    // ->take(5)
                    ->get();

        if($date != null){
            $response2 = DB::table('master_campaign')       
            ->select("linkly_details.ld_ip")          
            ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
            ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
            ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
            ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
            ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
            // ->where('li_sid' ,'!=', null)
            ->where("linkly_details.ld_timestamp","<=",$date)
            ->groupBy('linkly_details.ld_ip')               
            // ->take(5)
            ->get();  
        }else{
            $response2 = DB::table('master_campaign')
                ->select(DB::raw('COUNT(linkly_details.ld_ip) as people'))          
                ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                // ->where('li_sid' ,'!=', null)
                ->groupBy('linkly_details.ld_ip')               
                // ->take(5)
                ->get();  
        }

        

        foreach ($response as $row) {
            $row->people =  count($response2);                                  

            if($date != null){     
                $response3 = DB::table('master_campaign')
                    ->select("linkly_details.ld_ip")          
                    ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                    ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                    ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                    ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                    ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                    ->where("linkly_details.ld_id",$row->li_id)  
                    // ->where('li_sid' ,'=', null)
                    ->where("linkly_details.ld_timestamp","<=",$date)     
                    ->groupBy('linkly_details.ld_ip')   
                    // ->groupBy('linkly_details.ld_id')                     
                    // ->take(5)
                    ->get(); 
                    
                    $response4 = DB::table('master_campaign')
                    ->select("linkly_details.ld_ip")          
                    ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                    ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                    ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                    ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                    ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                    ->where("linkly_details.ld_id",$row->li_id)  
                    // ->where('li_sid' ,'=', null)
                    ->where("linkly_details.ld_timestamp","<=",$date)     
                    // ->groupBy('linkly_details.ld_ip')   
                    // ->groupBy('linkly_details.ld_id')                     
                    // ->take(5)
                    ->get();  

                    $row->unique_clicks = count($response3);  
                    $row->total_clicks = count($response4);  
            }else{
                $response3 = DB::table('master_campaign')
                    ->select(DB::raw('COUNT(linkly_details.ld_ip) as people'))          
                    ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                    ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                    ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                    ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                    ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                    ->where("linkly_details.ld_id",$row->li_id)  
                    // ->where('li_sid' ,'=', null)
                    ->groupBy('linkly_details.ld_ip')   
                    // ->groupBy('linkly_details.ld_id')                     
                    // ->take(5)
                    ->get();  

                    $response4 = DB::table('master_campaign')
                    ->select(DB::raw('linkly_details.ld_ip'))          
                    ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                    ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                    ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                    ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                    ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                    ->where("linkly_details.ld_id",$row->li_id)  
                    // ->where('li_sid' ,'=', null)
                    // ->groupBy('linkly_details.ld_ip')   
                    // ->groupBy('linkly_details.ld_id')                     
                    // ->take(5)
                    ->get();  


                    

                    $row->unique_clicks = count($response3); 

                    $row->total_clicks = count($response4);       
            }
            

                    

                
        }

      

         

        return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
        ));

        
    }

    // form tracking pixel
    public function getLinklyDetail($id, Request $request){

        $datezz = $request->input("datezz"); 
        
        if($datezz != null){ 

            $response = DB::table('master_campaign')   
                    ->select(DB::raw('Count(linkly_details.ld_platform) as total'), 'linkly_details.ld_country', 'linkly_details.ld_platform', 'linkly_details.ld_browser', 'linkly_details.ld_timestamp')
                    ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                    ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                    ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                    ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')   
                    // ->join('Sent_Mail','Sent_Mail.Camp_Code_Id','=','master_campaign_trackingpixel.mctp_mc_track_id')
                    ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)  
                    ->where("linkly_details.ld_timestamp","<=", $datezz)                 
                    // ->where('li_sid' ,'=', null)
                    ->groupBy('linkly_details.ld_platform') 
                    ->orderBy('total','desc')                
                    ->take(5)             
                    ->get();

        }else{

            $response = DB::table('master_campaign')   
                    ->select(DB::raw('Count(linkly_details.ld_platform) as total'), 'linkly_details.ld_country', 'linkly_details.ld_platform', 'linkly_details.ld_browser', 'linkly_details.ld_timestamp')
                    ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                    ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                    ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                    ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')   
                    // ->join('Sent_Mail','Sent_Mail.Camp_Code_Id','=','master_campaign_trackingpixel.mctp_mc_track_id')
                    ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)    
                    // ->where('li_sid' ,'=', null)
                    ->groupBy('linkly_details.ld_platform') 
                    ->orderBy('total','desc')                
                    ->take(5)             
                    ->get();

        }

            

            return Response::json(array(
            'error' => false,
            'response' => json_encode($response),
            'status_code' => 200
            ));
    }

    public function getAdTotalClicks($id){
        
                    $response = DB::table('master_campaign')
                            ->select(DB::raw('Count(linkly_details.ld_id) as total'))
                            ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                            ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                            ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                            ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                            ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id)
                            ->where('li_sid' ,'=', null)
                            ->get();
        
                    return Response::json(array(
                    'error' => false,
                    'response' => json_encode($response),
                    'status_code' => 200
                    ));
            }

            public function getLinklyDetailsById_New($id, Request $request){

                // $datezz = $request->input("datezz");
        
                    $response = DB::table('master_campaign')
                                    ->select('master_campaign.mc_id')
                                    ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                                    ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                                    ->where("Linkly.li_id",$id)
                                    // ->where('li_sid' ,'=', null)
                                    // ->take(5)        
                                    ->get();
        
                
                
                            
                
                            return Response::json(array(
                            'error' => false,
                            'response' => json_encode($response),
                            'status_code' => 200
                            ));
            }

    public function getLinklyDetailsById($id, Request $request){

        $datezz = $request->input("datezz");

        if($datezz != null){
            $response = DB::table('master_campaign')
                            ->select('linkly_details.ld_ip','linkly_details.ld_referer', 'linkly_details.ld_country', 'linkly_details.ld_platform', 'linkly_details.ld_browser', 'linkly_details.ld_timestamp','master_campaign.mc_id')
                            ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                            ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                            ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                            ->where("Linkly.li_id",$id)
                            ->where("ld_timestamp",$datezz)
                            ->where('li_sid' ,'=', null)    
                            // ->take(5)
                            ->get();
        }else{
            $response = DB::table('master_campaign')
                            ->select('linkly_details.ld_ip','linkly_details.ld_referer', 'linkly_details.ld_country', 'linkly_details.ld_platform', 'linkly_details.ld_browser', 'linkly_details.ld_timestamp','master_campaign.mc_id')
                            ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                            ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                            ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                            ->where("Linkly.li_id",$id)
                            ->where('li_sid' ,'=', null)
                            // ->take(5)        
                            ->get();

        }
        
                    
        
                    return Response::json(array(
                    'error' => false,
                    'response' => json_encode($response),
                    'status_code' => 200
                    ));
    }

    public function getTotalOpens($id){


        $response = DB::table('master_campaign')
                            ->select('master_campaign.mc_id','master_campaign.mc_name','master_campaign.mc_created_date','master_campaign.mc_order_id','master_campaign.mc_order_hash','master_campaign.mc_opens','master_campaign.mc_bonus', 'master_campaign.mc_type')
                            ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                            ->join('master_campaign_link','master_campaign_link.mcl_mc_id','=','master_campaign.mc_id')
                            ->join('Linkly','Linkly.li_id','=','master_campaign_link.mcl_li_id')                
                            ->where('Linkly.li_id',$id)
                            ->get();
                            
        

            // return Response::json(array(
            //                         'error' => false,
            //                         'response' => json_encode($response),             
            //                         'status_code' => 200         
            //                         ));










        foreach ($response as $row1) {

            $total_opens_assigned = DB::table('master_campaign_detail')
                ->select(DB::raw('SUM(balance) as total_opens_assigned'))
                ->where('mc_id', $row1->mc_id)
                ->groupBy('mc_id')
                ->get();
            if(count($total_opens_assigned) == 0){
                $row1->total_opens_assigned = 0;
            }else{
                $row1->total_opens_assigned = $total_opens_assigned[0]->total_opens_assigned;
                
            }
    
            $row1->total_opens_assigned = $row1->total_opens_assigned + $row1->mc_opens;
        }

        $bonus = $response[0]->mc_bonus;   
        $limit = $response[0]->total_opens_assigned; 
        
        
        $type = $response[0]->mc_type;
        
        if($type == 1){
            
                    $response = DB::table('master_campaign')
                                        ->select('master_campaign_trackingpixel.mctp_mc_track_id')          
                                        ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                                        ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                                        ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                                        ->where("Linkly.li_id",$id)
                                        ->get();  
        
                                        // return Response::json(array(
                                        //     'error' => false,
                                        //     'response' => json_encode((int)$response[0]->mctp_mc_track_id),
                                        //     'status_code' => 200
                                        //     ));   
                $id22 = (int)$response[0]->mctp_mc_track_id; 
                
                
                
        
        
                $response = DB::table('Sent_Mail_Detail')
                        ->select(DB::raw('COUNT(Email) as Opens'))    
                        ->where('Camp_Code_Id',$id22)     
                        ->get(); 
            
        }
        else{
            $response[0]->Opens = null;   
        }
         
        

        



                $limit += $bonus;

            if($limit < $response[0]->Opens){

                // $limit += $bonus;                     

                        for ($i = $limit; $i >= 0 ; $i = $i - 5) {                
                    
                            $response = DB::table('Sent_Mail_Detail')
                                ->select(DB::raw("DATE_FORMAT(Sm_Open_Time, '%Y-%m-%d %H:%i:%s') as Sm_Open_Time"))             
                                ->where('Camp_Code_Id',$id22) 
                                ->orderBy("Sm_Open_Time")   
                                ->limit($i)  
                                ->get(); 
                                
                                
    
                        $date = $response[count($response) - 1]->Sm_Open_Time;
        
                        $response = DB::table('master_campaign')
                                ->select('linkly_details.ld_timestamp')          
                                ->join("master_campaign_link",'master_campaign_link.mcl_mc_id', '=' ,'master_campaign.mc_id')
                                ->join("Linkly",'master_campaign_link.mcl_li_id', '=' ,'Linkly.li_id')
                                ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                ->join('master_campaign_trackingpixel','master_campaign_trackingpixel.mctp_mc_id','=','master_campaign.mc_id')
                                ->where("master_campaign_trackingpixel.mctp_mc_track_id",$id22)  
                                // ->where('li_sid' ,'=', null)    
                                ->where('linkly_details.ld_timestamp' ,'<=', $date)   
                                ->get();   
    
                        if($i + count($response) <= $limit){    

                            $data = array(
                                "date" => $date,                             
                                "opens" => $i,
                                "clicks" => count($response),
                                "total_opens_assigned" => $limit     
                            );                               

                            $response = DB::table('Linkly')->select('Linkly.li_name','Linkly.li_url as url', 'Linkly.li_unique as code', 'Linkly.li_id as detail_code','Linkly.li_timestamp')         
                                    ->join("master_campaign_link", 'Linkly.li_id', '=','master_campaign_link.mcl_li_id')
                                    ->where("Linkly.li_id",$id)
                                    ->get();    

                                $response2 = DB::table('Linkly')
                                            ->select(DB::raw('COUNT(linkly_details.ld_ip) as people'))          
                                            ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                            ->where("linkly_details.ld_id",$id)
                                            ->where("linkly_details.ld_timestamp","<=",$date)      
                                            // ->where('li_sid' ,'=', null)
                                            ->groupBy('linkly_details.ld_ip')               
                                            // ->take(5)
                                            ->get();  

                                 $response3 = DB::table('Linkly')
                                            ->select("*")          
                                            ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                            ->where("linkly_details.ld_id",$id)
                                            ->where("linkly_details.ld_timestamp","<=",$date)      
                                            // ->where('li_sid' ,'=', null)
                                            // ->groupBy('linkly_details.ld_ip')               
                                            // ->take(5)
                                            ->get();
                                $response4 = DB::table('linkly_details')
                                        // ->select("*")          
                                        // ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                        ->where("linkly_details.ld_id",$id)
                                        ->where("linkly_details.ld_timestamp","<=",$date)  
                                        ->where("linkly_details.ld_bot","=",1)  
                                        ->get();  

                                foreach ($response as $row) {
                                    $row->unique_opensss = count($response2); 
                                    $row->Total_Opens = count($response3);
                                    $row->Bots = count($response4);  
                                    $row->datezz = $date;   
                                    $row->limit = $limit;      
                                }
                                

                                return Response::json(array(
                                    'error' => false,
                                    'response' => json_encode($response),
                                    'status_code' => 200
                                    ));
                        }      
                    }

            }else{

                    $response = DB::table('Linkly')->select('Linkly.li_name','Linkly.li_url as url', DB::raw('( select COUNT(linkly_details.ld_id) from linkly_details where ld_id = li_id group by linkly_details.ld_id) as Total_Opens'), 'Linkly.li_unique as code', 'Linkly.li_id as detail_code','Linkly.li_timestamp')      
                    ->join("master_campaign_link", 'Linkly.li_id', '=','master_campaign_link.mcl_li_id')
                    ->where("Linkly.li_id",$id)
                    ->orderBy('Total_Opens','desc')  
                    ->get();  

                    $response2 = DB::table('Linkly')
                                ->select(DB::raw('COUNT(linkly_details.ld_ip) as people'))          
                                ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                                ->where("linkly_details.ld_id",$id)
                                // ->where('li_sid' ,'=', null)
                                ->groupBy('linkly_details.ld_ip')               
                                // ->take(5)
                                ->get();  

                    $response4 = DB::table('linkly_details')
                            // ->select("*")          
                            // ->join('linkly_details','linkly_details.ld_id','=','Linkly.li_id')
                            ->where("linkly_details.ld_id",$id)
                            ->where("linkly_details.ld_bot","=",1)  
                            ->get();  

                    foreach ($response as $row) {
                        $row->unique_opensss = count($response2);
                        $row->Bots = count($response4);     
                        $row->datezz = null;   
                        $row->limit = $limit;  
                    }
                    

                    return Response::json(array(
                        'error' => false,
                        'response' => json_encode($response),
                        'status_code' => 200
                        ));
            }



            ////////////////

        

    }

    public function getLatestAdLinks(Request $request){
        
                $email = $request->input('email');
        
                $response = DB::table('master_campaign')
                                ->select('Linkly.li_id','Linkly.li_name','Linkly.li_timestamp')
                                ->join('master_campaign_link','master_campaign.mc_id','=' ,'master_campaign_link.mcl_mc_id')
                                ->join('Linkly','master_campaign_link.mcl_li_id','=','Linkly.li_id')
                                ->join('mt_users','mt_users.mu_id','=','master_campaign.mc_company_id')
                                ->where('mt_users.mu_username', $email)
                                ->limit(4)
                                ->orderBy('li_timestamp','desc')
                                ->get();

                
                foreach ($response as $row) {
                    
                    $query = DB::table('linkly_details')
                                ->select(DB::raw('COUNT(ld_id) as Opens'))
                                ->where('ld_id',$row->li_id)->get();

                                $row->Total_Opens =  $query[0]->Opens;

                }
        
                return Response::json(array(
                    'error' => false,
                    'response' => json_encode($response),
                    'status_code' => 200
                ));
        
                // return $campaigns;
    }
    

}
