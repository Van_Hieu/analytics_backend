<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

use Illuminate\Contracts\Encryption\DecryptException;



class EmailController extends Controller
{
    
    public function sendMail(Request $request){
        
        // $first_name = $request->input('first_name');
        // $sure_name = $request->input('sure_name');
        // $email = $request->input('email');
        // $description = $request->input('description');
        
        
        
        // Mail::send('Html.view', $data, function ($message) {
        //     $message->from($email, $first_name.' '.$sure_name);
        //     // $message->sender('john@johndoe.com', 'John Doe');
        
        //     $message->to('vanh@merchantwise.com', 'Van Hieu');
        
        //     // $message->cc('john@johndoe.com', 'John Doe');
        //     // $message->bcc('john@johndoe.com', 'John Doe');
        
        //     // $message->replyTo('john@johndoe.com', 'John Doe');
        
        //     $message->subject('Subject');
          
        // });

        // Mail::raw('Text to e-mail', function ($message) {

        //     $first_name = 'Van Hieu';
        //     $sure_name = 'Nguyen';
        //     $email = 'vanh@merchantwise.com';
        //     $description = 'TESTTING';
           
        //     $message->from('vanh@merchantwise.com', $first_name.' '.$sure_name);
        //     // $message->sender('vanh@merchantwise.com', 'John Doe');
        
        //     $message->to('vanh@merchantwise.com', 'Van Hieu');
        
        //     // $message->cc('john@johndoe.com', 'John Doe');
        //     // $message->bcc('john@johndoe.com', 'John Doe');
        
        //     // $message->replyTo('john@johndoe.com', 'John Doe');
        
        //     $message->subject('Subject');   
        // });

        $title = "Hello";
        $content = "World";

        Mail::send('emails.send', ['title' => $title, 'content' => $content], function ($message)
        {

            $message->from('naveed@merchantwise.com', 'Naveed');

            $message->to('vanh@merchantwise.com'," Vanh");

            $message->subject('Subject');  
        });

        return response()->json(['message' => 'Request completed']);
        
        
    } 

}
