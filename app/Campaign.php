<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
     protected $table = 'Campaign';

     protected $primaryKey = 'Camp_Code_Id';
}
