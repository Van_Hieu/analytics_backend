<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


/* Master Campaign ----------- */

// Route::get('abc/{id}', function ($id) {
//     return 'User '.$id;
// });

Route::post('mastercampaign', 'CampaignController@createMaterCampaign');

Route::post('mastercampaign/get_industries', 'CampaignController@getIndustry');

Route::post('mastercampaign/get_campaigns', 'CampaignController@getMasterCampaigns');

Route::post('mastercampaign/update_is_sent_email', 'CampaignController@update_is_sent_email');

Route::post('mastercampaign/update_is_sent_email_campaigns', 'CampaignController@update_is_sent_email_campaigns');



Route::get('mastercampaign/{id?}', 'CampaignController@getMasterCampaign');

Route::post('mastercampaign/email_trackers/{id?}', 'CampaignController@getEmailTrackers');

Route::post('mastercampaign/email_trackers_new/{id?}', 'CampaignController@getEmailTrackers_New');


Route::post('mastercampaign/ad_trackers/{id?}', 'CampaignController@getAdTrackers');


Route::post('mastercampaign/ad_trackers_new/{id?}', 'CampaignController@getAdTrackers_New');

Route::post('mastercampaign/update_campaign', 'CampaignController@update_Campaign');


Route::post('mastercampaign/get_order_code', 'CampaignController@get_Order_Code');

Route::post('mastercampaign/ending_campaign', 'CampaignController@end_campaign');

Route::post('mastercampaign/update_credits_for_starter', 'CampaignController@update_credits_for_starter');









Route::post('mastercampaign/get_orders', 'CampaignController@getOrders');

Route::post('mastercampaign/get_opens_campaigns_subscriptions', 'CampaignController@getOpensCampaignsSubscriptions');

Route::post('mastercampaign/add_opens_credit', 'CampaignController@addOpensClicksCredit');

Route::post('mastercampaign/change_set_cap_to_track_all', 'CampaignController@changeSetCapToTrackAll');





Route::post('mastercampaign/check_broken_links', 'CampaignController@check_Broken_Links_From_Campaign');

Route::post('mastercampaign/fixed_broken_links', 'CampaignController@fixed_Broken_Links_From_Campaign');

Route::post('mastercampaign/check_links', 'CampaignController@check_Link_From_Campaign');  











/* Email Tracker ----------- */

Route::post('campaign', 'CampaignController@createCampaign');


Route::post('campaigns', 'CampaignController@getCampaigns');


Route::post('campaign/get_latest_camps', 'CampaignController@getLatestCampaigns');

Route::post('campaign/get_latest_camps_comp', 'CampaignController@getLatestCampaignsComparision');



Route::get('campaign/{id?}', 'CampaignController@getCampaign');

Route::get('campaign/opens/{id?}', 'CampaignController@getTotalOpened');

Route::post('campaign/popular_email_clients/{id?}', 'CampaignController@getPopularEmailClients');

Route::post('campaign/unique_popular_email_clients/{id?}', 'CampaignController@getUniquePopularEmailClients');


Route::post('campaign/popular_countries/{id?}', 'CampaignController@getPopularCountries');

Route::get('campaign/detail/{id?}', 'CampaignController@getDetails');

Route::get('campaign/mail_services/{id?}', 'CampaignController@getMailService');

Route::post('campaign/user_location/{id?}', 'CampaignController@getUserLocation');

Route::post('campaign/read_time/{id?}', 'CampaignController@getReadingTimebySecond');      

Route::post('campaign/read_statistics/{id?}', 'CampaignController@getReadingStatistics'); 

Route::post('campaign/reading_time_device_desktop/{id?}', 'CampaignController@getEmailClientsReadingTimeDesktop');

Route::post('campaign/reading_time_device_mobile/{id?}', 'CampaignController@getEmailClientsReadingTimeMobile');

Route::post('campaign/reading_time_device_tablet/{id?}', 'CampaignController@getEmailClientsReadingTimeTablet');




Route::get('campaign/open_timeline/{id?}', 'CampaignController@getOpensBaseOnTimeline');

Route::get('campaign/devices/{id?}', 'CampaignController@getDeviceType');

Route::post('campaign/unique_opened/{id?}', 'CampaignController@getUniqueOpened');     


Route::post('campaign/open_daily_activity/{id?}/{val?}/{datess?}', 'CampaignController@openHourlyActivity');           


Route::post('campaign/email_clients/{id?}', 'CampaignController@getEmailClients');

Route::post('campaign/email_clients_platform/{id?}', 'CampaignController@getEmailClientsPlatform');




Route::post('campaign/calculate_rate', 'CampaignController@calculate_rate');  


Route::post('campaign/calculate_rate_new', 'CampaignController@calculate_rate_new');  






Route::post('campaign/create_report', 'CampaignController@createReport');  

Route::get('campaign/get_report/{id?}', 'CampaignController@getReports');  

Route::post('campaign/get_reports', 'CampaignController@getAllReports');

Route::get('campaign/get_hot_leads/{id?}', 'CampaignController@getHotLeads');


Route::get('campaign/get_hot_leads_reading/{id?}', 'CampaignController@getHotLeadsReading');

Route::get('campaign/get_could_spark/{id?}', 'CampaignController@getCouldSpark');

Route::get('campaign/get_gone_cold/{id?}', 'CampaignController@getGoneCold');

Route::get('campaign/get_hot_leads_data/{id?}', 'CampaignController@getHotLeadsData');

Route::post('campaign/test_hot_leads', 'CampaignController@getHotLeadsAndGoneCold');  

Route::post('campaign/get_hotleads', 'CampaignController@get_HotLeads'); 


Route::post('campaign/test_hot_leads_2', 'CampaignController@getHotLeadsAndGoneCold_2');    


Route::get('campaign/get_total_unique_open/{id?}', 'CampaignController@getTotal_Unique_Open');       



Route::post('campaign/show_all_time_open', 'CampaignController@show_all_time_open');    


Route::post('campaign/create_html_file', 'CampaignController@create_html_file');    















/* Link Tracker ----------- */

Route::post('linkly/{id?}', 'AdTrackerController@getLinklyActivitiesById');   

Route::post('linkly/get_links/{id?}', 'AdTrackerController@getLinkly'); 


Route::post('linkly/get_links_new/{id?}', 'AdTrackerController@getLinklyNew');                 
  


Route::post('linkly/get_links_details/{id?}', 'AdTrackerController@getLinklyDetail');

Route::get('linkly/get_total_opens/{id?}', 'AdTrackerController@getTotalOpens');

Route::post('linkly/get_links_details_by_id/{id?}', 'AdTrackerController@getLinklyDetailsById');

Route::post('linkly/get_links_details_by_id_new/{id?}', 'AdTrackerController@getLinklyDetailsById_New');

Route::get('linkly/get_ad_total_opens/{id?}', 'AdTrackerController@getAdTotalClicks');

Route::post('linkly/get_ad_latest', 'AdTrackerController@getLatestAdLinks');

Route::post('linkly/open_hourly_click_activity/{id?}/{val?}/{datess?}', 'AdTrackerController@openHourlyClickActivity');                        

Route::post('linkly/get_email_click/{id?}/{link_id?}', 'AdTrackerController@get_email_click');                        











/* User --------------*/

Route::post('user', 'UserController@checkUserExist');

Route::post('user/create', 'UserController@createUser');

Route::delete('user/delete', 'UserController@deleteUser');

Route::put('user/update', 'UserController@updateUser');

Route::post('user/update_email', 'UserController@updateUserEmail');



Route::post('send_mail', 'EmailController@sendMail');