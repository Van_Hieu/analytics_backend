<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello','HelloWorld@index');

Route::get('/hello/{name}', 'HelloWorld@show');

Route::get('foo', function () {
    return 'Hello World';
});

// /* Campaign ----------- */
// Route::get('/api/v1/campaign/{name}', 'CampaignController@createMaterCampaign');

// Route::get('/api/v1/campaign/createCampaign/{name}', 'CampaignController@createCampaign');

// Route::get('/api/v1/campaign/getCampaigns/{email?}', 'CampaignController@getCampaigns');

// Route::get('/api/v1/campaign/getCampaign/{id?}', 'CampaignController@getCampaign');
// /* Campaign ----------- */

// /* User --------------*/
// Route::get('/api/v1/user/createUser/', 'User@createUser');


// /* User --------------*/



